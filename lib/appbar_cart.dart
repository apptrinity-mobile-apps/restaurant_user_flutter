import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class YourAppbarCart extends StatelessWidget implements PreferredSizeWidget {
  final double height;


  const YourAppbarCart({
    Key key,
    @required this.height,
  }) : super(key: key);

  @override
  Size get preferredSize => Size.fromHeight(height);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          AppBar(
            toolbarHeight: height,
            //toolbarHeight:  height-30.00,
            automaticallyImplyLeading: false,
            elevation: 0.0,
            backgroundColor: Colors.white,
            centerTitle: true,
            title: Text("Your Order"),
            leading: Builder(
              builder: (BuildContext context) {
                return IconButton(
                  padding: EdgeInsets.only(left: 12.0),
                  icon: Image.asset("images/back_arrow.png", width: 20, height: 20),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
