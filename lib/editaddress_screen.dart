import 'package:async/async.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:restaurant_user/apis/addUserAddress.dart';
import 'package:restaurant_user/apis/editUserAddress.dart';
import 'package:restaurant_user/apis/getAddressLists.dart';
import 'package:restaurant_user/session/userRepository.dart';
import 'package:restaurant_user/utils/DialogClass.dart';
import 'package:restaurant_user/utils/all_constans.dart';
import 'package:restaurant_user/utils/back_press.dart';
import 'package:restaurant_user/utils/sizeconfig.dart';
import 'package:toast/toast.dart';

import 'add_address_location.dart';
import 'appbar_cart.dart';
import 'model/get_addresslistresponse.dart';
import 'model/getallmenus.dart';
import 'model/getitemmodifiersresponse.dart';
import 'model/getmenugroups.dart';
import 'model/getmenuitems.dart';
import 'myaddress_screen.dart';

class EditAddressScreen extends StatefulWidget {
  final String edit_address_type;
  final String edit_full_address;
  final String edit_geolocation;
  final String edit_latitude;
  final String edit_longitude;
  final String edit_floor;
  final String edit_howtoreach;
  final bool edit_isdefault;
  final String edit_address_id;

  const EditAddressScreen(this.edit_address_type, this.edit_full_address, this.edit_geolocation, this.edit_latitude, this.edit_longitude,
      this.edit_floor, this.edit_howtoreach, this.edit_isdefault, this.edit_address_id,
      {Key key})
      : super(key: key);

  @override
  _EditAddressScreenState createState() => _EditAddressScreenState();
}

class _EditAddressScreenState extends State<EditAddressScreen> {
  int cart_count = 0;
  bool enable_searchlist = false;
  String employee_id = "";
  String first_name = "";
  String last_name = "";
  String restaurant_id = "";
  String servicearea_id = "";
  String selected_address_type_list = "";
  String user_id = "";
  double screenheight = 0.0;
  String returnVal = "";
  bool hasTimerStopped = false;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  String showgoutview = "order_location_icon.png";
  String orderscanner = "order_scanner.png";
  String restaurantsquare = "restaurant_square.png";
  String noorders = "no_order_dash.png";
  TextEditingController search_Controller = TextEditingController();
  bool isSwitched = false;
  bool isSwitchedveg = false;

  List<MenuList> _get_all_menus = new List();
  List<MenugroupsList> _get_menu_groups = new List();
  bool _loading = true;

  int selected_pos = -1;
  List<String> selectedCheckbox = [];

  final List<String> address_type = [
    "Home",
    "Work",
    "Hotel",
    "Other",
  ];

  int selected = 0;
  TextEditingController location_Controller = TextEditingController();
  TextEditingController complete_address_Controller = TextEditingController();
  TextEditingController floor_address_Controller = TextEditingController();
  TextEditingController how_to_reach_address_Controller = TextEditingController();
  String selected_address_type = "";

  var current_address;
  var current_address_string;
  var current_lat;
  var current_long;
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Position _currentPosition;
  LatLng pinPosition;
  final _formKey = GlobalKey<FormState>();
  CancelableOperation cancelableOperation;

  @override
  void initState() {
    super.initState();

    UserRepository().getuserdetails().then((getuserdetails) {
      setState(() {
        user_id = getuserdetails[0];
      });
    });

    if (widget.edit_geolocation != null) {
      current_address_string = widget.edit_geolocation;
    }

    complete_address_Controller.text = widget.edit_full_address.toString();
    floor_address_Controller.text = widget.edit_floor.toString();
    how_to_reach_address_Controller.text = widget.edit_howtoreach.toString();
    location_Controller.text = widget.edit_full_address.toString();

    print("EDITADDRESSINITIAL" +
        complete_address_Controller.text.toString() +
        "=====" +
        floor_address_Controller.text.toString() +
        "=====" +
        how_to_reach_address_Controller.text.toString() +
        "=====" +
        location_Controller.text.toString());
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    //  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: login_passcode_bg1));
    print("Height" + SizeConfig.screenHeight.toString());
    final space = SizedBox(height: 20);
    if (SizeConfig.screenHeight >= 600 && SizeConfig.screenHeight < 800) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 550 && SizeConfig.screenHeight < 600) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 800) {
      screenheight = 550;
    }
    final screen_height = MediaQuery.of(context).size.height;
    print(screen_height);

    return WillPopScope(
        onWillPop: () async => onBackPressed(context),
        child:Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: Colors.white,
        appBar: AppBar(
          toolbarHeight: 56,
          //toolbarHeight:  height-30.00,
          automaticallyImplyLeading: false,
          elevation: 0.0,
          backgroundColor: Colors.white,
          centerTitle: true,
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                padding: EdgeInsets.only(left: 12.0),
                icon: Image.asset("images/back_arrow.png", width: 20, height: 20),
                onPressed: () {
                  onBackPressed(context);
                },
              );
            },
          ),
          actions: [

          ],
        ),
        body: SingleChildScrollView(
            child: Container(
                margin: EdgeInsets.fromLTRB(40, 0, 40, 20),
                width: double.infinity,
                alignment: Alignment.center,
                child: new Form(
                    key: _formKey, //padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
                    child: new Column(
                      children: [
                        Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              'Enter Address Details',
                              style: new TextStyle(color: login_passcode_text, fontSize: 20.0, fontWeight: FontWeight.bold),
                            )),
                        SizedBox(
                          height: 0,
                        ),
                        Container(
                            alignment: Alignment.topLeft,
                            margin: EdgeInsets.only(bottom: 5, top: 15),
                            child: Text(
                              'Your Location',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                color: text_hint_color,
                                fontSize: 14,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w500,
                              ),
                            )),
                        TextFormField(
                          autocorrect: false,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            prefixIcon: IconButton(icon: new Image.asset('images/current_location_icon.png', height: 25, width: 25)),
                            prefixIconConstraints: BoxConstraints(
                              minWidth: 5,
                              minHeight: 5,
                            ),
                            isDense: true,
                            suffixIconConstraints: BoxConstraints(
                              minWidth: 2,
                              minHeight: 2,
                            ),
                            suffixIcon: InkWell(
                                child: Padding(
                                    padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                    child: Text(
                                      "Change",
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontSize: 16,
                                        color: login_passcode_bg1,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.w600,
                                      ),
                                    )),
                                onTap: () {
                                  Navigator.of(context)
                                      .pushReplacement(MaterialPageRoute(builder: (BuildContext context) => AddAddressLocationSearch()));
                                }),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0)),
                            hintText: current_address_string,
                            hintStyle: TextStyle(
                              color: location_text,
                              fontSize: 16,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          controller: location_Controller,
                          maxLines: null,
                        ),
                        Container(
                            alignment: Alignment.topLeft,
                            margin: EdgeInsets.only(bottom: 5, top: 15),
                            child: Text(
                              'Complete Address*',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                color: text_hint_color,
                                fontSize: 14,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w500,
                              ),
                            )),
                        TextFormField(
                          controller: complete_address_Controller,
                          obscureText: false,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(0.0),
                                borderSide: BorderSide(
                                  color: location_border,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(0.0),
                                borderSide: BorderSide(
                                  color: login_form_hint,
                                  width: 1.0,
                                ),
                              ),
                              filled: true,
                              fillColor: Colors.white,
                              hintText: widget.edit_full_address,
                              hintStyle: TextStyle(
                                color: location_text,
                                fontSize: 16,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w400,
                              ),
                              contentPadding: EdgeInsets.only(
                                bottom: 30 / 2,
                                left: 15 / 2,
                                right: 15 / 2, // HERE THE IMPORTANT PART
                                // HERE THE IMPORTANT PART
                              ),
                              border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))),
                        ),
                        Container(
                            alignment: Alignment.topLeft,
                            margin: EdgeInsets.only(bottom: 5, top: 15),
                            child: Text(
                              'Floor(Optional)',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                color: text_hint_color,
                                fontSize: 14,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w500,
                              ),
                            )),
                        TextFormField(
                          controller: floor_address_Controller,
                          obscureText: false,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(0.0),
                                borderSide: BorderSide(
                                  color: location_border,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(0.0),
                                borderSide: BorderSide(
                                  color: login_form_hint,
                                  width: 1.0,
                                ),
                              ),
                              filled: true,
                              fillColor: Colors.white,
                              hintText: widget.edit_floor,
                              hintStyle: TextStyle(
                                color: location_text,
                                fontSize: 16,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w400,
                              ),
                              contentPadding: EdgeInsets.only(
                                bottom: 30 / 2,
                                left: 15 / 2,
                                right: 15 / 2, // HERE THE IMPORTANT PART
                                // HERE THE IMPORTANT PART
                              ),
                              border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))),
                        ),
                        Container(
                            alignment: Alignment.topLeft,
                            margin: EdgeInsets.only(bottom: 5, top: 15),
                            child: Text(
                              'How To Reach ( Optional )',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                color: text_hint_color,
                                fontSize: 14,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w500,
                              ),
                            )),
                        TextFormField(
                          controller: how_to_reach_address_Controller,
                          obscureText: false,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(0.0),
                                borderSide: BorderSide(
                                  color: location_border,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(0.0),
                                borderSide: BorderSide(
                                  color: login_form_hint,
                                  width: 1.0,
                                ),
                              ),
                              filled: true,
                              fillColor: Colors.white,
                              hintText: widget.edit_howtoreach,
                              hintStyle: TextStyle(
                                color: location_text,
                                fontSize: 16,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w400,
                              ),
                              contentPadding: EdgeInsets.only(
                                bottom: 30 / 2,
                                left: 15 / 2,
                                right: 15 / 2, // HERE THE IMPORTANT PART
                                // HERE THE IMPORTANT PART
                              ),
                              border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))),
                        ),
                        Container(
                            alignment: Alignment.topLeft,
                            margin: EdgeInsets.only(bottom: 0, top: 10),
                            child: Text(
                              'Select The Address Type',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                color: text_hint_color,
                                fontSize: 14,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w500,
                              ),
                            )),
                        Container(
                          margin: EdgeInsets.only(left: 0, right: 0),
                          padding: EdgeInsets.symmetric(horizontal: 0.0, vertical: 10.0),
                          height: MediaQuery.of(context).size.height * 0.1,
                          color: Colors.white,
                          child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: address_type.length,
                              itemBuilder: (context, index) {
                                return Container(
                                  margin: EdgeInsets.all(5),
                                  width: MediaQuery.of(context).size.width * 0.2,
                                  child: InkWell(
                                      onTap: () {
                                        setState(() {
                                          selected_pos = index;
                                          print(address_type[index]);
                                          selected_address_type_list = address_type[index];
                                          print("ADDRESSTYPE" + selected_address_type_list);
                                        });
                                      },
                                      child: Card(
                                        elevation: 2,
                                        color: selected_pos == index ? login_passcode_text : Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(0),
                                        ),
                                        child: Container(
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              color: login_passcode_bg2,
                                            ),
                                            borderRadius: BorderRadius.all(Radius.circular(0)),
                                          ),
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              Text(
                                                address_type[index],
                                                style: TextStyle(color: selected_pos == index ? Colors.white : login_passcode_text, fontSize: 14),
                                              )
                                            ],
                                          ),
                                        ),
                                      )),
                                );
                              }),
                        ),
                        Container(
                            margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                            // height: 50,
                            width: double.infinity,
                            // alignment: Alignment.center,
                            decoration: BoxDecoration(color: login_passcode_bg1, borderRadius: BorderRadius.circular(0)),
                                child: TextButton(
                                    // minWidth: double.infinity,
                                    // height: double.infinity,
                                    child: Text("UPDATE ADDRESS",
                                        style: TextStyle(fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w800, color: Colors.white)),
                                    onPressed: () {
                                      if (_formKey.currentState.validate()) {
                                        _formKey.currentState.save();
                                        SignDialogs.showLoadingDialog(context, "Loading...", _keyLoader);
                                        cancelableOperation?.cancel();
                                        CancelableOperation.fromFuture(Future.delayed(Duration(seconds: 1), () {
                                          print(complete_address_Controller.text.toString() +
                                              "-------" +
                                              floor_address_Controller.text.toString() +
                                              "--------" +
                                              how_to_reach_address_Controller.text.toString() +
                                              "------" +
                                              selected_address_type_list +
                                              "-----" +
                                              widget.edit_latitude +
                                              "-----" +
                                              widget.edit_longitude +
                                              "------" +
                                              user_id +
                                              "-----" +
                                              widget.edit_address_id);
                                          EditAddressRepository()
                                              .editUserAddress(
                                                  complete_address_Controller.text.toString(),
                                                  floor_address_Controller.text.toString(),
                                                  how_to_reach_address_Controller.text.toString(),
                                                  selected_address_type_list,
                                                  widget.edit_latitude,
                                                  widget.edit_longitude,
                                                  user_id,
                                                  true,
                                                  selected_address_type_list,
                                                  widget.edit_address_id)
                                              .then((value) {
                                            print(value);
                                            if (value.responseStatus == 0) {
                                              Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
                                              Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                                            } else if (value.responseStatus == 1) {
                                              Navigator.of(_keyLoader.currentContext, rootNavigator: false).pop();
                                              Toast.show("Address Updated Successfully", context,
                                                  duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                                              Navigator.of(context)
                                                  .pushReplacement(MaterialPageRoute(builder: (BuildContext context) => MyAddressScreen("", "", "")));
                                            } else {
                                              Navigator.of(_keyLoader.currentContext, rootNavigator: false).pop();
                                              Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                                            }
                                          });
                                        }));
                                      }
                                    }))
                      ],
                    ))))));
  }
}
