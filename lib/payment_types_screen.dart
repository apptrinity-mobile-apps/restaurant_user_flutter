import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:requests/requests.dart';
import 'package:restaurant_user/apis/getrestaurants.dart';
import 'package:restaurant_user/appbar_cart.dart';
import 'package:restaurant_user/utils/all_constans.dart';
import 'package:restaurant_user/utils/back_press.dart';
import 'package:restaurant_user/utils/sizeconfig.dart';

import 'cart.dart';
import 'utils/dottedline.dart';
import 'model/get_restaurantsresponse.dart';
import 'model/getallmenus.dart';
import 'model/getitemmodifiersresponse.dart';
import 'model/getmenugroups.dart';
import 'model/getmenuitems.dart';

class GetMenuItems {
  Future<List<MenuItem>> getmenu(
    String menu_id,
    String menu_groupid,
    String res_id,
  ) async {
    var body = json.encode({'menuId': menu_id, 'menuGroupId': menu_groupid, 'restaurantId': res_id});
    print(body);
    dynamic response = await Requests.post(base_url_waiter + "menu_items", body: body, headers: {'Content-type': 'application/json'});
    print("resultadditem---" + response);
    final res = json.decode(response);
    //print("result---"+res);
    if (res['responseStatus'] == 1) {
      Iterable list = res['menuItem'];
      return list.map((model) => MenuItem.fromJson(model)).toList();
    } else {
      return [];
    }
  }
}

class PaymentTypesScreen extends StatefulWidget {

  final String restaurant_id;
  final String delivery_location;
  final String delivery_lattitude;
  final String delivery_longitude;
  final String dine_in_id;
  final String dine_in_name;
  final String dine_in_behaviour;

  const PaymentTypesScreen(this.restaurant_id,this.delivery_location, this.delivery_lattitude, this.delivery_longitude,this.dine_in_id, this.dine_in_name, this.dine_in_behaviour, {Key key})
      : super(key: key);
  @override
  _PaymentTypesScreenState createState() => _PaymentTypesScreenState();
}

class _PaymentTypesScreenState extends State<PaymentTypesScreen> {
  int cart_count = 0;
  bool enable_searchlist = false;
  String userid = "60e420c859263dad5bcb64b0";
  String employee_id = "";
  String first_name = "";
  String last_name = "";
  String restaurant_id = "";
  String servicearea_id = "";
  String user_id = "";
  double screenheight = 0.0;
  String returnVal = "";
  bool hasTimerStopped = false;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  TextEditingController search_Controller = TextEditingController();
  List<Restaurants> getRestaurants = new List();
  bool isSwitched = false;
  bool isSwitchedveg = false;

  List<MenuList> _get_all_menus = new List();
  List<MenugroupsList> _get_menu_groups = new List();
  bool _loading = true;

  int selected_pos = 0;
  String res_id = "60e41f7f59263dad5bcb64a7";
  String menu_id = "";
  String menu_groupid = "";
  List<MenuItem> _get_all_menuitems = new List();
  List<ModifiersGroupsList> _get_all_menuitemsmodifiersgrouplist;
  List<ModifierList> _get_all_menuitemsmodifierslist;
  List<String> selectedCheckbox = [];

  int selected = 0;
  var counter_value = 1;

  @override
  void initState() {
    super.initState();
    RestaurantRepository().getRestaurants().then((result_showalldayview) {
      setState(() {
        getRestaurants = result_showalldayview;
        print("RESTAURANTLENGTH=====" + getRestaurants.length.toString());

        cart_count = getRestaurants.length;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    //  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: login_passcode_bg1));
    print("Height" + SizeConfig.screenHeight.toString());
    final space = SizedBox(height: 20);
    if (SizeConfig.screenHeight >= 600 && SizeConfig.screenHeight < 800) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 550 && SizeConfig.screenHeight < 600) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 800) {
      screenheight = 550;
    }
    final screen_height = MediaQuery.of(context).size.height;
    print(screen_height);

    return WillPopScope(
        onWillPop: () async => onBackPressed(context),
        child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: Colors.white,
        appBar: AppBar(
          toolbarHeight: 56,
          //toolbarHeight:  height-30.00,
          automaticallyImplyLeading: false,
          elevation: 0.0,
          backgroundColor: Colors.white,
          centerTitle: true,
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                padding: EdgeInsets.only(left: 12.0),
                icon: Image.asset("images/back_arrow.png", width: 20, height: 20),
                onPressed: () {
                  onBackPressed(context);
                },
              );
            },
          ),
          actions: [

          ],
        ),
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.fromLTRB(15, 0, 15, 0),
            //height: SizeConfig.safeBlockHorizontal * 155,
            child: Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisSize: MainAxisSize.min, children: [
              Padding(
                  padding: EdgeInsets.fromLTRB(2, 0, 0, 5),
                  child: Text(
                    "Select Payment Method",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 18,
                      color: login_passcode_text,
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w800,
                    ),
                  )),
              Padding(
                padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                child: DotWidget(
                  dashColor: location_border,
                  dashHeight: 1,
                  dashWidth: 10,
                  emptyWidth: 2,
                  totalWidth: 360,
                ),
              ),
              Padding(
                  padding: EdgeInsets.fromLTRB(10, 10, 0, 5),
                  child: Text(
                    "UPI'S",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 18,
                      color: login_passcode_text,
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w800,
                    ),
                  )),
              Padding(
                  padding: EdgeInsets.fromLTRB(10, 0, 0, 5),
                  child: Text(
                    "Pay using your UPI Accounts.",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 14,
                      color: login_passcode_text,
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w400,
                    ),
                  )),
              Container(
                  margin: EdgeInsets.fromLTRB(5, 5, 5, 0),
                  alignment: Alignment.center,
                  child: Row(
                    children: [
                      InkWell(
                        onTap: () {
                          setState(() {
                            print("GPAY");
                          });
                        },
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(1, 1, 1, 1),
                          child: Image.asset(
                            "images/gpay_icon.png",
                            height: 100,
                            width: 100,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          setState(() {
                            print("APPLEPAY");
                          });
                        },
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(10, 1, 1, 1),
                          child: Image.asset(
                            "images/applepay_icon.png",
                            height: 100,
                            width: 100,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ],
                  )),
              Padding(
                  padding: EdgeInsets.fromLTRB(10, 10, 0, 5),
                  child: Text(
                    "Online Payments",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 18,
                      color: login_passcode_text,
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w800,
                    ),
                  )),
              Padding(
                  padding: EdgeInsets.fromLTRB(10, 0, 0, 5),
                  child: Text(
                    "Pay using your credit and debit cards and netbanking.",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 14,
                      color: login_passcode_text,
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w400,
                    ),
                  )),
              Container(
                  height: 50,
                  margin: EdgeInsets.fromLTRB(10, 15, 10, 0),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(color: login_passcode_text, borderRadius: BorderRadius.circular(5)),
                  child: InkWell(child: FlatButton(child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                          padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                          child: Text(
                            "CREDIT CARD",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.white,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w800,
                            ),
                          )),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                              padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                              child: Image.asset(
                                "images/back_arrow.png",
                                color: Colors.white,
                                height: 20,
                                width: 20,
                                fit: BoxFit.cover,
                              )),
                        ],
                      )
                    ],
                  ),onPressed:(){ print("CREDITCARD");
                  Navigator.push(context, MaterialPageRoute(builder: (context) => CartScreen("Credit",widget.dine_in_id,widget.dine_in_name,widget.dine_in_behaviour,widget.delivery_location,widget.delivery_lattitude,widget.delivery_longitude,widget.restaurant_id,"")));
                  },),)),
              Container(
                  height: 50,
                  margin: EdgeInsets.fromLTRB(10, 15, 10, 0),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(color: receipt_header, borderRadius: BorderRadius.circular(5)),
                  child: InkWell(child: FlatButton(child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                          padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                          child: Text(
                            "DEBIT CARD",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: 14,
                              color: login_passcode_text,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w800,
                            ),
                          )),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                              padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                              child: Image.asset(
                                "images/back_arrow.png",
                                color: Colors.white,
                                height: 20,
                                width: 20,
                                fit: BoxFit.cover,
                              )),
                        ],
                      )
                    ],
                  ),onPressed:(){ print("DEBIT CARD");
                  Navigator.push(context, MaterialPageRoute(builder: (context) => CartScreen("Debit",widget.dine_in_id,widget.dine_in_name,widget.dine_in_behaviour,widget.delivery_location,widget.delivery_lattitude,widget.delivery_longitude,widget.restaurant_id,"")));
                  },),)),

              Container(
                  height: 50,
                  margin: EdgeInsets.fromLTRB(10, 15, 10, 0),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(color: login_passcode_text, borderRadius: BorderRadius.circular(5)),
                  child: InkWell(child: FlatButton(child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                          padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                          child: Text(
                            "NETBAKING",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.white,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w800,
                            ),
                          )),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                              padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                              child: Image.asset(
                                "images/back_arrow.png",
                                color: Colors.white,
                                height: 20,
                                width: 20,
                                fit: BoxFit.cover,
                              )),
                        ],
                      )
                    ],
                  ),onPressed:(){
                    print("NETBANKING");
                    Navigator.push(context, MaterialPageRoute(builder: (context) => CartScreen("Bank",widget.dine_in_id,widget.dine_in_name,widget.dine_in_behaviour,widget.delivery_location,widget.delivery_lattitude,widget.delivery_longitude,widget.restaurant_id,"")));
                  },),)),
              Padding(
                  padding: EdgeInsets.fromLTRB(10, 15, 0, 5),
                  child: Text(
                    "Other Payment Options",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 18,
                      color: login_passcode_text,
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w800,
                    ),
                  )),
              Padding(
                  padding: EdgeInsets.fromLTRB(10, 0, 0, 5),
                  child: Text(
                    "Pay using these methods if you like.",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 14,
                      color: login_passcode_text,
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w400,
                    ),
                  )),
              Container(
                  height: 50,
                  margin: EdgeInsets.fromLTRB(10, 15, 10, 0),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(color: receipt_header, borderRadius: BorderRadius.circular(5)),
                  child: InkWell(child: FlatButton(child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                          padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                          child: Text(
                            "CASH ON DELIVERY",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: 14,
                              color: login_passcode_text,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w800,
                            ),
                          )),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                              padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                              child: Image.asset(
                                "images/back_arrow.png",
                                color: Colors.white,
                                height: 20,
                                width: 20,
                                fit: BoxFit.cover,
                              )),
                        ],
                      )
                    ],
                  ),onPressed:(){ print("CASH ON DELIVERY");
                  Navigator.push(context, MaterialPageRoute(builder: (context) => CartScreen("Cash",widget.dine_in_id,widget.dine_in_name,widget.dine_in_behaviour,widget.delivery_location,widget.delivery_lattitude,widget.delivery_longitude,widget.restaurant_id,"")));},),)),
            ]),
          ),
        )));
  }
}
