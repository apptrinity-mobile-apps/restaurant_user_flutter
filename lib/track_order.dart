import 'dart:async';

import 'package:async/async.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:restaurant_user/dashboard.dart';
import 'package:restaurant_user/session/userRepository.dart';
import 'package:restaurant_user/utils/all_constans.dart';
import 'package:restaurant_user/utils/back_press.dart';
import 'package:restaurant_user/utils/sizeconfig.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;

import 'utils/dottedline.dart';

class TrackOrder extends StatefulWidget {
  final String from_screen;
  const TrackOrder(this.from_screen, {Key key}):super(key: key);

  @override
  _TrackOrderState createState() => _TrackOrderState();
}

class _TrackOrderState extends State<TrackOrder> {
  BitmapDescriptor pinLocationIcon;
  Set<Marker> _markers = {};
  Completer<GoogleMapController> _controller = Completer();

  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Position _currentPosition;
  LatLng pinPosition;
  TextEditingController location_Controller = TextEditingController();
  var current_address;
  CameraPosition initialLocation;
  final Marker marker = Marker(icon: BitmapDescriptor.fromAsset('images/current_location.png'));

  BitmapDescriptor customIcon;
  TextEditingController pickupnotes_controller = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  CancelableOperation cancelableOperation;
  String session_latitude = "",session_longitude = "",session_location = "";

  @override
  void initState() {
    super.initState();
    UserRepository().getlocationdetails().then((getlocation) {
      setState(() {
        session_latitude = getlocation[0];
        session_longitude = getlocation[1];
        session_location = getlocation[2];
      });
    });
    _getCurrentLocation();
  }

  _getCurrentLocation() {
    geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.best).then((Position position) {
      setState(() {
        _currentPosition = position;
        if (_currentPosition.latitude != null && _currentPosition.longitude != null) {
          pinPosition = LatLng(_currentPosition.latitude, _currentPosition.longitude);
          initialLocation = CameraPosition(zoom: 16, bearing: 30, target: pinPosition);
        }
      });
      _getLocation();
      setCustomMapPin();
    }).catchError((e) {
      print(e);
    });
  }

  /*_getAddressFromLatLng() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() {
        print("Locationname"+place.name+""+place.subLocality+""+_currentPosition.latitude.toString()+"------"+_currentPosition.longitude.toString());
        */ /*_currentAddress =
        "${place.locality}, ${place.postalCode}, ${place.country}";*/ /*
      });
    } catch (e) {
      print(e);
    }
  }
*/

  _getLocation() async {
    Position position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    debugPrint('location: ${position.latitude}');
    final coordinates = new Coordinates(position.latitude, position.longitude);
    var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;

    print("${first.featureName} : ${first.addressLine}");
    debugPrint('LOCATIONADDRESS: ${first.featureName}+"======="+${first.addressLine}');

    current_address = first.addressLine;
    location_Controller.text = first.addressLine.toString();

    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(target: current_address, zoom: 16),
      ),
    );
  }

  void setCustomMapPin() async {
    pinLocationIcon = await BitmapDescriptor.fromAsset('images/current_location_icon.png');
  }

  /*Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png)).buffer.asUint8List();
  }
*/

  /* void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }*/

  @override
  Widget build(BuildContext context) {
    /*if (_currentPosition.latitude != null && _currentPosition.longitude != null) {
      pinPosition = LatLng(_currentPosition.latitude, _currentPosition.longitude);
      initialLocation = CameraPosition(zoom: 16, bearing: 30, target: pinPosition);
    }*/

    // these are the minimum required values to set
    // the camera position
    // CameraPosition initialLocation = CameraPosition(zoom: 16, bearing: 30, target: pinPosition);
    SizeConfig().init(context);
    //  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: login_passcode_bg1));
    print("Height" + SizeConfig.screenHeight.toString());

    return WillPopScope(
        onWillPop: () async => _onBackPressed(),
        child: MaterialApp(
      home: Scaffold(
          resizeToAvoidBottomInset: true,
          appBar: AppBar(
            toolbarHeight: 56,
            //toolbarHeight:  height-30.00,
            automaticallyImplyLeading: false,
            elevation: 0.0,
            backgroundColor: Colors.white,
            centerTitle: true,
            title: Text("Your Order"),
            leading: Builder(
              builder: (BuildContext context) {
                return IconButton(
                  padding: EdgeInsets.only(left: 12.0),
                  icon: Image.asset("images/back_arrow.png", width: 20, height: 20),
                  onPressed: () {
                    // Navigator.of(context).pop();
                    _onBackPressed();
                    },
                );
              },
            ),
          ),
          bottomSheet: Container(
              margin: EdgeInsets.fromLTRB(15, 10, 15, 15),
              // alignment: Alignment.center,
              decoration: BoxDecoration(color: login_passcode_text, borderRadius: BorderRadius.circular(5)),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                    child: TextButton(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                  padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                                  child: Image.asset(
                                    "images/share_with_friends.png",
                                    height: 45,
                                    width: 45,
                                    fit: BoxFit.cover,
                                  )),
                              Container(
                                alignment: Alignment.topLeft,
                                child: Padding(
                                    padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                    child: Text(
                                      "Need Support",
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontSize: 16,
                                        color: dashboard_bg,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.w300,
                                      ),
                                    )),
                              ),
                            ],
                          ),
                          Padding(
                              padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                              child: Icon(
                                Icons.arrow_forward_ios_rounded,
                                color: Colors.white,
                                size: 18,
                              )),
                        ],
                      ),
                      onPressed: () {
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(80, 10, 20, 0),
                    child: DotWidget(
                      dashColor: location_border,
                      dashHeight: 0.3,
                      dashWidth: 10,
                      emptyWidth: 0,
                      totalWidth: 250,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                    child: TextButton(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                  padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                                  child: Image.asset(
                                    "images/share_with_friends.png",
                                    height: 45,
                                    width: 45,
                                    fit: BoxFit.cover,
                                  )),
                              Container(
                                alignment: Alignment.topLeft,
                                child: Padding(
                                    padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                    child: Text(
                                      "Share Details with friends",
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontSize: 16,
                                        color: dashboard_bg,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.w300,
                                      ),
                                    )),
                              ),
                            ],
                          ),
                          Padding(
                              padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                              child: Icon(
                                Icons.arrow_forward_ios_rounded,
                                color: Colors.white,
                                size: 18,
                              )),
                        ],
                      ),
                      onPressed: () {
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(80, 10, 20, 0),
                    child: DotWidget(
                      dashColor: location_border,
                      dashHeight: 0.3,
                      dashWidth: 10,
                      emptyWidth: 0,
                      totalWidth: 250,
                    ),
                  ),
                ],
              )),
          body: Container(
            color: Colors.white,
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                  height: 250,
                  child: _currentPosition == null
                      ? Center(
                    child: SpinKitPulse(color: Colors.lightBlueAccent),
                  )
                      : GoogleMap(
                      myLocationEnabled: true,
                      // compassEnabled: true,
                      markers: _markers,
                      scrollGesturesEnabled: true,
                      zoomGesturesEnabled: true,
                      rotateGesturesEnabled: true,
                      tiltGesturesEnabled: true,
                      initialCameraPosition: initialLocation,
                      onMapCreated: (GoogleMapController controller) {
                        //controller.
                        _controller.complete(controller);

                        setState(() {
                          _markers.add(Marker(markerId: MarkerId('<MARKER_ID>'), position: pinPosition, icon: pinLocationIcon));
                        });
                      }),
                ),
                SingleChildScrollView(
                  child: Column(
                    children: [
                      Container(
                          margin: EdgeInsets.fromLTRB(20, 20, 20, 20),
                          // alignment: Alignment.center,
                          decoration: BoxDecoration(color: text_split_bg, borderRadius: BorderRadius.circular(5)),
                          child: Column(
                            children: [
                              Container(
                                margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
                                height: 60,
                                  child: TextButton(
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Padding(
                                            padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                                            child: Image.asset(
                                              "images/support_icon.png",
                                              height: 50,
                                              width: 50,
                                              fit: BoxFit.cover,
                                            )),
                                        Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Padding(
                                                padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                child: Text(
                                                  "Shyam Doe",
                                                  textAlign: TextAlign.left,
                                                  style: TextStyle(
                                                    fontSize: 18,
                                                    color: login_passcode_text,
                                                    fontFamily: 'Poppins',
                                                    fontWeight: FontWeight.w800,
                                                  ),
                                                )),
                                            Padding(
                                                padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                                child: Text(
                                                  "Chicago, USA",
                                                  textAlign: TextAlign.left,
                                                  style: TextStyle(
                                                    fontSize: 14,
                                                    color: login_passcode_text,
                                                    fontFamily: 'Poppins',
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                )),
                                          ],
                                        ),
                                        Padding(
                                            padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                                            child: Image.asset(
                                              "images/call_icon.png",
                                              height: 50,
                                              width: 50,
                                              fit: BoxFit.cover,
                                            )),
                                      ],
                                    ),
                                    onPressed: () {
                                      print("CallIng");
                                      //launch("tel://9849496520");
                                      UrlLauncher.launch('tel:+${919849496520}');
                                    },
                                  ),
                              ),
                            ],
                          )),
                      Container(
                        margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                        child: new Form(
                          key: _formKey,
                          child: Column(
                            children: [
                              TextFormField(
                                validator: (val) {
                                  if (val.isEmpty) return 'Any pickup notes';
                                  return null;
                                },
                                controller: pickupnotes_controller,
                                obscureText: false,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.white,
                                  hintText: "Any pickup notes",
                                  hintStyle: TextStyle(
                                    color: login_form_hint,
                                    fontSize: SizeConfig.blockSizeHorizontal * 2.8,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.w400,
                                  ),
                                  contentPadding: EdgeInsets.only(
                                    bottom: 30 / 2,
                                    left: 50 / 2, // HERE THE IMPORTANT PART
                                    // HERE THE IMPORTANT PART
                                  ),
                                ),
                              ),
                              Container(
                                  margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                                  // height: 50,
                                  width: double.infinity,
                                  // alignment: Alignment.center,
                                  decoration: BoxDecoration(color: login_passcode_bg1, borderRadius: BorderRadius.circular(3)),
                                      child: TextButton(
                                          // minWidth: double.infinity,
                                          // height: double.infinity,
                                          child: Text("SEND", style: TextStyle(fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w800, color: Colors.white)),
                                          onPressed: () {
                                            if (_formKey.currentState.validate()) {
                                              _formKey.currentState.save();
                                              cancelableOperation?.cancel();
                                              CancelableOperation.fromFuture(Future.delayed(Duration(seconds: 1), () {}));
                                            }
                                          }))
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          )),
    ));
  }

  Future<bool> _onBackPressed() async {
    if (widget.from_screen == "cart") {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => HomePage(session_latitude, session_longitude, session_location)),
              (Route<dynamic> route) => false);
    } else if (widget.from_screen == "orders") {
      onBackPressed(context);
    }
    return true;
  }

}

class Utils {
  static String mapStyles = '''[
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
]''';
}
