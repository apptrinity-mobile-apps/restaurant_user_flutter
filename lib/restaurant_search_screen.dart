import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:restaurant_user/apis/getSearchRestaurantsApi.dart';
import 'package:restaurant_user/utils/all_constans.dart';
import 'package:restaurant_user/utils/back_press.dart';
import 'package:toast/toast.dart';

import 'utils/dottedline.dart';
import 'model/get_restaurantsresponse.dart';
import 'model/getsearchrestaurantresponse.dart';

class RestaurantSearchScreen extends StatefulWidget {
  @override
  _RestaurantSearchScreenState createState() => new _RestaurantSearchScreenState();
}

class _RestaurantSearchScreenState extends State<RestaurantSearchScreen> {
  TextEditingController controller = new TextEditingController();

  // Get json result and convert it to model. Then add
  Future<Null> getUserDetails() async {
    final response = await http.get(url);
    final responseJson = json.decode(response.body);

    setState(() {
      for (Map user in responseJson) {
        restaurantsList.add(RestaurantDetails.fromJson(user));
      }
    });
  }

  bool _loading = true;
  List<RestaurantsList> __search_items_list = new List();

  @override
  void initState() {
    super.initState();

    getUserDetails();

    GetSearchRestaurantsApi().searchrestaurantbynmae("Minerva").then((value) {
      print("SEARCHRESTAURANT RESPSTATUS  " + value.responseStatus.toString());
      debugPrint(
          base_url + "------" + value.responseStatus.toString() + "-------" + value.result.toString() + "-----" + value.restaurantsList.toString());
      if (value.responseStatus == 1) {
        setState(() {
          _loading = false;

          for (int t = 0; t < value.restaurantsList.length; t++) {
            __search_items_list.add(value.restaurantsList[t]);
          }
        });
      } else if (value.responseStatus == 0) {
        setState(() {
          _loading = false;
          Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
        });
      } else {
        setState(() {
          _loading = false;
          Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => onBackPressed(context),
        child: Scaffold(
          body: new Column(
            children: <Widget>[
              new Container(
                margin: EdgeInsets.fromLTRB(10, 30, 10, 0),
                color: order_appbar_bg,
                child: new Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: new Card(
                    child: new ListTile(
                      leading: new Icon(Icons.search),
                      title: new TextField(
                        controller: controller,
                        decoration: new InputDecoration(hintText: 'Search', border: InputBorder.none),
                        onChanged: onSearchTextChanged,
                      ),
                      trailing: new IconButton(
                        icon: new Icon(Icons.cancel),
                        onPressed: () {
                          controller.clear();
                          onSearchTextChanged('');
                        },
                      ),
                    ),
                  ),
                ),
              ),
              new Expanded(
                child: __search_items_list.length != 0 || controller.text.isNotEmpty
                    ? new ListView.builder(
                        itemCount: __search_items_list.length,
                        itemBuilder: (context, i) {
                          return new Card(
                            child: new ListTile(
                              leading: new CircleAvatar(
                                backgroundImage: new NetworkImage(
                                  __search_items_list[i].restaurantLogo,
                                ),
                              ),
                              title: new Text(__search_items_list[i].restaurantName + ' ' + __search_items_list[i].restaurantName),
                            ),
                            margin: const EdgeInsets.all(0.0),
                          );
                        },
                      )
                    : new ListView.builder(
                        itemCount: restaurantsList.length,
                        itemBuilder: (context, index) {
                          return new Card(
                            child: new ListTile(
                              leading: new CircleAvatar(
                                backgroundImage: new NetworkImage(
                                  restaurantsList[index].restaurantLogo,
                                ),
                              ),
                              title: new Text(restaurantsList[index].restaurantName + ' ' + restaurantsList[index].restaurantName),
                            ),
                            margin: const EdgeInsets.all(0.0),
                          );
                        },
                      ),
              ),
            ],
          ),
        ));
  }

  onSearchTextChanged(String text) async {
    _searchResult.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    restaurantsList.forEach((restaurantsList) {
      if (restaurantsList.restaurantName.contains(text) || restaurantsList.restaurantName.contains(text)) _searchResult.add(restaurantsList);
    });

    setState(() {});
  }
}

List<RestaurantDetails> _searchResult = [];

List<RestaurantDetails> restaurantsList = [];

// final String url = 'http://3.14.187.24/api/user/search_restaurant_by_name';
final String url = 'http://18.190.55.150/api/user/search_restaurant_by_name';

class RestaurantDetails {
  final int id;
  final String restaurantId, restaurantName, restaurantLogo;

  RestaurantDetails({this.id, this.restaurantId, this.restaurantName, this.restaurantLogo});

  factory RestaurantDetails.fromJson(Map<String, dynamic> json) {
    return new RestaurantDetails(
      id: json['id'],
      restaurantId: json['restaurantId'],
      restaurantName: json['restaurantName'],
      restaurantLogo: json['restaurantLogo'],
    );
  }
}
