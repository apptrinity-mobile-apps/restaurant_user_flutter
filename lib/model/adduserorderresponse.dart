class adduserorderresponse {
  String orderId;
  int responseStatus;
  String result;

  adduserorderresponse({this.orderId, this.responseStatus, this.result});

  adduserorderresponse.fromJson(Map<String, dynamic> json) {
    orderId = json['orderId'];
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['orderId'] = this.orderId;
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}
