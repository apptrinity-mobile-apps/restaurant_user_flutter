
class GetAddressListresponse {
  int responseStatus;
  String result;
  List<UserAddressBookList> userAddressBookList;

  GetAddressListresponse({this.responseStatus, this.result, this.userAddressBookList});

  GetAddressListresponse.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    if (json['userAddressBookList'] != null) {
      userAddressBookList = new List<UserAddressBookList>();
      json['userAddressBookList'].forEach((v) {
        userAddressBookList.add(new UserAddressBookList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    if (this.userAddressBookList != null) {
      data['userAddressBookList'] =
          this.userAddressBookList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class UserAddressBookList {
  String addressType;
  String createdOn;
  String customerId;
  String floor;
  String fullAddress;
  GeoLocation geoLocation;
  String howToReach;
  String id;
  bool isDefault;
  String latitude;
  String longitude;
  Null zipCode;

  UserAddressBookList(
      {this.addressType,
        this.createdOn,
        this.customerId,
        this.floor,
        this.fullAddress,
        this.geoLocation,
        this.howToReach,
        this.id,
        this.isDefault,
        this.latitude,
        this.longitude,
        this.zipCode});

  UserAddressBookList.fromJson(Map<String, dynamic> json) {
    addressType = json['addressType'];
    createdOn = json['createdOn'];
    customerId = json['customerId'];
    floor = json['floor'];
    fullAddress = json['fullAddress'];
    geoLocation = json['geoLocation'] != null
        ? new GeoLocation.fromJson(json['geoLocation'])
        : null;
    howToReach = json['howToReach'];
    id = json['id'];
    isDefault = json['isDefault'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    zipCode = json['zipCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['addressType'] = this.addressType;
    data['createdOn'] = this.createdOn;
    data['customerId'] = this.customerId;
    data['floor'] = this.floor;
    data['fullAddress'] = this.fullAddress;
    if (this.geoLocation != null) {
      data['geoLocation'] = this.geoLocation.toJson();
    }
    data['howToReach'] = this.howToReach;
    data['id'] = this.id;
    data['isDefault'] = this.isDefault;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['zipCode'] = this.zipCode;
    return data;
  }
}

class GeoLocation {
  List<double> coordinates;
  String type;

  GeoLocation({this.coordinates, this.type});

  GeoLocation.fromJson(Map<String, dynamic> json) {
    coordinates = json['coordinates'].cast<double>();
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['coordinates'] = this.coordinates;
    data['type'] = this.type;
    return data;
  }
}
