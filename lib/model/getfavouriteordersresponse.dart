
import 'package:restaurant_user/model/getitemmodifiersresponse.dart';

class getfavouriteordersresponse {

  List<FavoriteOrders> favoriteOrders;
  int responseStatus;
  String result;

  getfavouriteordersresponse({this.favoriteOrders, this.responseStatus, this.result});

  getfavouriteordersresponse.fromJson(Map<String, dynamic> json) {
  if (json['favoriteOrders'] != null) {
  favoriteOrders = new List<FavoriteOrders>();
  json['favoriteOrders'].forEach((v) {
  favoriteOrders.add(new FavoriteOrders.fromJson(v));
  });
  }
  responseStatus = json['responseStatus'];
  result = json['result'];
  }

  Map<String, dynamic> toJson() {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  if (this.favoriteOrders != null) {
  data['favoriteOrders'] =
  this.favoriteOrders.map((v) => v.toJson()).toList();
  }
  data['responseStatus'] = this.responseStatus;
  data['result'] = this.result;
  return data;
  }
  }

  class FavoriteOrders {
  String id;
  List<Items> items;
  String orderedOn;
  String restaurantLogo;
  String restaurantName;
  double totalAmount;

  FavoriteOrders(
  {this.id,
  this.items,
  this.orderedOn,
  this.restaurantLogo,
  this.restaurantName,
  this.totalAmount});

  FavoriteOrders.fromJson(Map<String, dynamic> json) {
  id = json['id'];
  if (json['items'] != null) {
  items = new List<Items>();
  json['items'].forEach((v) {
  items.add(new Items.fromJson(v));
  });
  }
  orderedOn = json['orderedOn'];
  restaurantLogo = json['restaurantLogo'];
  restaurantName = json['restaurantName'];
  totalAmount = json['totalAmount'];
  }

  Map<String, dynamic> toJson() {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['id'] = this.id;
  if (this.items != null) {
  data['items'] = this.items.map((v) => v.toJson()).toList();
  }
  data['orderedOn'] = this.orderedOn;
  data['restaurantLogo'] = this.restaurantLogo;
  data['restaurantName'] = this.restaurantName;
  data['totalAmount'] = this.totalAmount;
  return data;
  }
  }

  class Items {
  String itemName;
  int quantity;

  Items({this.itemName, this.quantity});

  Items.fromJson(Map<String, dynamic> json) {
  itemName = json['itemName'];
  quantity = json['quantity'];
  }

  Map<String, dynamic> toJson() {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['itemName'] = this.itemName;
  data['quantity'] = this.quantity;
  return data;
  }


}
