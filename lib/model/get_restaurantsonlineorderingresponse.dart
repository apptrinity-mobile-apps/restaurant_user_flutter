

class GetRestaurantsOnlineOrderingresponse {
  int responseStatus;
  String result;
  RulesData rulesData;

  GetRestaurantsOnlineOrderingresponse({this.responseStatus, this.result, this.rulesData});

  GetRestaurantsOnlineOrderingresponse.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    rulesData = json['rulesData'] != null
        ? new RulesData.fromJson(json['rulesData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    if (this.rulesData != null) {
      data['rulesData'] = this.rulesData.toJson();
    }
    return data;
  }
}

class RulesData {
  List<ApprovalRules> approvalRules;
  bool deliveryStatus;
  bool ordersAcceptanceStatus;
  bool rulesEnabled;
  bool takeOutStatus;

  RulesData(
      {this.approvalRules,
        this.deliveryStatus,
        this.ordersAcceptanceStatus,
        this.rulesEnabled,
        this.takeOutStatus});

  RulesData.fromJson(Map<String, dynamic> json) {
    if (json['approvalRules'] != null) {
      approvalRules = new List<ApprovalRules>();
      json['approvalRules'].forEach((v) {
        approvalRules.add(new ApprovalRules.fromJson(v));
      });
    }
    deliveryStatus = json['deliveryStatus'];
    ordersAcceptanceStatus = json['ordersAcceptanceStatus'];
    rulesEnabled = json['rulesEnabled'];
    takeOutStatus = json['takeOutStatus'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.approvalRules != null) {
      data['approvalRules'] =
          this.approvalRules.map((v) => v.toJson()).toList();
    }
    data['deliveryStatus'] = this.deliveryStatus;
    data['ordersAcceptanceStatus'] = this.ordersAcceptanceStatus;
    data['rulesEnabled'] = this.rulesEnabled;
    data['takeOutStatus'] = this.takeOutStatus;
    return data;
  }
}

class ApprovalRules {
  bool enable;
  String enforcement;
  String order;
  String ruleType;
  int triggerAmount;

  ApprovalRules(
      {this.enable,
        this.enforcement,
        this.order,
        this.ruleType,
        this.triggerAmount});

  ApprovalRules.fromJson(Map<String, dynamic> json) {
    enable = json['enable'];
    enforcement = json['enforcement'];
    order = json['order'];
    ruleType = json['ruleType'];
    triggerAmount = json['triggerAmount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['enable'] = this.enable;
    data['enforcement'] = this.enforcement;
    data['order'] = this.order;
    data['ruleType'] = this.ruleType;
    data['triggerAmount'] = this.triggerAmount;
    return data;
  }
}