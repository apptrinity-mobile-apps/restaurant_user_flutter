
import 'package:restaurant_user/model/getitemmodifiersresponse.dart';

class getyourordersresponse {

  List<OrdersList> ordersList;
  int responseStatus;
  String result;

  getyourordersresponse({this.ordersList, this.responseStatus, this.result});

  getyourordersresponse.fromJson(Map<String, dynamic> json) {
  if (json['ordersList'] != null) {
  ordersList = new List<OrdersList>();
  json['ordersList'].forEach((v) {
  ordersList.add(new OrdersList.fromJson(v));
  });
  }
  responseStatus = json['responseStatus'];
  result = json['result'];
  }

  Map<String, dynamic> toJson() {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  if (this.ordersList != null) {
  data['ordersList'] = this.ordersList.map((v) => v.toJson()).toList();
  }
  data['responseStatus'] = this.responseStatus;
  data['result'] = this.result;
  return data;
  }
  }

  class OrdersList {
  String createdOn;
  String id;
  List<ItemsList> itemsList;
  String restaurantLogo;
  String restaurantId;
  String restaurantName;
  double totalAmount;

  OrdersList(
  {this.createdOn,
  this.id,
  this.itemsList,
  this.restaurantLogo,
  this.restaurantId,
  this.restaurantName,
  this.totalAmount});

  OrdersList.fromJson(Map<String, dynamic> json) {
  createdOn = json['createdOn'];
  id = json['id'];
  if (json['itemsList'] != null) {
  itemsList = new List<ItemsList>();
  json['itemsList'].forEach((v) {
  itemsList.add(new ItemsList.fromJson(v));
  });
  }
  restaurantLogo = json['restaurantLogo'];
  restaurantId = json['restaurantId'];
  restaurantName = json['restaurantName'];
  totalAmount = json['totalAmount'];
  }

  Map<String, dynamic> toJson() {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['createdOn'] = this.createdOn;
  data['id'] = this.id;
  if (this.itemsList != null) {
  data['itemsList'] = this.itemsList.map((v) => v.toJson()).toList();
  }
  data['restaurantLogo'] = this.restaurantLogo;
  data['restaurantId'] = this.restaurantId;
  data['restaurantName'] = this.restaurantName;
  data['totalAmount'] = this.totalAmount;
  return data;
  }
  }

  class ItemsList {
  String itemName;
  List<ModifierList> modifiersList;
  int quantity;

  ItemsList({this.itemName, this.modifiersList, this.quantity});

  ItemsList.fromJson(Map<String, dynamic> json) {
  itemName = json['itemName'];
  if (json['modifiersList'] != null) {
  modifiersList = new List<ModifierList>();
  json['modifiersList'].forEach((v) {
  modifiersList.add(new ModifierList.fromJson(v));
  });
  }
  quantity = json['quantity'];
  }

  Map<String, dynamic> toJson() {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['itemName'] = this.itemName;
  if (this.modifiersList != null) {
  data['modifiersList'] =
  this.modifiersList.map((v) => v.toJson()).toList();
  }
  data['quantity'] = this.quantity;
  return data;
  }

}
