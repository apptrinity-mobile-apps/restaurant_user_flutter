import 'SendTaxRatesModel.dart';

class GetRestaurantDetailResponse {
  List<Menus> menus;
  List<PopularDishes> popularDishes;
  int responseStatus;
  RestaurantDetails restaurantDetails;
  String result;

  GetRestaurantDetailResponse(
      {this.menus,
        this.popularDishes,
        this.responseStatus,
        this.restaurantDetails,
        this.result});

  GetRestaurantDetailResponse.fromJson(Map<String, dynamic> json) {
    if (json['menus'] != null) {
      menus = new List<Menus>();
      json['menus'].forEach((v) {
        menus.add(new Menus.fromJson(v));
      });
    }
    if (json['popularDishes'] != null) {
      popularDishes = new List<PopularDishes>();
      json['popularDishes'].forEach((v) {
        popularDishes.add(new PopularDishes.fromJson(v));
      });
    }
    responseStatus = json['responseStatus'];
    restaurantDetails = json['restaurantDetails'] != null
        ? new RestaurantDetails.fromJson(json['restaurantDetails'])
        : null;
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.menus != null) {
      data['menus'] = this.menus.map((v) => v.toJson()).toList();
    }
    if (this.popularDishes != null) {
      data['popularDishes'] =
          this.popularDishes.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = this.responseStatus;
    if (this.restaurantDetails != null) {
      data['restaurantDetails'] = this.restaurantDetails.toJson();
    }
    data['result'] = this.result;
    return data;
  }
}

class Menus {
  String id;
  List<MenuGroups> menuGroups;
  String menuName;
  String restaurantId;

  Menus({this.id, this.menuGroups, this.menuName, this.restaurantId});

  Menus.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    if (json['menuGroups'] != null) {
      menuGroups = new List<MenuGroups>();
      json['menuGroups'].forEach((v) {
        menuGroups.add(new MenuGroups.fromJson(v));
      });
    }
    menuName = json['menuName'];
    restaurantId = json['restaurantId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.menuGroups != null) {
      data['menuGroups'] = this.menuGroups.map((v) => v.toJson()).toList();
    }
    data['menuName'] = this.menuName;
    data['restaurantId'] = this.restaurantId;
    return data;
  }
}

class MenuGroups {
  String groupDescription;
  String id;
  List<MenuItems> menuItems;
  String name;
  int priceStrategy;
  String restaurantId;
  String type;

  MenuGroups(
      {this.groupDescription,
        this.id,
        this.menuItems,
        this.name,
        this.priceStrategy,
        this.restaurantId,
        this.type});

  MenuGroups.fromJson(Map<String, dynamic> json) {
    groupDescription = json['groupDescription'];
    id = json['id'];
    if (json['menuItems'] != null) {
      menuItems = new List<MenuItems>();
      json['menuItems'].forEach((v) {
        menuItems.add(new MenuItems.fromJson(v));
      });
    }
    name = json['name'];
    priceStrategy = json['priceStrategy'];
    restaurantId = json['restaurantId'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['groupDescription'] = this.groupDescription;
    data['id'] = this.id;
    if (this.menuItems != null) {
      data['menuItems'] = this.menuItems.map((v) => v.toJson()).toList();
    }
    data['name'] = this.name;
    data['priceStrategy'] = this.priceStrategy;
    data['restaurantId'] = this.restaurantId;
    data['type'] = this.type;
    return data;
  }
}

class MenuItems {
  String basePrice;
  bool diningOptionTaxException;
  bool diningTaxOption;
  String id;
  bool inheritDiningOptionTax;
  bool inheritTaxInclude;
  bool inheritTaxRate;
  String itemDescription;
  String itemImage;
  String name;
  int pricingStrategy;
  int priceProvider;
  String restaurantId;
  bool taxIncludeOption;
  List<SendTaxRatesModel> taxesList;
  String type;
  List<SizeList> sizeList;

  MenuItems(
      {this.basePrice,
        this.diningOptionTaxException,
        this.diningTaxOption,
        this.id,
        this.inheritDiningOptionTax,
        this.inheritTaxInclude,
        this.inheritTaxRate,
        this.itemDescription,
        this.itemImage,
        this.name,
        this.pricingStrategy,
        this.priceProvider,
        this.restaurantId,
        this.taxIncludeOption,
        this.taxesList,
        this.type,
        this.sizeList});

  MenuItems.fromJson(Map<String, dynamic> json) {
    basePrice = json['basePrice'] == null ? "" : json['basePrice'];
    diningOptionTaxException = json['diningOptionTaxException'];
    diningTaxOption = json['diningTaxOption'];
    id = json['id'];
    inheritDiningOptionTax = json['inheritDiningOptionTax'];
    inheritTaxInclude = json['inheritTaxInclude'];
    inheritTaxRate = json['inheritTaxRate'];
    itemDescription = json['itemDescription'];
    itemImage = json['itemImage'];
    name = json['name'];
    pricingStrategy = json['pricingStrategy'];
    priceProvider = json['priceProvider'];
    restaurantId = json['restaurantId'];
    taxIncludeOption = json['taxIncludeOption'];
    if (json['taxesList'] != null) {
      taxesList = new List<Null>();
      json['taxesList'].forEach((v) {
        taxesList.add(new SendTaxRatesModel.fromJson(v));
      });
    }
    type = json['type'];
    if (json['sizeList'] != null) {
      sizeList = new List<SizeList>();
      json['sizeList'].forEach((v) {
        sizeList.add(new SizeList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['basePrice'] = this.basePrice;
    data['diningOptionTaxException'] = this.diningOptionTaxException;
    data['diningTaxOption'] = this.diningTaxOption;
    data['id'] = this.id;
    data['inheritDiningOptionTax'] = this.inheritDiningOptionTax;
    data['inheritTaxInclude'] = this.inheritTaxInclude;
    data['inheritTaxRate'] = this.inheritTaxRate;
    data['itemDescription'] = this.itemDescription;
    data['itemImage'] = this.itemImage;
    data['name'] = this.name;
    data['pricingStrategy'] = this.pricingStrategy;
    data['priceProvider'] = this.priceProvider;
    data['restaurantId'] = this.restaurantId;
    data['taxIncludeOption'] = this.taxIncludeOption;
    if (this.taxesList != null) {
      data['taxesList'] = this.taxesList.map((v) => v.toJson()).toList();
    }
    data['type'] = this.type;
    if (this.sizeList != null) {
      data['sizeList'] = this.sizeList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SizeList {
  double price;
  String sizeName;

  SizeList({this.price, this.sizeName});

  SizeList.fromJson(Map<String, dynamic> json) {
    price = json['price'] as double;
    sizeName = json['sizeName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['price'] = this.price;
    data['sizeName'] = this.sizeName;
    return data;
  }
}

class PopularDishes {
  String basePrice;
  String id;
  int inventory;
  String itemImage;
  int maxQuantity;
  String menuGroupId;
  String menuGroupName;
  String menuId;
  String menuName;
  String name;
  String portion;
  int pricingStrategy;
  String restaurantId;
  List<SendTaxRatesModel> taxesList;
  List<SizeList> sizeList;

  PopularDishes(
      {this.basePrice,
        this.id,
        this.inventory,
        this.itemImage,
        this.maxQuantity,
        this.menuGroupId,
        this.menuGroupName,
        this.menuId,
        this.menuName,
        this.name,
        this.portion,
        this.pricingStrategy,
        this.restaurantId,
        this.taxesList,
        this.sizeList});

  PopularDishes.fromJson(Map<String, dynamic> json) {
    basePrice = json['basePrice'] == null ? "" : json['basePrice'];
    id = json['id'];
    inventory = json['inventory'];
    itemImage = json['itemImage'];
    maxQuantity = json['maxQuantity'];
    menuGroupId = json['menuGroupId'];
    menuGroupName = json['menuGroupName'];
    menuId = json['menuId'];
    menuName = json['menuName'];
    name = json['name'];
    portion = json['portion'];
    pricingStrategy = json['pricingStrategy'];
    restaurantId = json['restaurantId'];
    if (json['taxesList'] != null) {
      taxesList = <SendTaxRatesModel>[];
      json['taxesList'].forEach((v) {
        taxesList.add(new SendTaxRatesModel.fromJson(v));
      });
    }
    if (json['sizeList'] != null) {
      sizeList = <SizeList>[];
      json['sizeList'].forEach((v) {
        sizeList.add(new SizeList.fromJson(v));
      });
    } else {
      sizeList = <SizeList>[];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['basePrice'] = this.basePrice;
    data['id'] = this.id;
    data['inventory'] = this.inventory;
    data['itemImage'] = this.itemImage;
    data['maxQuantity'] = this.maxQuantity;
    data['menuGroupId'] = this.menuGroupId;
    data['menuGroupName'] = this.menuGroupName;
    data['menuId'] = this.menuId;
    data['menuName'] = this.menuName;
    data['name'] = this.name;
    data['portion'] = this.portion;
    data['pricingStrategy'] = this.pricingStrategy;
    data['restaurantId'] = this.restaurantId;
    if (this.taxesList != null) {
      data['taxesList'] = this.taxesList.map((v) => v.toJson()).toList();
    }
    if (this.sizeList != null) {
      data['sizeList'] = this.sizeList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class RestaurantDetails {
  String cityId;
  String countryId;
  String createdOn;
  String email;
  String franchiseId;
  String id;
  bool isFranchiseRestaurant;
  String leadId;
  String location;
  String resType;
  int restauranPhNo;
  String restaurantDescription;
  String restaurantId;
  String restaurantLogo;
  String restaurantName;
  String restaurantType;
  String stateId;
  int status;

  RestaurantDetails(
      {this.cityId,
        this.countryId,
        this.createdOn,
        this.email,
        this.franchiseId,
        this.id,
        this.isFranchiseRestaurant,
        this.leadId,
        this.location,
        this.resType,
        this.restauranPhNo,
        this.restaurantDescription,
        this.restaurantId,
        this.restaurantLogo,
        this.restaurantName,
        this.restaurantType,
        this.stateId,
        this.status});

  RestaurantDetails.fromJson(Map<String, dynamic> json) {
    cityId = json['cityId'];
    countryId = json['countryId'];
    createdOn = json['createdOn'];
    email = json['email'];
    franchiseId = json['franchiseId'];
    id = json['id'];
    isFranchiseRestaurant = json['isFranchiseRestaurant'];
    leadId = json['leadId'];
    location = json['location'];
    resType = json['resType'];
    restauranPhNo = json['restauranPhNo'];
    restaurantDescription = json['restaurantDescription'];
    restaurantId = json['restaurantId'];
    restaurantLogo = json['restaurantLogo'];
    restaurantName = json['restaurantName'];
    restaurantType = json['restaurantType'];
    stateId = json['stateId'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cityId'] = this.cityId;
    data['countryId'] = this.countryId;
    data['createdOn'] = this.createdOn;
    data['email'] = this.email;
    data['franchiseId'] = this.franchiseId;
    data['id'] = this.id;
    data['isFranchiseRestaurant'] = this.isFranchiseRestaurant;
    data['leadId'] = this.leadId;
    data['location'] = this.location;
    data['resType'] = this.resType;
    data['restauranPhNo'] = this.restauranPhNo;
    data['restaurantDescription'] = this.restaurantDescription;
    data['restaurantId'] = this.restaurantId;
    data['restaurantLogo'] = this.restaurantLogo;
    data['restaurantName'] = this.restaurantName;
    data['restaurantType'] = this.restaurantType;
    data['stateId'] = this.stateId;
    data['status'] = this.status;
    return data;
  }
}

