import 'SendTaxRatesModel.dart';
import 'getitemmodifierssubmitresponse.dart';

class GetCartItems {
  List<MenuCartItemapi> menuItem;

  GetCartItems({this.menuItem});

  GetCartItems.fromJson(Map<String, dynamic> json) {
    if (json['menuItem'] != null) {
      menuItem = new List<MenuCartItemapi>();
      json['menuItem'].forEach((v) {
        menuItem.add(new MenuCartItemapi.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.menuItem != null) {
      data['menuItem'] = this.menuItem.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class MenuCartItemapi {
  String itemId;
  String menuId;
  String menuGroupId;
  String itemName;
  String unitPrice;
  String quantity;
  String totalPrice;
  List<ModifierListApi> modifiersList;
  List<SpecialRequestListApi> specialRequestList;
  String itemType;
  String discountId;
  String discountName;
  String discountLevel;
  String discountType;
  int discountValue;
  double discountAmount;
  String comboId;
  String comboUniqueId;
  bool isCombo;
  String bogoId;
  String bogoUniqueId;
  bool isBogo;
  bool discountApplicable;
  bool discountApplied;
  String item_text_desc;
  List<SendTaxRatesModel> taxesList;

//  List<SizeList> sizeList;
  // List<TimePriceList> timePriceList;

  MenuCartItemapi(
      this.itemId,
      this.menuId,
      this.menuGroupId,
      this.itemName,
      this.unitPrice,
      this.quantity,
      this.totalPrice,
      this.item_text_desc,
      this.modifiersList,
      this.specialRequestList,
      this.itemType,
      this.discountId,
      this.discountName,
      this.discountLevel,
      this.discountType,
      this.discountValue,
      this.discountAmount,
      this.comboId,
      this.comboUniqueId,
      this.isCombo,
      this.bogoId,
      this.bogoUniqueId,
      this.isBogo,
      this.discountApplicable,
      this.discountApplied,
      this.taxesList
      );

  MenuCartItemapi.fromJson(Map<String, dynamic> json) {
    itemId = json['itemId'];
    menuId = json['menuId'];
    menuGroupId = json['menuGroupId'];
    itemName = json['itemName'] as String;
    unitPrice = json['unitPrice'] as String;
    quantity = json['quantity'] as String;
    totalPrice = json['totalPrice'] as String;

    item_text_desc = json['item_text_desc'] as String;

    if (json['modifiersList'] != null) {
      modifiersList = new List<ModifierListApi>();
      json['modifiersList'].forEach((v) {
        modifiersList.add(new ModifierListApi.fromJson(v));
      });
    } else {
      modifiersList = new List<ModifierListApi>();
      modifiersList = [];
    }
    if (json['specialRequestList'] != null) {
      specialRequestList = new List<SpecialRequestListApi>();
      json['specialRequestList'].forEach((v) {
        specialRequestList.add(new SpecialRequestListApi.fromJson(v));
      });
    } else {
      specialRequestList = new List<SpecialRequestListApi>();
      specialRequestList = [];
    }

    itemType = json['itemType'] as String;

    discountId = json['discountId'] as String;
    discountName = json['discountName'] as String;
    discountLevel = json['discountLevel'] as String;
    discountType = json['discountType'] as String;
    discountValue = json['discountValue'] as int;
    discountAmount = json['discountAmount'] as double;
    comboId = json['comboId'] as String;
    comboUniqueId = json['comboUniqueId'] as String;
    isCombo = json['isCombo'] as bool;
    bogoId = json['bogoId'] as String;
    bogoUniqueId = json['bogoUniqueId'] as String;
    isBogo = json['isBogo'] as bool;
    discountApplicable = json['discountApplicable'] as bool;
    discountApplied = json['discountApplied'] as bool;
    if (json['taxesList'] != null) {
      taxesList = new List<SendTaxRatesModel>();
      json['taxesList'].forEach((v) {
        taxesList.add(new SendTaxRatesModel.fromJson(v));
      });
    } else {
      taxesList = new List<SendTaxRatesModel>();
      taxesList = [];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['itemId'] = this.itemId;
    data['menuId'] = this.menuId;
    data['menuGroupId'] = this.menuGroupId;
    data['itemName'] = this.itemName;
    data['unitPrice'] = this.unitPrice;
    data['quantity'] = this.quantity;
    data['totalPrice'] = this.totalPrice;
    data['item_text_desc'] = this.item_text_desc;
    if (this.modifiersList != null) {
      data['modifiersList'] = this.modifiersList.map((v) => v.toJson()).toList();
    }
    if (this.specialRequestList != null) {
      data['specialRequestList'] = this.specialRequestList.map((v) => v.toJson()).toList();
    }

    data['itemType'] = this.itemType;
    data['discountId'] = this.discountId;
    data['discountName'] = this.discountName;
    data['discountLevel'] = this.discountLevel;
    data['discountType'] = this.discountType;
    data['discountValue'] = this.discountValue;
    data['discountAmount'] = this.discountAmount;
    data['comboId'] = this.comboId;
    data['comboUniqueId'] = this.comboUniqueId;
    data['isCombo'] = this.isCombo;
    data['bogoId'] = this.bogoId;
    data['bogoUniqueId'] = this.bogoUniqueId;
    data['isBogo'] = this.isBogo;
    data['discountApplicable'] = this.discountApplicable;
    data['discountApplied'] = this.discountApplied;

    if (this.taxesList != null) {
      data['taxesList'] = this.taxesList.map((v) => v.toJson()).toList();
    }
    if (this.taxesList != null) {
      data['taxesList'] = this.taxesList.map((v) => v.toJson()).toList();
    }

    return data;
  }
}

