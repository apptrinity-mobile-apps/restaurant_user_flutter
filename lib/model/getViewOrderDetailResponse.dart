
import 'package:restaurant_user/model/getitemmodifiersresponse.dart';

import 'SendTaxRatesModel.dart';

class getViewOrderdetailResponse {

  int responseStatus;
  String result;
  ViewOrderDetails viewOrderDetails;

  getViewOrderdetailResponse({this.responseStatus, this.result, this.viewOrderDetails});

  getViewOrderdetailResponse.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    viewOrderDetails = json['viewOrderDetails'] != null ? new ViewOrderDetails.fromJson(json['viewOrderDetails']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    if (this.viewOrderDetails != null) {
      data['viewOrderDetails'] = this.viewOrderDetails.toJson();
    }
    return data;
  }
}

class ViewOrderDetails {
  Null backgroundImage;
  Null bannerImage;
  String createdOn;
  String creditsUsed;
  CurbsideInfo curbsideInfo;
  String deliveryFee;
  String deliveryInstructions;
  int deliveryStatus;
  String dineInBehaviour;
  String dineInOption;
  String dineInOptionName;
  String discountAmount;
  String id;
  bool isOnlineOrder;
  List<OrderItems> orderItems;
  int orderNumber;
  int paymentStatus;
  String restaurantId;
  String restaurantLogo;
  List<ServiceCharges> serviceCharges;
  String serviceChargesAmount;
  int status;
  String subTotal;
  String taxAmount;
  String tipAmount;
  String totalAmount;

  ViewOrderDetails({this.backgroundImage, this.bannerImage, this.createdOn, this.creditsUsed, this.curbsideInfo, this.deliveryFee, this.deliveryInstructions, this.deliveryStatus, this.dineInBehaviour, this.dineInOption, this.dineInOptionName, this.discountAmount, this.id, this.isOnlineOrder, this.orderItems, this.orderNumber, this.paymentStatus, this.restaurantId, this.restaurantLogo, this.serviceCharges, this.serviceChargesAmount, this.status, this.subTotal, this.taxAmount, this.tipAmount, this.totalAmount});

  ViewOrderDetails.fromJson(Map<String, dynamic> json) {
    backgroundImage = json['backgroundImage'];
    bannerImage = json['bannerImage'];
    createdOn = json['createdOn'];
    creditsUsed = json['creditsUsed'];
    curbsideInfo = json['curbsideInfo'] != null ? new CurbsideInfo.fromJson(json['curbsideInfo']) : null;
    deliveryFee = json['deliveryFee'];
    deliveryInstructions = json['deliveryInstructions'];
    deliveryStatus = json['deliveryStatus'];
    dineInBehaviour = json['dineInBehaviour'];
    dineInOption = json['dineInOption'];
    dineInOptionName = json['dineInOptionName'];
    discountAmount = json['discountAmount'];
    id = json['id'];
    isOnlineOrder = json['isOnlineOrder'];
    if (json['orderItems'] != null) {
      orderItems = new List<OrderItems>();
      json['orderItems'].forEach((v) { orderItems.add(new OrderItems.fromJson(v)); });
    }
    orderNumber = json['orderNumber'];
    paymentStatus = json['paymentStatus'];
    restaurantId = json['restaurantId'];
    restaurantLogo = json['restaurantLogo'];
    if (json['serviceCharges'] != null) {
      serviceCharges = new List<ServiceCharges>();
      json['serviceCharges'].forEach((v) { serviceCharges.add(new ServiceCharges.fromJson(v)); });
    }
    serviceChargesAmount = json['serviceChargesAmount'];
    status = json['status'];
    subTotal = json['subTotal'];
    taxAmount = json['taxAmount'];
    tipAmount = json['tipAmount'];
    totalAmount = json['totalAmount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['backgroundImage'] = this.backgroundImage;
    data['bannerImage'] = this.bannerImage;
    data['createdOn'] = this.createdOn;
    data['creditsUsed'] = this.creditsUsed;
    if (this.curbsideInfo != null) {
      data['curbsideInfo'] = this.curbsideInfo.toJson();
    }
    data['deliveryFee'] = this.deliveryFee;
    data['deliveryInstructions'] = this.deliveryInstructions;
    data['deliveryStatus'] = this.deliveryStatus;
    data['dineInBehaviour'] = this.dineInBehaviour;
    data['dineInOption'] = this.dineInOption;
    data['dineInOptionName'] = this.dineInOptionName;
    data['discountAmount'] = this.discountAmount;
    data['id'] = this.id;
    data['isOnlineOrder'] = this.isOnlineOrder;
    if (this.orderItems != null) {
      data['orderItems'] = this.orderItems.map((v) => v.toJson()).toList();
    }
    data['orderNumber'] = this.orderNumber;
    data['paymentStatus'] = this.paymentStatus;
    data['restaurantId'] = this.restaurantId;
    data['restaurantLogo'] = this.restaurantLogo;
    if (this.serviceCharges != null) {
      data['serviceCharges'] = this.serviceCharges.map((v) => v.toJson()).toList();
    }
    data['serviceChargesAmount'] = this.serviceChargesAmount;
    data['status'] = this.status;
    data['subTotal'] = this.subTotal;
    data['taxAmount'] = this.taxAmount;
    data['tipAmount'] = this.tipAmount;
    data['totalAmount'] = this.totalAmount;
    return data;
  }
}

class CurbsideInfo {


  CurbsideInfo();

CurbsideInfo.fromJson(Map<String, dynamic> json) {
}

Map<String, dynamic> toJson() {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  return data;
}
}

class OrderItems {
  String discountAmount;
  String discountType;
  String discountValue;
  String itemId;
  String itemImage;
  String itemName;
  List<ModifiersList> modifiersList;
  int quantity;
  List<SpecialRequestList> specialRequestList;
  bool taxIncludeOption;
  String totalPrice;
  String unitPrice;

  OrderItems({this.discountAmount, this.discountType, this.discountValue, this.itemId, this.itemImage, this.itemName, this.modifiersList, this.quantity, this.specialRequestList, this.taxIncludeOption, this.totalPrice, this.unitPrice});

  OrderItems.fromJson(Map<String, dynamic> json) {
    discountAmount = json['discountAmount'];
    discountType = json['discountType'];
    discountValue = json['discountValue'];
    itemId = json['itemId'];
    itemImage = json['itemImage'];
    itemName = json['itemName'];
    if (json['modifiersList'] != null) {
      modifiersList = new List<ModifiersList>();
      json['modifiersList'].forEach((v) { modifiersList.add(new ModifiersList.fromJson(v)); });
    }
    quantity = json['quantity'];
    if (json['specialRequestList'] != null) {
      specialRequestList = new List<SpecialRequestList>();
      json['specialRequestList'].forEach((v) { specialRequestList.add(new SpecialRequestList.fromJson(v)); });
    }
    taxIncludeOption = json['taxIncludeOption'];
    totalPrice = json['totalPrice'];
    unitPrice = json['unitPrice'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['discountAmount'] = this.discountAmount;
    data['discountType'] = this.discountType;
    data['discountValue'] = this.discountValue;
    data['itemId'] = this.itemId;
    data['itemImage'] = this.itemImage;
    data['itemName'] = this.itemName;
    if (this.modifiersList != null) {
      data['modifiersList'] = this.modifiersList.map((v) => v.toJson()).toList();
    }
    data['quantity'] = this.quantity;
    if (this.specialRequestList != null) {
      data['specialRequestList'] = this.specialRequestList.map((v) => v.toJson()).toList();
    }
    data['taxIncludeOption'] = this.taxIncludeOption;
    data['totalPrice'] = this.totalPrice;
    data['unitPrice'] = this.unitPrice;
    return data;
  }
}

class ModifiersList {
  String modifierGroupId;
  String modifierId;
  String modifierName;
  int modifierQty;
  String modifierTotalPrice;
  String modifierUnitPrice;

  ModifiersList(
      {this.modifierGroupId,
        this.modifierId,
        this.modifierName,
        this.modifierQty,
        this.modifierTotalPrice,
        this.modifierUnitPrice});

  ModifiersList.fromJson(Map<String, dynamic> json) {
    modifierGroupId = json['modifierGroupId'];
    modifierId = json['modifierId'];
    modifierName = json['modifierName'];
    modifierQty = json['modifierQty'];
    modifierTotalPrice = json['modifierTotalPrice'];
    modifierUnitPrice = json['modifierUnitPrice'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['modifierGroupId'] = this.modifierGroupId;
    data['modifierId'] = this.modifierId;
    data['modifierName'] = this.modifierName;
    data['modifierQty'] = this.modifierQty;
    data['modifierTotalPrice'] = this.modifierTotalPrice;
    data['modifierUnitPrice'] = this.modifierUnitPrice;
    return data;
  }
}
class SpecialRequestList {
  String name;
  int requestPrice;

  SpecialRequestList(this.name, this.requestPrice);

  SpecialRequestList.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    requestPrice = json['requestPrice'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['requestPrice'] = this.requestPrice;
    return data;
  }
}

class ServiceCharges {
  String name;
  int requestPrice;

  ServiceCharges(this.name, this.requestPrice);

  ServiceCharges.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    requestPrice = json['requestPrice'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['requestPrice'] = this.requestPrice;
    return data;
  }
}
