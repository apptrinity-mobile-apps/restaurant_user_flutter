class DeliveryInfo {
  String notes;
  String id;
  String latitude;
  String longitude;
  String addressTag;
  String fullAddress;

  DeliveryInfo(this.notes, this.id, this.latitude, this.longitude, this.addressTag, this.fullAddress);

  DeliveryInfo.fromJson(Map<String, dynamic> json) {
    notes = json['notes'];
    id = json['id'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    addressTag = json['addressTag'];
    fullAddress = json['fullAddress'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['notes'] = this.notes;
    data['id'] = this.id;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['addressTag'] = this.addressTag;
    data['fullAddress'] = this.fullAddress;
    return data;
  }
}