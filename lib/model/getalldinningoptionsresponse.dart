class getalldinningoptionsresponse {
  List<DineInList> dineInList;
  int responseStatus;
  String result;

  getalldinningoptionsresponse({this.dineInList, this.responseStatus, this.result});

  getalldinningoptionsresponse.fromJson(Map<String, dynamic> json) {
    if (json['dineInList'] != null) {
      dineInList = new List<DineInList>();
      json['dineInList'].forEach((v) {
        dineInList.add(new DineInList.fromJson(v));
      });
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.dineInList != null) {
      data['dineInList'] = this.dineInList.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class DineInList {
  String behavior;
  String id;
  String optionName;

  DineInList({this.behavior, this.id, this.optionName});

  DineInList.fromJson(Map<String, dynamic> json) {
    behavior = json['behavior'];
    id = json['id'];
    optionName = json['optionName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['behavior'] = this.behavior;
    data['id'] = this.id;
    data['optionName'] = this.optionName;
    return data;
  }
}


