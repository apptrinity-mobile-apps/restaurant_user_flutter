class Getmenugroups {
  List<MenugroupsList> menugroupsList;
  int responseStatus;
  String result;

  Getmenugroups({this.menugroupsList, this.responseStatus, this.result});

  Getmenugroups.fromJson(Map<String, dynamic> json) {
    if (json['menugroupsList'] != null) {
      menugroupsList = new List<MenugroupsList>();
      json['menugroupsList'].forEach((v) {
        menugroupsList.add(new MenugroupsList.fromJson(v));
      });
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.menugroupsList != null) {
      data['menugroupsList'] =
          this.menugroupsList.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class MenugroupsList {
  String id;
  String name;

  MenugroupsList({this.id, this.name});

  MenugroupsList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }
}