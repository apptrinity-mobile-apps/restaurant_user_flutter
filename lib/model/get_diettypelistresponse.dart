

class GetDietTypeListresponse {
  List<DietTypeLists> dietTypeLists;
  int responseStatus;
  String result;

  GetDietTypeListresponse({this.dietTypeLists, this.responseStatus, this.result});

  GetDietTypeListresponse.fromJson(Map<String, dynamic> json) {
    if (json['dietTypeLists'] != null) {
      dietTypeLists = new List<DietTypeLists>();
      json['dietTypeLists'].forEach((v) {
        dietTypeLists.add(new DietTypeLists.fromJson(v));
      });
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.dietTypeLists != null) {
      data['dietTypeLists'] =
          this.dietTypeLists.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class DietTypeLists {
  String createdOn;
  String id;
  int index;
  String name;
  int status;
  String userId;

  DietTypeLists(
      {this.createdOn,
        this.id,
        this.index,
        this.name,
        this.status,
        this.userId});

  DietTypeLists.fromJson(Map<String, dynamic> json) {
    createdOn = json['createdOn'];
    id = json['id'];
    index = json['index'];
    name = json['name'];
    status = json['status'];
    userId = json['userId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdOn'] = this.createdOn;
    data['id'] = this.id;
    data['index'] = this.index;
    data['name'] = this.name;
    data['status'] = this.status;
    data['userId'] = this.userId;
    return data;
  }
}


