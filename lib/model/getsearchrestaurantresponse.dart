
class getsearchrestaurantresponse {
  int responseStatus;
  List<RestaurantsList> restaurantsList;
  String result;

  getsearchrestaurantresponse({this.responseStatus, this.restaurantsList, this.result});

  getsearchrestaurantresponse.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    if (json['restaurantsList'] != null) {
      restaurantsList = new List<RestaurantsList>();
      json['restaurantsList'].forEach((v) {
        restaurantsList.add(new RestaurantsList.fromJson(v));
      });
    }
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['responseStatus'] = this.responseStatus;
    if (this.restaurantsList != null) {
      data['restaurantsList'] = this.restaurantsList.map((v) => v.toJson()).toList();
    }
    data['result'] = this.result;
    return data;
  }
}

class RestaurantsList {
  String id;
  String restaurantId;
  String restaurantLogo;
  String restaurantName;

  RestaurantsList({this.id, this.restaurantId, this.restaurantLogo, this.restaurantName});

  RestaurantsList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    restaurantId = json['restaurantId'];
    restaurantLogo = json['restaurantLogo'] == null ?"":json['restaurantLogo'];
    restaurantName = json['restaurantName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['restaurantId'] = this.restaurantId;
    data['restaurantLogo'] = this.restaurantLogo;
    data['restaurantName'] = this.restaurantName;
    return data;
  }
}
