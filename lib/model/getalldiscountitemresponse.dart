class getalldiscountitemresponse {

  List<Discounts> discounts;
  int responseStatus;
  String result;

  getalldiscountitemresponse({this.discounts, this.responseStatus, this.result});

  getalldiscountitemresponse.fromJson(Map<String, dynamic> json) {
  if (json['discounts'] != null) {
  discounts = new List<Discounts>();
  json['discounts'].forEach((v) {
  discounts.add(new Discounts.fromJson(v));
  });
  }
  responseStatus = json['responseStatus'];
  result = json['result'];
  }

  Map<String, dynamic> toJson() {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  if (this.discounts != null) {
  data['discounts'] = this.discounts.map((v) => v.toJson()).toList();
  }
  data['responseStatus'] = this.responseStatus;
  data['result'] = this.result;
  return data;
  }
  }

  class Discounts {
  bool allowOtherDiscounts;
  int appliesTo;
  String discountValueType;
  String id;
  double maxDiscountAmount;
  double minDiscountAmount;
  String name;
  int orderValue;
  int status;
  String type;
  String uniqueNumber;
  double value;

  Discounts(
  {this.allowOtherDiscounts,
  this.appliesTo,
  this.discountValueType,
  this.id,
  this.maxDiscountAmount,
  this.minDiscountAmount,
  this.name,
  this.orderValue,
  this.status,
  this.type,
  this.uniqueNumber,
  this.value});

  Discounts.fromJson(Map<String, dynamic> json) {
  allowOtherDiscounts = json['allowOtherDiscounts'];
  appliesTo = json['appliesTo'];
  discountValueType = json['discountValueType'];
  id = json['id'];
  maxDiscountAmount = json['maxDiscountAmount'] as double;
  minDiscountAmount = json['minDiscountAmount']as double;
  name = json['name'];
  orderValue = json['orderValue'];
  status = json['status'];
  type = json['type'];
  uniqueNumber = json['uniqueNumber'];
  value = json['value'];
  }

  Map<String, dynamic> toJson() {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['allowOtherDiscounts'] = this.allowOtherDiscounts;
  data['appliesTo'] = this.appliesTo;
  data['discountValueType'] = this.discountValueType;
  data['id'] = this.id;
  data['maxDiscountAmount'] = this.maxDiscountAmount;
  data['minDiscountAmount'] = this.minDiscountAmount;
  data['name'] = this.name;
  data['orderValue'] = this.orderValue;
  data['status'] = this.status;
  data['type'] = this.type;
  data['uniqueNumber'] = this.uniqueNumber;
  data['value'] = this.value as double;
  return data;
  }

}
