
class getsearchrestaurantmenuresponse {
  List<MenuList> menuList;
  int responseStatus;
  String result;

  getsearchrestaurantmenuresponse({this.menuList, this.responseStatus, this.result});

  getsearchrestaurantmenuresponse.fromJson(Map<String, dynamic> json) {
    if (json['menuList'] != null) {
      menuList = new List<MenuList>();
      json['menuList'].forEach((v) {
        menuList.add(new MenuList.fromJson(v));
      });
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.menuList != null) {
      data['menuList'] = this.menuList.map((v) => v.toJson()).toList();
    }
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class MenuList {
  String id;
  String menuName;

  MenuList({this.id, this.menuName});

  MenuList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    menuName = json['menuName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['menuName'] = this.menuName;
    return data;
  }
}
