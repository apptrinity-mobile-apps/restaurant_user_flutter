class loginresponse {

  int responseStatus;
  String result;
  UserData userData;

  loginresponse({this.responseStatus, this.result, this.userData});

  loginresponse.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    userData = json['userData'] != null
        ? new UserData.fromJson(json['userData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    if (this.userData != null) {
      data['userData'] = this.userData.toJson();
    }
    return data;
  }
}

class UserData {
  Address address;
  String email;
  String firstName;
  String id;
  String lastName;
  String phoneNumber;
  int preCardNo;
  int status;

  UserData(
      {this.address,
        this.email,
        this.firstName,
        this.id,
        this.lastName,
        this.phoneNumber,
        this.preCardNo,
        this.status});

  UserData.fromJson(Map<String, dynamic> json) {
    address =
    json['address'] != null ? new Address.fromJson(json['address']) : null;
    email = json['email'];
    firstName = json['firstName'];
    id = json['id'];
    lastName = json['lastName'];
    phoneNumber = json['phoneNumber'];
    preCardNo = json['preCardNo'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.address != null) {
      data['address'] = this.address.toJson();
    }
    data['email'] = this.email;
    data['firstName'] = this.firstName;
    data['id'] = this.id;
    data['lastName'] = this.lastName;
    data['phoneNumber'] = this.phoneNumber;
    data['preCardNo'] = this.preCardNo;
    data['status'] = this.status;
    return data;
  }
}

class Address {
  String firstName;

  Address({this.firstName});

  Address.fromJson(Map<String, dynamic> json) {
    firstName = json['firstName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['firstName'] = this.firstName;
    return data;
  }
}
