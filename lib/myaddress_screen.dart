import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:restaurant_user/apis/deleteUserAddress.dart';
import 'package:restaurant_user/apis/getAddressLists.dart';
import 'package:restaurant_user/editaddress_screen.dart';
import 'package:restaurant_user/session/userRepository.dart';
import 'package:restaurant_user/utils/all_constans.dart';
import 'package:restaurant_user/utils/back_press.dart';
import 'package:restaurant_user/utils/sizeconfig.dart';
import 'package:restaurant_user/utils/snack_bars.dart';
import 'package:toast/toast.dart';

import 'addaddress_screen.dart';
import 'appbar_cart.dart';
import 'model/get_addresslistresponse.dart';

class MyAddressScreen extends StatefulWidget {
  final String current_latitude;
  final String current_longitude;
  final String current_location;

  const MyAddressScreen(this.current_latitude, this.current_longitude, this.current_location, {Key key}) : super(key: key);

  @override
  _MyAddressScreenState createState() => _MyAddressScreenState();
}

class _MyAddressScreenState extends State<MyAddressScreen> {
  int cart_count = 0;
  bool enable_searchlist = false;
  String employee_id = "";
  String first_name = "";
  String last_name = "";
  String restaurant_id = "";
  String servicearea_id = "";
  String user_id = "";
  double screenheight = 0.0;
  String returnVal = "";
  bool hasTimerStopped = false;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  String showgoutview = "order_location_icon.png";
  String orderscanner = "order_scanner.png";
  String restaurantsquare = "restaurant_square.png";
  String noorders = "no_order_dash.png";
  TextEditingController search_Controller = TextEditingController();
  List<UserAddressBookList> getAddressLists = new List();
  bool isSwitched = false;
  bool isSwitchedveg = false;

  bool _loading = true;

  int selected_pos = 0;
  String menu_id = "";
  String menu_groupid = "";
  List<String> selectedCheckbox = [];

  int selected = 0;
  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();

    UserRepository().getuserdetails().then((getuserdetails) {
      setState(() {
        user_id = getuserdetails[0];
        getSavedAddresses();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    //  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: login_passcode_bg1));
    print("Height" + SizeConfig.screenHeight.toString());
    final space = SizedBox(height: 20);
    if (SizeConfig.screenHeight >= 600 && SizeConfig.screenHeight < 800) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 550 && SizeConfig.screenHeight < 600) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 800) {
      screenheight = 550;
    }
    final screen_height = MediaQuery.of(context).size.height;
    print(screen_height);

    return WillPopScope(
        onWillPop: () async => onBackPressed(context),
        child:Scaffold(
        key: _scaffoldkey,
        resizeToAvoidBottomInset: true,
        backgroundColor: Colors.white,
        appBar: AppBar(
          toolbarHeight: 56,
          //toolbarHeight:  height-30.00,
          automaticallyImplyLeading: false,
          elevation: 0.0,
          backgroundColor: Colors.white,
          centerTitle: true,
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                padding: EdgeInsets.only(left: 12.0),
                icon: Image.asset("images/back_arrow.png", width: 20, height: 20),
                onPressed: () {
                  onBackPressed(context);
                },
              );
            },
          ),
          actions: [

          ],
        ),
        body: _loading
            ? Center(
                child: SpinKitCircle(color: Colors.lightBlueAccent),
              )
            :  Container(
                  margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  //height: SizeConfig.safeBlockHorizontal * 155,
                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisSize: MainAxisSize.min, children: [
                    Padding(
                        padding: EdgeInsets.fromLTRB(10, 0, 0, 5),
                        child: Text(
                          "My Address",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontSize: 20,
                            color: login_passcode_text,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w800,
                          ),
                        )),
                    Container(
                        margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                        width: double.infinity,
                        //height: SizeConfig.safeBlockHorizontal * 155,
                        child: Container(
                            // height: 50,
                            margin: EdgeInsets.fromLTRB(10, 5, 15, 0),
                            // alignment: Alignment.center,
                            decoration: BoxDecoration(color: receipt_header, borderRadius: BorderRadius.circular(5)),
                            child: TextButton(
                              child: Text("+ ADD ADDRESS",
                                  style: TextStyle(
                                      fontSize: SizeConfig.safeBlockHorizontal * 4,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w800,
                                      color: Colors.white)),
                              onPressed: () {
                                Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => AddAddressScreen("", "", ""))).then((value) {
                                  getSavedAddresses();
                                });
                              },
                            ))),
                    getAddressLists.length > 0
                        ? Expanded(child: ListView.builder(
                            physics: ClampingScrollPhysics(),
                            shrinkWrap: true,
                            primary: false,
                            scrollDirection: Axis.vertical,
                            itemBuilder: (context, index) {
                              return Card(
                                      margin: EdgeInsets.fromLTRB(15, 10, 15, 0),
                                      color: Colors.white,
                                      elevation: 3,
                                      child: ListTile(
                                        title: Container(
                                            margin: EdgeInsets.fromLTRB(5, 10, 10, 10),
                                            // alignment: Alignment.center,
                                            child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                                              Padding(
                                                  padding: EdgeInsets.fromLTRB(5, 5, 0, 5),
                                                  child: Text(
                                                    getAddressLists[index].addressType,
                                                    textAlign: TextAlign.start,
                                                    style: TextStyle(
                                                      fontSize: 18,
                                                      color: login_passcode_text,
                                                      fontFamily: 'Poppins',
                                                      fontWeight: FontWeight.w600,
                                                    ),
                                                  )),
                                              Padding(
                                                  padding: EdgeInsets.fromLTRB(5, 5, 0, 5),
                                                  child: Text(
                                                    getAddressLists[index].fullAddress,
                                                    textAlign: TextAlign.start,
                                                    style: TextStyle(
                                                      fontSize: 16,
                                                      color: login_passcode_text,
                                                      fontFamily: 'Poppins',
                                                      fontWeight: FontWeight.w400,
                                                    ),
                                                  )),

                                              /*Container(
                                  margin: EdgeInsets.fromLTRB(5, 20, 5, 10),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(color: login_passcode_text, borderRadius: BorderRadius.circular(5)),
                                  child: InkWell(
                                      child: FlatButton(
                                          child: Text("ADD DELIVERY INSTRUCTIONS",
                                              style: TextStyle(fontSize: SizeConfig.safeBlockHorizontal * 4, fontFamily: 'Poppins', fontWeight: FontWeight.w400, color: Colors.white)),
                                          onPressed: () {}))),*/
                                            ]),
                                          ),
                                        trailing: PopupMenuButton(
                                          icon: Padding(
                                            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                            child: Image.asset(
                                              'images/dots.png',
                                              height: 55,
                                              width: 25,
                                            ),
                                          ),
                                          itemBuilder: (context) {
                                            return [
                                              PopupMenuItem(
                                                value: 'edit',
                                                child: Text('Edit'),
                                              ),
                                              PopupMenuItem(
                                                value: 'delete',
                                                child: Text('Delete'),
                                              )
                                            ];
                                          },
                                          onSelected: (String value) => actionPopUpItemSelected(value, index),
                                        ),
                                      ));
                            },
                            itemCount: getAddressLists.length,
                          ))
                        : Container(
                            margin: EdgeInsets.fromLTRB(0, 100, 0, 0),
                            alignment: Alignment.center,
                            padding: EdgeInsets.only(left: 0, top: 80),
                            child: Text("No Addresses Found",
                                style: TextStyle(color: login_passcode_text, fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w600)),
                          ),
                  ]),
                ),
              ));
  }

  void actionPopUpItemSelected(String value, int index) {
    _scaffoldkey.currentState.hideCurrentSnackBar();
    String message;
    if (value == 'edit') {
      // message = 'You selected edit for $name';

      Navigator.of(context).push(MaterialPageRoute(
          builder: (BuildContext context) => EditAddressScreen(
                getAddressLists[index].addressType,
                getAddressLists[index].fullAddress,
                getAddressLists[index].geoLocation.toString(),
                getAddressLists[index].latitude,
                getAddressLists[index].longitude,
                getAddressLists[index].floor,
                getAddressLists[index].howToReach,
                getAddressLists[index].isDefault,
                getAddressLists[index].id,
              )));
    } else if (value == 'delete') {
      //message = 'You selected delete for $name';
        DeleteAddressRepository().deleteUserAddress(user_id, getAddressLists[index].id).then((value) {
          print(value);
          if (value.responseStatus == 0) {
            // Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
            Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
          } else if (value.responseStatus == 1) {
            // Navigator.of(_keyLoader.currentContext, rootNavigator: false).pop();
            Toast.show("Address Deleted Successfully", context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
            setState(() {
              _loading = true;
              getSavedAddresses();
            });
            // Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => MyAddressScreen("", "", "")));
          } else {
            Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
          }
      });
    } else {
      message = 'Not implemented';
    }
    snackBarLight(context, message);
  }

  void getSavedAddresses() {
    AddressListRepository().getAddressLists(user_id).then((result_addresslist) {
      setState(() {
        _loading = false;
        getAddressLists = [];
        getAddressLists = result_addresslist;
        print("ADDRESSSLENGTH=====" + getAddressLists.length.toString());
      });
    });
  }
}
