import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:restaurant_user/login_screen.dart';
import 'package:restaurant_user/session/userRepository.dart';
import 'package:restaurant_user/signup_screen.dart';

import 'dashboard.dart';
import 'location_search.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  String is_Login_session = "";

  @override
  void initState() {
    super.initState();

    //login required
    /*UserRepository().getuserdetails().then((getIsLoginWaiter) {
      setState(() {
        is_Login_session = getIsLoginWaiter[5];
        print("ISLOGIN=====" + is_Login_session.toString());
        if (is_Login_session == "true") {
          Timer(
              Duration(seconds: 3),
              () => Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(builder: (context) => LocationSearch()),
                  ));
        } else {
          Timer(
              Duration(seconds: 3),
              () => Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(builder: (context) => LoginPage()),
                  ));
        }
      });
    });*/

    // no login required
    Timer(
        Duration(seconds: 3),
            () => Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => LocationSearch()), (Route<dynamic> route) => false),
        );

    /* UserRepository().getRestaurant_id().then((getIsLogin) {
      setState(() {
        UserRepository().getuserdetails().then((getIsLoginWaiter){
          setState((){
        is_Login_session = getIsLogin[1];
        is_Login_session_waiter = getIsLoginWaiter[3];
        print("ISLOGIN=====" + is_Login_session.toString() +"------"+is_Login_session_waiter);
        if(is_Login_session == "true"){
          if(is_Login_session_waiter == "true"){
            Timer(
                Duration(seconds: 3),
                    () => Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => HomePage()),
                ));
          }else if(is_Login_session_waiter == ""){
            Timer(
                Duration(seconds: 3),
                    () => Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyApp()),
                ));
          }else{
            Timer(
                Duration(seconds: 3),
                    () => Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyApp()),
                ));
          }
        }else if(is_Login_session == ""){
          print("ISFALSE=====" + is_Login_session.toString());
          Timer(
              Duration(seconds: 3),
                  () => Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (BuildContext context) => RestauranLoginCode())));
        }else{
          Timer(
              Duration(seconds: 3),
                  () => Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (BuildContext context) => RestauranLoginCode())));
        }

          });
        });

      });
    });*/
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        body: Container(
          decoration: BoxDecoration(image: DecorationImage(image: ExactAssetImage('images/splash_bg.png'), fit: BoxFit.cover)),
          child: Center(
            child: Image(
              alignment: Alignment.center,
              height: 150,
              width: 150,
              image: AssetImage('images/splash_logo.png'),
              //fit: BoxFit.fitWidth,
              //width: double.infinity,
            ),
          ),
        ));
  }
}
