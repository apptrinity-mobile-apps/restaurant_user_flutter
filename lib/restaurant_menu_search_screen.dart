import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:restaurant_user/apis/getSearchRestaurantMenuApi.dart';
import 'package:restaurant_user/apis/getSearchRestaurantsApi.dart';
import 'package:restaurant_user/model/getsearchrestaurantmenuresponse.dart';
import 'package:restaurant_user/restaurantdetail_screen.dart';
import 'package:restaurant_user/session/userRepository.dart';
import 'package:restaurant_user/showorders_screen.dart';
import 'package:restaurant_user/utils/all_constans.dart';
import 'package:restaurant_user/utils/back_press.dart';
import 'package:toast/toast.dart';

import 'utils/dottedline.dart';
import 'model/get_restaurantsresponse.dart';
import 'model/getsearchrestaurantresponse.dart';

class RestaurantMenuSearchScreen extends StatefulWidget {
  @override
  _RestaurantMenuSearchScreenState createState() => new _RestaurantMenuSearchScreenState();
}

class _RestaurantMenuSearchScreenState extends State<RestaurantMenuSearchScreen> {
  TextEditingController editingController = TextEditingController();

  final duplicateItems = List<String>.generate(10000, (i) => "Item $i");
  var items = List<String>();
  bool _loading = true;
  List<MenuList> __search_items_list = new List();

  String session_latitude = "";
  String session_longitude = "";
  String session_location = "";
  String res_id = "612f13ad5a69ba9c1950761b";

  @override
  void initState() {
    super.initState();

    UserRepository().getlocationdetails().then((getlocation) {
      setState(() {
        session_latitude = getlocation[0];
        session_longitude = getlocation[1];
        session_location = getlocation[2];
      });
    });

    GetSearchRestaurantMenuApi().searchrestaurantmenubynmae(res_id, "").then((value) {
      print("SEARCHRESTAURANT RESPSTATUS  " + value.responseStatus.toString());
      debugPrint(base_url + "------" + value.responseStatus.toString() + "-------" + value.result.toString() + "-----" + value.menuList.toString());
      if (value.responseStatus == 1) {
        setState(() {
          _loading = false;

          for (int t = 0; t < value.menuList.length; t++) {
            __search_items_list.add(value.menuList[t]);
          }
        });
      } else if (value.responseStatus == 0) {
        setState(() {
          _loading = false;
          Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
        });
      } else {
        setState(() {
          _loading = false;
          Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => onBackPressed(context),
        child: Scaffold(
      body: Container(
        margin: EdgeInsets.fromLTRB(10, 30, 10, 0),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                onChanged: (value) {
                  setState(() {
                    GetSearchRestaurantMenuApi().searchrestaurantmenubynmae(res_id, value).then((value) {
                      __search_items_list.clear();
                      print("SEARCHRESTAURANT RESPSTATUS  " + value.responseStatus.toString());
                      debugPrint(base_url +
                          "------" +
                          value.responseStatus.toString() +
                          "-------" +
                          value.result.toString() +
                          "-----" +
                          value.menuList.toString());
                      if (value.responseStatus == 1) {
                        setState(() {
                          _loading = false;
                          for (int t = 0; t < value.menuList.length; t++) {
                            __search_items_list.add(value.menuList[t]);
                          }
                        });
                      } else if (value.responseStatus == 0) {
                        setState(() {
                          _loading = false;
                          Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                        });
                      } else {
                        setState(() {
                          _loading = false;
                          Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                        });
                      }
                    });
                  });
                },
                controller: editingController,
                decoration: InputDecoration(
                    labelText: "Search",
                    hintText: "Menu name,or a dish...",
                    focusColor: location_border,
                    hoverColor: location_border,
                    prefixIcon: IconButton(
                      padding: EdgeInsets.only(left: 12.0),
                      icon: Image.asset("images/back_arrow.png", width: 20, height: 20),
                      onPressed: () {
                        Navigator.of(context).pop();
                        /*Navigator.of(context).pushReplacement(MaterialPageRoute(
                            builder: (BuildContext context) => ShowOrdersScreen(session_latitude, session_longitude, session_location)));*/
                      },
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      borderSide: BorderSide(
                        color: login_form_hint,
                        width: 1.0,
                      ),
                    )),
              ),
            ),
            Expanded(
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: __search_items_list.length,
                  itemBuilder: (context, index) {
                    if (editingController.text.isEmpty) {
                      return Container(
                        margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              print("ITEMCLICK" + __search_items_list[index].id + "=====" + __search_items_list[index].menuName);
                              Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (BuildContext context) => RestaurantDetailScreen("")));
                            });
                          },
                          child: ListTile(
                            /*leading: Padding(
                              padding: EdgeInsets.only(top: 5, left: 0, bottom: 5, right: 0),
                              child: Image.asset("images/back_arrow.png", width: 20, height: 20),
                            ),*/
                            title: Padding(
                                padding: EdgeInsets.fromLTRB(0, 0, 0, 5),
                                child: Text(
                                  __search_items_list[index].menuName,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 18,
                                    color: login_passcode_text,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.w600,
                                  ),
                                )),
                          ),
                        ),
                      );
                    } else if (__search_items_list[index].menuName.toLowerCase().contains(editingController.text) ||
                        __search_items_list[index].menuName.toLowerCase().contains(editingController.text)) {
                      return Container(
                        margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              print("ITEMCLICK" + __search_items_list[index].id + "=====" + __search_items_list[index].menuName);
                              Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (BuildContext context) => RestaurantDetailScreen("")));
                            });
                          },
                          child: ListTile(
                            leading: Padding(
                              padding: EdgeInsets.only(top: 5, left: 0, bottom: 5, right: 0),
                              child: Image.asset("images/back_arrow.png", width: 20, height: 20),
                            ),
                            title: Padding(
                                padding: EdgeInsets.fromLTRB(0, 0, 0, 5),
                                child: Text(
                                  __search_items_list[index].menuName,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 18,
                                    color: login_passcode_text,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.w600,
                                  ),
                                )),
                          ),
                        ),
                      );
                    } else {
                      return Container();
                    }
                  }),
            ),
          ],
        ),
      ),
    ));
  }
}
