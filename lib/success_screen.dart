import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:restaurant_user/appbar_cart.dart';
import 'package:restaurant_user/session/userRepository.dart';
import 'package:restaurant_user/showorders_screen.dart';
import 'package:restaurant_user/track_order.dart';
import 'package:restaurant_user/utils/all_constans.dart';
import 'package:restaurant_user/utils/back_press.dart';
import 'package:restaurant_user/utils/sizeconfig.dart';

import 'cart.dart';
import 'dashboard.dart';
import 'location_search.dart';

class SuccessScreen extends StatefulWidget {
  @override
  _SuccessScreenState createState() => _SuccessScreenState();
}

class _SuccessScreenState extends State<SuccessScreen> {
  double screenheight = 0.0;
  String session_latitude = "", session_longitude = "", session_location = "";

  @override
  void initState() {
    UserRepository().getlocationdetails().then((getlocation) {
      setState(() {
        session_latitude = getlocation[0];
        session_longitude = getlocation[1];
        session_location = getlocation[2];
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    //  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: login_passcode_bg1));
    print("Height" + SizeConfig.screenHeight.toString());
    final space = SizedBox(height: 20);
    if (SizeConfig.screenHeight >= 600 && SizeConfig.screenHeight < 800) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 550 && SizeConfig.screenHeight < 600) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 800) {
      screenheight = 550;
    }
    final screen_height = MediaQuery.of(context).size.height;
    print(screen_height);

    return WillPopScope(
        onWillPop: () async => onBackPressed(context),
        child: Scaffold(
            resizeToAvoidBottomInset: true,
            backgroundColor: Colors.white,
            /*appBar: AppBar(
          toolbarHeight: SizeConfig.safeBlockHorizontal * 25,
          //toolbarHeight:  height-30.00,
          automaticallyImplyLeading: false,
          elevation: 0.0,
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text("Your Order"),
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                padding: EdgeInsets.only(left: 12.0),
                icon: Image.asset("images/back_arrow.png", width: 20, height: 20),
                onPressed: () {
                  // Navigator.of(context).pop();
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(builder: (context) => HomePage(session_latitude, session_longitude, session_location)),
                      (Route<dynamic> route) => false);
                },
              );
            },
          ),
        ),*/
            body: SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.fromLTRB(15, 0, 15, 0),
                //height: SizeConfig.safeBlockHorizontal * 155,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Padding(
                          padding: EdgeInsets.fromLTRB(40, 40, 40, 40),
                          child: Image.asset(
                            "images/success.png",
                            fit: BoxFit.cover,
                          )),
                      Padding(
                          padding: EdgeInsets.fromLTRB(5, 20, 0, 0),
                          child: Text(
                            "Thank you!!",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 20,
                              color: login_passcode_text,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w800,
                            ),
                          )),
                      Padding(
                          padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                          child: Text(
                            "Your order has been placed",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 20,
                              color: login_passcode_text,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w800,
                            ),
                          )),
                      Container(
                          // height: 50,
                          margin: EdgeInsets.fromLTRB(10, 30, 10, 0),
                          // alignment: Alignment.center,
                          decoration: BoxDecoration(color: receipt_header, borderRadius: BorderRadius.circular(5)),
                            child: TextButton(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Padding(
                                      padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                      child: Text(
                                        "RETURN HOME",
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          fontSize: 14,
                                          color: Colors.white,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w800,
                                        ),
                                      )),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Padding(
                                          padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                                          child: Image.asset(
                                            "images/back_arrow.png",
                                            color: Colors.white,
                                            height: 20,
                                            width: 20,
                                            fit: BoxFit.cover,
                                          )),
                                    ],
                                  )
                                ],
                              ),
                              onPressed: () {
                                print("home");
                                // Navigator.push(context, MaterialPageRoute(builder: (context) => LocationSearch()));
                                Navigator.pushAndRemoveUntil(
                                    context,
                                    MaterialPageRoute(builder: (context) => HomePage(session_latitude, session_longitude, session_location)),
                                    (Route<dynamic> route) => false);
                              },
                            ),
                          ),
                      Container(
                          // height: 50,
                          margin: EdgeInsets.fromLTRB(10, 15, 10, 0),
                          // alignment: Alignment.center,
                          decoration: BoxDecoration(color: login_passcode_text, borderRadius: BorderRadius.circular(5)),
                            child: TextButton(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Padding(
                                      padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                      child: Text(
                                        "TRACK YOUR ORDER",
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          fontSize: 14,
                                          color: Colors.white,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w800,
                                        ),
                                      )),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Padding(
                                          padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                                          child: Image.asset(
                                            "images/back_arrow.png",
                                            color: Colors.white,
                                            height: 20,
                                            width: 20,
                                            fit: BoxFit.cover,
                                          )),
                                    ],
                                  )
                                ],
                              ),
                              onPressed: () {
                                print("CREDITCARD");
                                // Navigator.push(context, MaterialPageRoute(builder: (context) => TrackOrder()));
                                Navigator.pushAndRemoveUntil(
                                    context, MaterialPageRoute(builder: (context) => TrackOrder("cart")), (Route<dynamic> route) => false);
                              },
                          )),
                    ]),
              ),
            )));
  }
}
