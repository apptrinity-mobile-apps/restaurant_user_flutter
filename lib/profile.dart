import 'package:flutter/material.dart';
import 'package:restaurant_user/utils/all_constans.dart';
import 'package:restaurant_user/utils/sizeconfig.dart';

import 'utils/dottedline.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  int cart_count = 0;
  bool enable_searchlist = false;
  String userid = "";
  String employee_id = "";
  String first_name = "";
  String last_name = "";
  String restaurant_id = "";
  String servicearea_id = "";
  String user_id = "";
  double screenheight = 0.0;
  String returnVal = "";
  bool hasTimerStopped = false;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  String showgoutview = "order_location_icon.png";
  String orderscanner = "order_scanner.png";
  String noorders = "no_order_dash.png";
  TextEditingController search_Controller = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    //  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: login_passcode_bg1));
    print("Height" + SizeConfig.screenHeight.toString());
    final space = SizedBox(height: 20);
    if (SizeConfig.screenHeight >= 600 && SizeConfig.screenHeight < 800) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 550 && SizeConfig.screenHeight < 600) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 800) {
      screenheight = 550;
    }
    final screen_height = MediaQuery.of(context).size.height;
    print(screen_height);

    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.white,
      appBar: /*YourAppbar( height: SizeConfig.safeBlockHorizontal * 25,)*/
          AppBar(
        toolbarHeight: 140,
        backgroundColor: order_appbar_bg,
        title: Column(children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Image.asset(
                'images/' + showgoutview,
                height: 30,
                width: 30,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Sacramento - California",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 20,
                      color: login_passcode_text,
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  DotWidget(
                    dashColor: Colors.black,
                    dashHeight: 1,
                    dashWidth: 10,
                    emptyWidth: 5,
                    //totalWidth: 220,
                  )
                ],
              ),
              Image.asset(
                'images/' + orderscanner,
                height: 30,
                width: 30,
              )
            ],
          ),
          space,
          TextFormField(
            controller: search_Controller,
            obscureText: false,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  borderSide: BorderSide(
                    color: location_border,
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  borderSide: BorderSide(
                    color: login_form_hint,
                    width: 1.0,
                  ),
                ),
                filled: true,
                fillColor: Colors.white,
                prefixIcon: new Image.asset(
                  'images/search.png',
                  height: 10,
                  width: 10,
                  scale: 2.0,
                ),
                hintText: "Restaurant name , cuisine , or a dish...",
                hintStyle: TextStyle(
                  color: location_text,
                  fontSize: 16,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w400,
                ),
                contentPadding: EdgeInsets.only(
                  bottom: 30 / 2,
                  left: 15 / 2,
                  right: 15 / 2, // HERE THE IMPORTANT PART
                  // HERE THE IMPORTANT PART
                ),
                border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))),
          )
        ]),
      ),
      body: Container(
          margin: EdgeInsets.fromLTRB(0, 30, 0, 0),
          height: SizeConfig.safeBlockHorizontal * 100,
          child: Column(children: [
            Expanded(
              child: Image.asset(
                'images/' + noorders,
                height: 500,
                width: 500,
              ),
            ),
          ])),
    );
  }
}
