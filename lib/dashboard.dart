import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:restaurant_user/goout_screen.dart';
import 'package:restaurant_user/showorders_screen.dart';
import 'package:restaurant_user/utils/all_constans.dart';
import 'package:restaurant_user/utils/back_press.dart';
import 'package:restaurant_user/utils/sizeconfig.dart';

import 'myprofile_screen.dart';

class HomePage extends StatefulWidget {
  final String current_latitude;
  final String current_longitude;
  final String current_location;

  const HomePage(this.current_latitude, this.current_longitude, this.current_location, {Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int cart_count = 0;
  bool enable_searchlist = false;
  bool _loading = true;
  PageController _controller = new PageController();
  String userid = "";
  String employee_id = "";
  String first_name = "";
  String last_name = "";
  String restaurant_id = "";
  String servicearea_id = "";
  String user_id = "";
  double screenheight = 0.0;
  String returnVal = "";
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  String showorderview = "dashboard_order.png";
  String showgoutview = "dashboard_goout.png";
  String showprofileview = "dashboard_profile.png";
  int showorder_status = 1;
  int showgoout_status = 0;
  int showprofile_status = 0;

  var screens = [];
  int selected_pos = 0;

  @override
  void initState() {
    super.initState();

    screens = [
      ShowOrdersScreen(widget.current_latitude, widget.current_longitude, widget.current_location),
      GoOutScreen(widget.current_latitude, widget.current_longitude, widget.current_location),
      MyProfileScreen(),
    ];
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: login_passcode_bg1));
    print("Height" + SizeConfig.screenHeight.toString());
    if (SizeConfig.screenHeight >= 600 && SizeConfig.screenHeight < 800) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 550 && SizeConfig.screenHeight < 600) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 800) {
      screenheight = 550;
    }
    final screen_height = MediaQuery.of(context).size.height;
    print(screen_height);

    return SafeArea(
        child: Scaffold(
            resizeToAvoidBottomInset: true,
            backgroundColor: Colors.white,
            bottomSheet: Container(
              height: 80,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Expanded(
                      flex: 1,
                      child: InkWell(
                        onTap: () {
                          print("SHOWBUTTONCLICKED" + showorder_status.toString());
                          if (showorder_status == 0) {
                            setState(() {
                              print("SHOWBUTTONINSIDE0" + showorder_status.toString());
                              showorder_status = 1;
                              showgoout_status = 0;
                              showprofile_status = 0;
                              selected_pos = 0;
                              //  show_orders.clear();
                            });
                          }
                        },
                        child: Container(
                          color: showorder_status == 0 ? login_passcode_text : login_passcode_bg1,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                  padding: EdgeInsets.only(top: 0, bottom: 0),
                                  child: Image.asset(
                                    'images/' + showorderview,
                                    height: 36,
                                    width: 36,
                                  )),
                              Padding(
                                  padding: EdgeInsets.only(left: 0, right: 0, top: 5, bottom: 0),
                                  child: Text(
                                    "Order",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontSize: 12,
                                      color: Colors.white,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ))
                            ],
                          ),
                        ),
                      )),
                  Expanded(
                      flex: 1,
                      child: InkWell(
                        onTap: () {
                          print("SHOWBUTTONCLICKED" + showgoout_status.toString());
                          if (showgoout_status == 0) {
                            setState(() {
                              print("SHOWBUTTONINSIDE0" + showgoout_status.toString());
                              showgoout_status = 1;
                              showprofile_status = 0;
                              showorder_status = 0;
                              selected_pos = 1;
                              //  show_orders.clear();
                            });
                          }
                        },
                        child: Container(
                          color: showgoout_status == 0 ? login_passcode_text : login_passcode_bg1,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                  padding: EdgeInsets.only(top: 5, bottom: 0),
                                  child: Image.asset(
                                    'images/' + showgoutview,
                                    height: 36,
                                    width: 36,
                                  )),
                              Padding(
                                  padding: EdgeInsets.only(left: 0, right: 0, top: 5, bottom: 0),
                                  child: Text(
                                    "Go Out",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontSize: 12,
                                      color: Colors.white,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ))
                            ],
                          ),
                        ),
                      )),
                  Expanded(
                      flex: 1,
                      child: InkWell(
                        onTap: () {
                          print("SHOWBUTTONCLICKED" + showprofile_status.toString() + "=====" + selected_pos.toString());
                          if (showprofile_status == 0) {
                            setState(() {
                              print("SHOWBUTTONINSIDE0" + showprofile_status.toString());
                              showprofile_status = 1;
                              showgoout_status = 0;
                              showorder_status = 0;
                              selected_pos = 2;
                              //  show_orders.clear();
                            });
                          }
                        },
                        child: Container(
                          color: showprofile_status == 0 ? login_passcode_text : login_passcode_bg1,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                  padding: EdgeInsets.only(top: 5, bottom: 0),
                                  child: Image.asset(
                                    'images/' + showprofileview,
                                    height: 36,
                                    width: 36,
                                  )),
                              Padding(
                                  padding: EdgeInsets.only(left: 0, right: 0, top: 5, bottom: 0),
                                  child: Text(
                                    "Profile",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontSize: 12,
                                      color: Colors.white,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ))
                            ],
                          ),
                        ),
                      )),
                ],
              ),
            ),
            body: screens[selected_pos]));
  }
}
