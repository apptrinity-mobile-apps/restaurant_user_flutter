import 'dart:convert';

import 'package:restaurant_user/utils/all_constans.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserRepository {


  static Future save_userid(String userid,String firstname,String lastname,String email,String phonenumber,String isLoginUser) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(keyUserid, userid);
    prefs.setString(keyFirstName, firstname);
    prefs.setString(keyLastName, lastname);
    prefs.setString(keyEmail, email);
    prefs.setString(keyPhoneNumber, phonenumber);
    prefs.setString(keyIsLoginUser, isLoginUser);
  }
  Future<List<String>> getuserdetails()  async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final userid = prefs.getString(keyUserid) ?? "";
    final firstname = prefs.getString(keyFirstName) ?? "";
    final lastname = prefs.getString(keyLastName) ?? "";
    final email = prefs.getString(keyEmail) ?? "";
    final phonenumber = prefs.getString(keyPhoneNumber) ?? "";
    final is_loginuser = prefs.getString(keyIsLoginUser) ?? "";

    List<String> user_data = [userid,firstname,lastname,email,phonenumber,is_loginuser];

    return user_data;
  }


  static Future save_location(String latitude,String longitude,String location) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(keyLatitude, latitude);
    prefs.setString(keyLongitude, longitude);
    prefs.setString(keyLocation, location);

  }

  Future<List<String>> getlocationdetails()  async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final latitude = prefs.getString(keyLatitude) ?? "";
    final longitude = prefs.getString(keyLongitude) ?? "";
    final location = prefs.getString(keyLocation) ?? "";

    List<String> location_data = [latitude,longitude,location];

    return location_data;
  }



  static Future Clearall() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.clear();
  }

  static Future removesaved_tablenumberguests() async {
    print("CLEARCART============");
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.remove('save_to_cartlist');
  }

}