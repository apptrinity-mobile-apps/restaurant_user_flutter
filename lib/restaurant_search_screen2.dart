import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:restaurant_user/apis/getSearchRestaurantsApi.dart';
import 'package:restaurant_user/restaurantdetail_screen.dart';
import 'package:restaurant_user/session/userRepository.dart';
import 'package:restaurant_user/showorders_screen.dart';
import 'package:restaurant_user/utils/all_constans.dart';
import 'package:restaurant_user/utils/back_press.dart';
import 'package:toast/toast.dart';

import 'dashboard.dart';
import 'utils/dottedline.dart';
import 'model/get_restaurantsresponse.dart';
import 'model/getsearchrestaurantresponse.dart';

class RestaurantSearchScreen2 extends StatefulWidget {
  @override
  _RestaurantSearchScreen2State createState() => new _RestaurantSearchScreen2State();
}

class _RestaurantSearchScreen2State extends State<RestaurantSearchScreen2> {
  TextEditingController editingController = TextEditingController();

  final duplicateItems = List<String>.generate(10000, (i) => "Item $i");
  var items = List<String>();
  bool _loading = true;
  List<RestaurantsList> __search_items_list = new List();

  String session_latitude = "";
  String session_longitude = "";
  String session_location = "";

  @override
  void initState() {
    super.initState();

    UserRepository().getlocationdetails().then((getlocation) {
      setState(() {
        session_latitude = getlocation[0];
        session_longitude = getlocation[1];
        session_location = getlocation[2];
      });
    });

    GetSearchRestaurantsApi().searchrestaurantbynmae("").then((value) {
      print("SEARCHRESTAURANT RESPSTATUS  " + value.responseStatus.toString());
      debugPrint(
          base_url + "------" + value.responseStatus.toString() + "-------" + value.result.toString() + "-----" + value.restaurantsList.toString());
      if (value.responseStatus == 1) {
        setState(() {
          _loading = false;

          for (int t = 0; t < value.restaurantsList.length; t++) {
            __search_items_list.add(value.restaurantsList[t]);
          }
        });
      } else if (value.responseStatus == 0) {
        setState(() {
          _loading = false;
          Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
        });
      } else {
        setState(() {
          _loading = false;
          Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => onBackPressed(context),
        child: Scaffold(
          body: Container(
            margin: EdgeInsets.fromLTRB(10, 30, 10, 0),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextField(
                    onChanged: (value) {
                      setState(() {
                        GetSearchRestaurantsApi().searchrestaurantbynmae(value).then((value) {
                          __search_items_list.clear();
                          print("SEARCHRESTAURANT RESPSTATUS  " + value.responseStatus.toString());
                          debugPrint(base_url +
                              "------" +
                              value.responseStatus.toString() +
                              "-------" +
                              value.result.toString() +
                              "-----" +
                              value.restaurantsList.toString());
                          if (value.responseStatus == 1) {
                            setState(() {
                              _loading = false;

                              for (int t = 0; t < value.restaurantsList.length; t++) {
                                __search_items_list.add(value.restaurantsList[t]);
                              }
                            });
                          } else if (value.responseStatus == 0) {
                            setState(() {
                              _loading = false;
                              Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                            });
                          } else {
                            setState(() {
                              _loading = false;
                              Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                            });
                          }
                        });
                      });
                    },
                    controller: editingController,
                    autofocus: true,
                    decoration: InputDecoration(
                        labelText: "Search",
                        hintText: "Restaurant name , cuisine , or a dish...",
                        focusColor: location_border,
                        hoverColor: location_border,
                        prefixIcon: IconButton(
                          padding: EdgeInsets.only(left: 12.0),
                          icon: Image.asset("images/back_arrow.png", width: 20, height: 20),
                          onPressed: () {
                            // Navigator.of(context).pop();
                            /*Navigator.of(context).pushReplacement(MaterialPageRoute(
                            builder: (BuildContext context) => ShowOrdersScreen(session_latitude,session_longitude,session_location)));*/
                            Navigator.of(context).pushReplacement(MaterialPageRoute(
                                builder: (BuildContext context) => HomePage(session_latitude, session_longitude, session_location)));
                          },
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(
                            color: login_form_hint,
                            width: 1.0,
                          ),
                        )),
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: __search_items_list.length,
                      itemBuilder: (context, index) {
                        if (editingController.text.isEmpty) {
                          return Container(
                            margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
                            child: InkWell(
                              onTap: () {
                                setState(() {
                                  print("ITEMCLICK" + __search_items_list[index].id + "=====" + __search_items_list[index].restaurantName);
                                  Navigator.of(context).push(
                                      MaterialPageRoute(builder: (BuildContext context) => RestaurantDetailScreen(__search_items_list[index].id)));
                                });
                              },
                              child: ListTile(
                                leading: Padding(
                                  padding: EdgeInsets.only(top: 5, left: 0, bottom: 5, right: 0),
                                  child: Container(
                                    width: 80.0,
                                    height: 120.0,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(fit: BoxFit.cover, image: NetworkImage(__search_items_list[index].restaurantLogo)),
                                      borderRadius: BorderRadius.all(Radius.circular(000.0)),
                                      color: location_border,
                                    ),
                                  ),
                                ),
                                title: Padding(
                                    padding: EdgeInsets.fromLTRB(0, 0, 0, 5),
                                    child: Text(
                                      __search_items_list[index].restaurantName,
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontSize: 18,
                                        color: login_passcode_text,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.w600,
                                      ),
                                    )),
                              ),
                            ),
                          );
                        } else if (__search_items_list[index].restaurantName.toLowerCase().contains(editingController.text) ||
                            __search_items_list[index].restaurantName.toLowerCase().contains(editingController.text)) {
                          return Container(
                            margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
                            child: InkWell(
                              onTap: () {
                                setState(() {
                                  print("ITEMCLICK" + __search_items_list[index].id + "=====" + __search_items_list[index].restaurantName);
                                  Navigator.of(context).push(
                                      MaterialPageRoute(builder: (BuildContext context) => RestaurantDetailScreen(__search_items_list[index].id)));
                                });
                              },
                              child: ListTile(
                                leading: Padding(
                                  padding: EdgeInsets.only(top: 5, left: 0, bottom: 5, right: 0),
                                  child: Container(
                                    width: 80.0,
                                    height: 80.0,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(fit: BoxFit.cover, image: NetworkImage(__search_items_list[index].restaurantLogo)),
                                      borderRadius: BorderRadius.all(Radius.circular(400.0)),
                                      color: location_border,
                                    ),
                                  ),
                                ),
                                title: Padding(
                                    padding: EdgeInsets.fromLTRB(0, 0, 0, 5),
                                    child: Text(
                                      __search_items_list[index].restaurantName,
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontSize: 18,
                                        color: login_passcode_text,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.w600,
                                      ),
                                    )),
                              ),
                            ),
                          );
                        } else {
                          return Container();
                        }
                      }),
                ),
              ],
            ),
          ),
        ));
  }
}
