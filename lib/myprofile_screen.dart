import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:restaurant_user/dashboard.dart';
import 'package:restaurant_user/myaddress_screen.dart';
import 'package:restaurant_user/session/userRepository.dart';
import 'package:restaurant_user/utils/all_constans.dart';
import 'package:restaurant_user/utils/sizeconfig.dart';
import 'package:restaurant_user/utils/snack_bars.dart';
import 'package:restaurant_user/your_orders_screen.dart';
import 'package:toast/toast.dart';

import 'apis/LogOutApi.dart';
import 'utils/dottedline.dart';
import 'favourite_orders_screen.dart';
import 'login_screen.dart';
import 'model/get_restaurantsresponse.dart';

class MyProfileScreen extends StatefulWidget {
  @override
  _MyProfileScreenState createState() => _MyProfileScreenState();
}

class _MyProfileScreenState extends State<MyProfileScreen> {
  double screenheight = 0.0;
  String returnVal = "";

  String user_id = "";
  String user_first_tname = "";
  String user_last_tname = "";
  String user_email = "";
  String user_phonenumber = "";
  bool hasTimerStopped = false, isUserLogged = false, _loading = true;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  TextEditingController search_Controller = TextEditingController();
  List<Restaurants> getRestaurants = new List();
  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();

    UserRepository().getuserdetails().then((getuserdetails) {
      setState(() {
        _loading = false;
        user_id = getuserdetails[0];
        user_first_tname = getuserdetails[1];
        user_last_tname = getuserdetails[2];
        user_email = getuserdetails[3];
        user_phonenumber = getuserdetails[4];
        isUserLogged = getuserdetails[0] != "" ? true : false;
        print("USERID====" +
            user_id +
            "====USERDEATILS===" +
            user_first_tname +
            "====" +
            user_last_tname +
            "====" +
            user_email +
            "====" +
            user_phonenumber);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    //  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: login_passcode_bg1));
    print("Height" + SizeConfig.screenHeight.toString());
    final space = SizedBox(height: 20);
    if (SizeConfig.screenHeight >= 600 && SizeConfig.screenHeight < 800) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 550 && SizeConfig.screenHeight < 600) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 800) {
      screenheight = 550;
    }
    final screen_height = MediaQuery.of(context).size.height;
    print(screen_height);

    return _loading
        ? Center(
            child: SpinKitCircle(color: Colors.lightBlueAccent),
          )
        : isUserLogged
            ? SafeArea(
                child: Scaffold(
                    key: _scaffoldkey,
                    resizeToAvoidBottomInset: true,
                    backgroundColor: Colors.white,
                    body: SingleChildScrollView(
                      child: Container(
                        color: Colors.white,
                        margin: EdgeInsets.fromLTRB(10, 50, 0, 100),
                        child: Column(children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: EdgeInsets.only(top: 0, left: 10, bottom: 5, right: 0),
                                child: Container(
                                  width: 80.0,
                                  height: 80.0,
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                        fit: BoxFit.cover,
                                        image: NetworkImage('https://i.ibb.co/kSfS1pS/happy-young-waiter-holding-glass-champagne-towel.png')),
                                    borderRadius: BorderRadius.all(Radius.circular(500.0)),
                                    color: location_border,
                                  ),
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                      child: Text(
                                        user_first_tname + " " + user_last_tname,
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          fontSize: 20,
                                          color: login_passcode_text,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w800,
                                        ),
                                      )),
                                  Padding(
                                      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                      child: Text(
                                        user_email,
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          fontSize: 14,
                                          color: login_passcode_text,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w400,
                                        ),
                                      )),
                                ],
                              ),
                              Container(
                                margin: EdgeInsets.fromLTRB(0, 0, 20, 0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Material(
                                      child: InkWell(
                                        onTap: () {
                                          gewinner();
                                        },
                                        child: Padding(
                                            padding: EdgeInsets.fromLTRB(5, 5, 5, 2),
                                            child: Text(
                                              "Log Out",
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontSize: 16,
                                                color: login_passcode_text,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w400,
                                              ),
                                            )),
                                      ),
                                    ),
                                    Material(
                                      child: InkWell(
                                        onTap: () {},
                                        child: Padding(
                                            padding: EdgeInsets.fromLTRB(5, 2, 5, 5),
                                            child: Text(
                                              "View Activity",
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontSize: 14,
                                                color: cart_text,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w400,
                                              ),
                                            )),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: 90,
                                  height: 120,
                                  child: Card(
                                      elevation: 0,
                                      //color: dashboard_bg,
                                      child: InkWell(
                                        onTap: () {},
                                        child: Container(
                                          decoration: BoxDecoration(
                                            border: Border.all(color: dashboard_bg),
                                            borderRadius: BorderRadius.horizontal(left: Radius.circular(0), right: Radius.zero),
                                          ),
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              Padding(
                                                  padding: EdgeInsets.only(top: 5, bottom: 0),
                                                  child: Image.asset(
                                                    'images/dashboard_profile.png',
                                                    height: 50,
                                                    width: 50,
                                                  )),
                                              SizedBox(height: 5),
                                              Padding(
                                                  padding: EdgeInsets.only(left: 0, right: 0, top: 0, bottom: 0),
                                                  child: Text(
                                                    "Bookmarks",
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                      fontSize: 12,
                                                      color: login_passcode_text,
                                                      fontFamily: 'Poppins',
                                                      fontWeight: FontWeight.w400,
                                                    ),
                                                  ))
                                            ],
                                          ),
                                        ),
                                      )),
                                ),
                                Container(
                                  width: 90,
                                  height: 120,
                                  child: Card(
                                      elevation: 0,
                                      //color: dashboard_bg,
                                      child: InkWell(
                                        onTap: () {},
                                        child: Container(
                                          decoration: BoxDecoration(
                                            border: Border.all(color: dashboard_bg),
                                            borderRadius: BorderRadius.horizontal(left: Radius.circular(0.00), right: Radius.zero),
                                          ),
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              Padding(
                                                  padding: EdgeInsets.only(top: 5, bottom: 0),
                                                  child: Image.asset(
                                                    'images/dashboard_profile.png',
                                                    height: 50,
                                                    width: 50,
                                                  )),
                                              SizedBox(height: 5),
                                              Padding(
                                                  padding: EdgeInsets.only(left: 0, right: 0, top: 0, bottom: 0),
                                                  child: Text(
                                                    "Notifications",
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                      fontSize: 12,
                                                      color: login_passcode_text,
                                                      fontFamily: 'Poppins',
                                                      fontWeight: FontWeight.w400,
                                                    ),
                                                  ))
                                            ],
                                          ),
                                        ),
                                      )),
                                ),
                                Container(
                                  width: 90,
                                  height: 120,
                                  child: Card(
                                      elevation: 0,
                                      //color: dashboard_bg,
                                      child: InkWell(
                                        onTap: () {},
                                        child: Container(
                                          decoration: BoxDecoration(
                                            border: Border.all(color: dashboard_bg),
                                            borderRadius: BorderRadius.horizontal(left: Radius.circular(0.00), right: Radius.zero),
                                          ),
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              Padding(
                                                  padding: EdgeInsets.only(top: 5, bottom: 0),
                                                  child: Image.asset(
                                                    'images/dashboard_profile.png',
                                                    height: 50,
                                                    width: 50,
                                                  )),
                                              SizedBox(height: 5),
                                              Padding(
                                                  padding: EdgeInsets.only(left: 0, right: 0, top: 0, bottom: 0),
                                                  child: Text(
                                                    "Settings",
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                      fontSize: 12,
                                                      color: login_passcode_text,
                                                      fontFamily: 'Poppins',
                                                      fontWeight: FontWeight.w400,
                                                    ),
                                                  ))
                                            ],
                                          ),
                                        ),
                                      )),
                                ),
                                Container(
                                  width: 90,
                                  height: 120,
                                  child: Card(
                                    elevation: 0,
                                    //color: dashboard_bg,
                                    child: InkWell(
                                      onTap: () {},
                                      child: Container(
                                        decoration: BoxDecoration(
                                          border: Border.all(color: dashboard_bg),
                                          borderRadius: BorderRadius.horizontal(left: Radius.circular(0.00), right: Radius.zero),
                                        ),
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            Padding(
                                                padding: EdgeInsets.only(top: 5, bottom: 0),
                                                child: Image.asset(
                                                  'images/dashboard_profile.png',
                                                  height: 50,
                                                  width: 50,
                                                )),
                                            SizedBox(height: 5),
                                            Padding(
                                                padding: EdgeInsets.only(left: 0, right: 0, top: 0, bottom: 0),
                                                child: Text(
                                                  "Payments",
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    fontSize: 12,
                                                    color: login_passcode_text,
                                                    fontFamily: 'Poppins',
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                ))
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            child: Padding(
                                padding: EdgeInsets.only(left: 18, right: 0, top: 10, bottom: 0),
                                child: Text(
                                  "Food Orders",
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 20,
                                    color: login_passcode_text,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.w600,
                                  ),
                                )),
                          ),
                          Container(
                              margin: EdgeInsets.fromLTRB(15, 10, 15, 15),
                              alignment: Alignment.center,
                              child: Card(
                                  elevation: 2,
                                  color: order_appbar_bg,
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                                  child: Container(
                                      decoration: BoxDecoration(color: login_passcode_text, borderRadius: BorderRadius.circular(5)),
                                      child: Column(
                                        children: [
                                          Container(
                                            margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                                            child: TextButton(
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Row(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    children: [
                                                      Padding(
                                                          padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                                                          child: Image.asset(
                                                            "images/share_with_friends.png",
                                                            height: 45,
                                                            width: 45,
                                                            fit: BoxFit.cover,
                                                          )),
                                                      Container(
                                                        alignment: Alignment.topLeft,
                                                        child: Padding(
                                                            padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                            child: Text(
                                                              "Your Orders",
                                                              textAlign: TextAlign.left,
                                                              style: TextStyle(
                                                                fontSize: 16,
                                                                color: dashboard_bg,
                                                                fontFamily: 'Poppins',
                                                                fontWeight: FontWeight.w300,
                                                              ),
                                                            )),
                                                      ),
                                                    ],
                                                  ),
                                                  Padding(
                                                      padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                                                      child: Icon(
                                                        Icons.arrow_forward_ios_rounded,
                                                        color: Colors.white,
                                                        size: 18,
                                                      )),
                                                ],
                                              ),
                                              onPressed: () {
                                                print("YOURORDERS");
                                                Navigator.push(context, MaterialPageRoute(builder: (context) => YourOrderScreen()));
                                              },
                                            ),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.fromLTRB(80, 10, 20, 0),
                                            child: DotWidget(
                                              dashColor: location_border,
                                              dashHeight: 0.3,
                                              dashWidth: 10,
                                              emptyWidth: 0,
                                              totalWidth: 250,
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                            child: TextButton(
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Row(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    children: [
                                                      Padding(
                                                          padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                                                          child: Image.asset(
                                                            "images/share_with_friends.png",
                                                            height: 45,
                                                            width: 45,
                                                            fit: BoxFit.cover,
                                                          )),
                                                      Container(
                                                        alignment: Alignment.topLeft,
                                                        child: Padding(
                                                            padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                            child: Text(
                                                              "Favorite Orders",
                                                              textAlign: TextAlign.left,
                                                              style: TextStyle(
                                                                fontSize: 16,
                                                                color: dashboard_bg,
                                                                fontFamily: 'Poppins',
                                                                fontWeight: FontWeight.w300,
                                                              ),
                                                            )),
                                                      ),
                                                    ],
                                                  ),
                                                  Padding(
                                                      padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                                                      child: Icon(
                                                        Icons.arrow_forward_ios_rounded,
                                                        color: Colors.white,
                                                        size: 18,
                                                      )),
                                                ],
                                              ),
                                              onPressed: () {
                                                print("FAVOURITEORDERS");
                                                Navigator.push(context, MaterialPageRoute(builder: (context) => FavouriteOrderScreen()));
                                              },
                                            ),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.fromLTRB(80, 10, 20, 0),
                                            child: DotWidget(
                                              dashColor: location_border,
                                              dashHeight: 0.3,
                                              dashWidth: 10,
                                              emptyWidth: 0,
                                              totalWidth: 250,
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                            child: TextButton(
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Row(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    children: [
                                                      Padding(
                                                          padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                                                          child: Image.asset(
                                                            "images/share_with_friends.png",
                                                            height: 45,
                                                            width: 45,
                                                            fit: BoxFit.cover,
                                                          )),
                                                      Container(
                                                        alignment: Alignment.topLeft,
                                                        child: Padding(
                                                            padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                            child: Text(
                                                              "Address Book",
                                                              textAlign: TextAlign.left,
                                                              style: TextStyle(
                                                                fontSize: 16,
                                                                color: dashboard_bg,
                                                                fontFamily: 'Poppins',
                                                                fontWeight: FontWeight.w300,
                                                              ),
                                                            )),
                                                      ),
                                                    ],
                                                  ),
                                                  Padding(
                                                      padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                                                      child: Icon(
                                                        Icons.arrow_forward_ios_rounded,
                                                        color: Colors.white,
                                                        size: 18,
                                                      )),
                                                ],
                                              ),
                                              onPressed: () {
                                                print("MYADDRESS");
                                                Navigator.push(context, MaterialPageRoute(builder: (context) => MyAddressScreen("", "", "")));
                                              },
                                            ),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.fromLTRB(80, 10, 20, 0),
                                            child: DotWidget(
                                              dashColor: location_border,
                                              dashHeight: 0.3,
                                              dashWidth: 10,
                                              emptyWidth: 0,
                                              totalWidth: 250,
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.fromLTRB(0, 10, 0, 20),
                                            child: TextButton(
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Row(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    children: [
                                                      Padding(
                                                          padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                                                          child: Image.asset(
                                                            "images/share_with_friends.png",
                                                            height: 45,
                                                            width: 45,
                                                            fit: BoxFit.cover,
                                                          )),
                                                      Container(
                                                        alignment: Alignment.topLeft,
                                                        child: Padding(
                                                            padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                            child: Text(
                                                              "Online Ordering Help",
                                                              textAlign: TextAlign.left,
                                                              style: TextStyle(
                                                                fontSize: 16,
                                                                color: dashboard_bg,
                                                                fontFamily: 'Poppins',
                                                                fontWeight: FontWeight.w300,
                                                              ),
                                                            )),
                                                      ),
                                                    ],
                                                  ),
                                                  Padding(
                                                      padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                                                      child: Icon(
                                                        Icons.arrow_forward_ios_rounded,
                                                        color: Colors.white,
                                                        size: 18,
                                                      )),
                                                ],
                                              ),
                                              onPressed: () {
                                                print("ONLINEORDERING");
                                                snackBarLight(context, "Coming soon.....");
                                                //Navigator.push(context, MaterialPageRoute(builder: (context) => CartScreen("Credit","","","","","","","","")));
                                              },
                                            ),
                                          ),
                                        ],
                                      )))),
                          Container(
                            alignment: Alignment.centerLeft,
                            child: Padding(
                                padding: EdgeInsets.only(left: 18, right: 0, top: 0, bottom: 0),
                                child: Text(
                                  "Contactless dining",
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 20,
                                    color: location_text,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.w600,
                                  ),
                                )),
                          ),
                          Container(
                              margin: EdgeInsets.fromLTRB(15, 10, 15, 15),
                              alignment: Alignment.center,
                              child: Card(
                                  elevation: 2,
                                  color: order_appbar_bg,
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                                  child: Container(
                                      decoration: BoxDecoration(color: login_passcode_text, borderRadius: BorderRadius.circular(5)),
                                      child: Column(
                                        children: [
                                          Container(
                                            margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                                            child: TextButton(
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Row(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    children: [
                                                      Padding(
                                                          padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                                                          child: Image.asset(
                                                            "images/share_with_friends.png",
                                                            height: 45,
                                                            width: 45,
                                                            fit: BoxFit.cover,
                                                          )),
                                                      Container(
                                                        alignment: Alignment.topLeft,
                                                        child: Padding(
                                                            padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                            child: Text(
                                                              "Scan",
                                                              textAlign: TextAlign.left,
                                                              style: TextStyle(
                                                                fontSize: 16,
                                                                color: dashboard_bg,
                                                                fontFamily: 'Poppins',
                                                                fontWeight: FontWeight.w300,
                                                              ),
                                                            )),
                                                      ),
                                                    ],
                                                  ),
                                                  Padding(
                                                      padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                                                      child: Icon(
                                                        Icons.arrow_forward_ios_rounded,
                                                        color: Colors.white,
                                                        size: 18,
                                                      )),
                                                ],
                                              ),
                                              onPressed: () {
                                                print("CREDITCARD");
                                                // Navigator.push(context, MaterialPageRoute(builder: (context) => CartScreen("Credit","","","","","","","")));
                                                snackBarLight(context, "Coming soon.....");
                                              },
                                            ),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.fromLTRB(80, 10, 20, 0),
                                            child: DotWidget(
                                              dashColor: location_border,
                                              dashHeight: 0.3,
                                              dashWidth: 10,
                                              emptyWidth: 0,
                                              totalWidth: 250,
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                            child: TextButton(
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Row(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    children: [
                                                      Padding(
                                                          padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                                                          child: Image.asset(
                                                            "images/share_with_friends.png",
                                                            height: 45,
                                                            width: 45,
                                                            fit: BoxFit.cover,
                                                          )),
                                                      Container(
                                                        alignment: Alignment.topLeft,
                                                        child: Padding(
                                                            padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                            child: Text(
                                                              "Dining History",
                                                              textAlign: TextAlign.left,
                                                              style: TextStyle(
                                                                fontSize: 16,
                                                                color: dashboard_bg,
                                                                fontFamily: 'Poppins',
                                                                fontWeight: FontWeight.w300,
                                                              ),
                                                            )),
                                                      ),
                                                    ],
                                                  ),
                                                  Padding(
                                                      padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                                                      child: Icon(
                                                        Icons.arrow_forward_ios_rounded,
                                                        color: Colors.white,
                                                        size: 18,
                                                      )),
                                                ],
                                              ),
                                              onPressed: () {
                                                print("CREDITCARD");
                                                //Navigator.push(context, MaterialPageRoute(builder: (context) => CartScreen("Credit","","","","","","","")));
                                                snackBarLight(context, "Coming soon.....");
                                              },
                                            ),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.fromLTRB(85, 10, 20, 0),
                                            child: DotWidget(
                                              dashColor: location_border,
                                              dashHeight: 0.3,
                                              dashWidth: 10,
                                              emptyWidth: 0,
                                              totalWidth: 250,
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.fromLTRB(0, 10, 0, 20),
                                            child: TextButton(
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Row(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    children: [
                                                      Padding(
                                                          padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                                                          child: Image.asset(
                                                            "images/share_with_friends.png",
                                                            height: 45,
                                                            width: 45,
                                                            fit: BoxFit.cover,
                                                          )),
                                                      Container(
                                                        alignment: Alignment.topLeft,
                                                        child: Padding(
                                                            padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                            child: Text(
                                                              "Dining Help",
                                                              textAlign: TextAlign.left,
                                                              style: TextStyle(
                                                                fontSize: 16,
                                                                color: dashboard_bg,
                                                                fontFamily: 'Poppins',
                                                                fontWeight: FontWeight.w300,
                                                              ),
                                                            )),
                                                      ),
                                                    ],
                                                  ),
                                                  Padding(
                                                      padding: EdgeInsets.fromLTRB(10, 0, 5, 0),
                                                      child: Icon(
                                                        Icons.arrow_forward_ios_rounded,
                                                        color: Colors.white,
                                                        size: 18,
                                                      )),
                                                ],
                                              ),
                                              onPressed: () {
                                                print("CREDITCARD");
                                                //Navigator.push(context, MaterialPageRoute(builder: (context) => CartScreen("Credit","","","","","","","")));
                                                snackBarLight(context, "Coming soon.....");
                                              },
                                            ),
                                          ),
                                        ],
                                      )))),
                          Container(
                            margin: EdgeInsets.fromLTRB(15, 10, 15, 0),
                            alignment: Alignment.center,
                            child: Card(
                              elevation: 2,
                              color: order_appbar_bg,
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(color: order_appbar_bg),
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                            child: TextButton(
                                              child: Text(
                                                "Send Feedback",
                                                textAlign: TextAlign.left,
                                                style: TextStyle(
                                                  fontSize: 16,
                                                  color: login_passcode_text,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w600,
                                                ),
                                              ),
                                              onPressed: () {
                                                snackBarLight(context, "Coming soon.....");
                                              },
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.fromLTRB(10, 20, 10, 0),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                            child: TextButton(
                                              child: Text(
                                                "Report a Safety Emergency",
                                                textAlign: TextAlign.left,
                                                style: TextStyle(
                                                  fontSize: 16,
                                                  color: login_passcode_text,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w600,
                                                ),
                                              ),
                                              onPressed: () {
                                                snackBarLight(context, "Coming soon.....");
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.fromLTRB(10, 20, 10, 10),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                            child: TextButton(
                                              child: Text(
                                                "Rate us on the Play Store",
                                                textAlign: TextAlign.left,
                                                style: TextStyle(
                                                  fontSize: 16,
                                                  color: login_passcode_text,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w600,
                                                ),
                                              ),
                                              onPressed: () {
                                                snackBarLight(context, "Coming soon.....");
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ]),
                      ),
                    )))
            : SafeArea(
                child: SingleChildScrollView(
                    child: Container(
                decoration: BoxDecoration(image: DecorationImage(image: ExactAssetImage('images/splash_bg.png'), fit: BoxFit.cover)),
                height: screen_height,
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        width: SizeConfig.screenWidth,
                        margin: EdgeInsets.fromLTRB(10, 20, 10, 20),
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(30, 40, 30, 40),
                          child: Text(
                            "Please login to continue.",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 26,
                              color: Colors.white,
                              fontFamily: 'Poppins',
                              letterSpacing: 0.4,
                              fontWeight: FontWeight.w800,
                            ),
                          ),
                        ),
                      ),
                      ElevatedButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => LoginPage()),
                            );
                          },
                          style: ElevatedButton.styleFrom(primary: cart_text),
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
                            child: Text(
                              "Proceed to login.",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 20,
                                color: Colors.white,
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w800,
                              ),
                            ),
                          ))
                    ],
                  ),
                ),
              )));
  }

  Future gewinner() async {
    returnVal = await showDialog(
        context: context,
        builder: (context) {
          return _MyDialog(
            user_id: user_id,
          );
        });
  }
}

class _MyDialog extends StatefulWidget {
  _MyDialog({
    this.user_id,
  });

  final String user_id;

  @override
  _MyDialogState createState() => _MyDialogState();
}

class _MyDialogState extends State<_MyDialog> {
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  String session_latitude = "", session_longitude = "", session_location = "";

  @override
  void initState() {
    UserRepository().getlocationdetails().then((getlocation) {
      setState(() {
        session_latitude = getlocation[0];
        session_longitude = getlocation[1];
        session_location = getlocation[2];
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Padding(
          padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
          child: Text(
            "Logout",
            textAlign: TextAlign.left,
            style: TextStyle(
              fontSize: 20,
              color: login_passcode_text,
              fontFamily: 'Poppins',
              fontWeight: FontWeight.w600,
            ),
          )), // To display the title it is optional
      content: Padding(
          padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
          child: Text(
            "Are you sure want to Logout?",
            textAlign: TextAlign.left,
            style: TextStyle(
              fontSize: 16,
              color: login_passcode_text,
              fontFamily: 'Poppins',
              fontWeight: FontWeight.w400,
            ),
          )), // Message which will be pop up on the screen
      // Action widget which will provide the user to acknowledge the choice
      actions: [
        TextButton(
          // FlatButton widget is used to make a text to work like a button
          // textColor: Colors.black,
          onPressed: () {
            Navigator.of(context).pop();
          }, // function used to perform after pressing the button
          child: Padding(
              padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
              child: Text(
                "NO",
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 16,
                  color: login_passcode_text,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w600,
                ),
              )),
        ),
        TextButton(
          // textColor: Colors.black,
          onPressed: () {
            setState(() {
              LogoutRepository().logOut(widget.user_id).then((value) {
                print(value);
                if (value.responseStatus == 0) {
                  // Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
                  Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                } else if (value.responseStatus == 1) {
                  print("LOGOUTRESPONSE " + "--" + value.result + "--" + value.responseStatus.toString());
                  setState(() {
                    Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                    UserRepository.Clearall();
                    // Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => LoginPage()), (Route<dynamic> route) => false);
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(builder: (context) => HomePage(session_latitude, session_longitude, session_location)),
                        (Route<dynamic> route) => false);
                  });
                } else {
                  Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                }
              });
            });
          },
          child: Padding(
              padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
              child: Text(
                "YES",
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 16,
                  color: login_passcode_text,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w600,
                ),
              )),
        ),
      ],
    );
  }
}
