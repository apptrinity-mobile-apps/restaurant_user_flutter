import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:restaurant_user/apis/getrestaurants.dart';
import 'package:restaurant_user/restaurant_search_screen2.dart';
import 'package:restaurant_user/restaurantdetail_screen.dart';
import 'package:restaurant_user/utils/all_constans.dart';
import 'package:restaurant_user/utils/sizeconfig.dart';

import 'utils/dottedline.dart';
import 'model/get_restaurantsresponse.dart';

class GoOutScreen extends StatefulWidget {
  final String current_latitude;
  final String current_longitude;
  final String current_location;

  const GoOutScreen(this.current_latitude, this.current_longitude, this.current_location, {Key key}) : super(key: key);

  @override
  _GoOutScreenState createState() => _GoOutScreenState();
}

class _GoOutScreenState extends State<GoOutScreen> {
  int cart_count = 0;
  bool enable_searchlist = false;
  String userid = "";
  String employee_id = "";
  String first_name = "";
  String last_name = "";
  String restaurant_id = "";
  String servicearea_id = "";
  String user_id = "";
  double screenheight = 0.0;
  String returnVal = "";
  bool hasTimerStopped = false;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  String showgoutview = "order_location_icon.png";
  String orderscanner = "order_scanner.png";
  String restaurantsquare = "restaurant_square.png";
  String noorders = "no_order_dash.png";
  TextEditingController search_Controller = TextEditingController();
  List<Restaurants> getRestaurants = new List();
  final myController = TextEditingController();
  bool _loading = true;

  @override
  void initState() {
    super.initState();
    _loading = false;
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    //  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: login_passcode_bg1));
    print("Height" + SizeConfig.screenHeight.toString());
    final space = SizedBox(height: 20);
    if (SizeConfig.screenHeight >= 600 && SizeConfig.screenHeight < 800) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 550 && SizeConfig.screenHeight < 600) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 800) {
      screenheight = 550;
    }
    final screen_height = MediaQuery.of(context).size.height;
    print(screen_height);

    return SafeArea(
        child: Scaffold(
            resizeToAvoidBottomInset: true,
            backgroundColor: Colors.white,
            body: _loading
                  ? Expanded(
                      child: SpinKitCircle(color: Colors.lightBlueAccent),
                    )
                  : Container(
                      color: order_appbar_bg,
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      child: Column(children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
                          child: Column(//crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                            Container(
                              margin: EdgeInsets.fromLTRB(0, 100, 0, 0),
                              alignment: Alignment.center,
                              padding: EdgeInsets.only(left: 15, top: 10),
                              child: Column(
                                children: [
                                  Image.asset(
                                    "images/no_order_dash.png",
                                    height: 500,
                                    width: 500,
                                  ),
                                  Padding(
                                      padding: EdgeInsets.only(top: 20, right: 0, bottom: 0),
                                      child: Text("No Data Found",
                                          style: TextStyle(
                                              color: login_passcode_text, fontSize: 24, fontFamily: 'Poppins', fontWeight: FontWeight.w400)))
                                ],
                              ),
                            )
                          ]),
                        ),
                      ]),
                    ),
            ));
  }
}
