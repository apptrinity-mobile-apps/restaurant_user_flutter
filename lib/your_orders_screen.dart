import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:restaurant_user/apis/getYourOrdersApi.dart';
import 'package:restaurant_user/session/userRepository.dart';
import 'package:restaurant_user/track_order.dart';
import 'package:restaurant_user/utils/all_constans.dart';
import 'package:restaurant_user/utils/back_press.dart';
import 'package:restaurant_user/utils/sizeconfig.dart';

import 'cart.dart';
import 'model/getyourordersresponse.dart';

class YourOrderScreen extends StatefulWidget {
  @override
  _YourOrderScreenState createState() => _YourOrderScreenState();
}

class _YourOrderScreenState extends State<YourOrderScreen> {
  int cart_count = 0;
  bool enable_searchlist = false;
  String userid = "60e420c859263dad5bcb64b0";
  String employee_id = "";
  String first_name = "";
  String last_name = "";
  String restaurant_id = "";
  String servicearea_id = "";
  String user_id = "";
  double screenheight = 0.0;
  String returnVal = "";
  bool hasTimerStopped = false;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  String showgoutview = "order_location_icon.png";
  String orderscanner = "order_scanner.png";
  String restaurantsquare = "restaurant_square.png";
  String noorders = "no_order_dash.png";
  TextEditingController search_Controller = TextEditingController();
  List<OrdersList> getYourOrders = new List();
  bool isSwitched = false;
  bool isSwitchedveg = false;

  int selected_pos = 0;
  String res_id = "60e41f7f59263dad5bcb64a7";
  String menu_id = "";
  String menu_groupid = "";
  List<String> selectedCheckbox = [];

  int selected = 0;
  bool _loading = true;

  @override
  void initState() {
    super.initState();

    UserRepository().getuserdetails().then((getuserdetails) {
      setState(() {
        user_id = getuserdetails[0];
        print("USERID====" + user_id);
        GetYourOrdersApi().getYourOrders(user_id).then((result_showalldayview) {
          setState(() {
            _loading = false;
            getYourOrders = result_showalldayview;
            print("ORDERSLENGTH=====" + getYourOrders.length.toString());
          });
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    //  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: login_passcode_bg1));
    print("Height" + SizeConfig.screenHeight.toString());
    final space = SizedBox(height: 20);
    if (SizeConfig.screenHeight >= 600 && SizeConfig.screenHeight < 800) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 550 && SizeConfig.screenHeight < 600) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 800) {
      screenheight = 550;
    }
    final screen_height = MediaQuery.of(context).size.height;
    print(screen_height);

    return WillPopScope(
        onWillPop: () async => onBackPressed(context),
        child: Scaffold(
            resizeToAvoidBottomInset: true,
            backgroundColor: Colors.white,
            appBar: AppBar(
              toolbarHeight: 56,
              //toolbarHeight:  height-30.00,
              automaticallyImplyLeading: false,
              elevation: 0.0,
              backgroundColor: Colors.white,
              centerTitle: true,
              leading: Builder(
                builder: (BuildContext context) {
                  return IconButton(
                    padding: EdgeInsets.only(left: 12.0),
                    icon: Image.asset("images/back_arrow.png", width: 20, height: 20),
                    onPressed: () {
                      onBackPressed(context);
                    },
                  );
                },
              ),
              actions: [],
            ),
            body:  Container(
                margin: EdgeInsets.fromLTRB(15, 0, 15, 0),
                //height: SizeConfig.safeBlockHorizontal * 155,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Padding(
                          padding: EdgeInsets.fromLTRB(15, 0, 0, 5),
                          child: Text(
                            "Your Orders",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: 20,
                              color: login_passcode_text,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w800,
                            ),
                          )),
                      Padding(
                          padding: EdgeInsets.fromLTRB(15, 0, 0, 5),
                          child: Text(
                            "Casual Dining - Chinese, Asian, Japanese",
                            textAlign: TextAlign.start,
                            style: TextStyle(
                              fontSize: 14,
                              color: login_passcode_text,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w400,
                            ),
                          )),
                      Padding(
                          padding: EdgeInsets.fromLTRB(15, 10, 0, 5),
                          child: Text(
                            "Today",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: 20,
                              color: login_passcode_text,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w800,
                            ),
                          )),
                      _loading
                          ? Expanded(
                              child: SpinKitCircle(color: Colors.lightBlueAccent),
                            )
                          : getYourOrders.length > 0
                              ? Expanded(child: ListView.builder(
                                  physics: ClampingScrollPhysics(),
                                  shrinkWrap: true,
                                  primary: false,
                                  scrollDirection: Axis.vertical,
                                  itemCount: getYourOrders.length,
                                  itemBuilder: (context, index) {
                                    return Container(
                                        width: SizeConfig.screenWidth,
                                        margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                        child: Card(
                                          margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                          color: Colors.white,
                                          elevation: 3,
                                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
                                          child: InkWell(
                                              onTap: () {
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(builder: (context) => TrackOrder("orders")),
                                                );
                                              },
                                              child: ClipRRect(
                                                borderRadius: BorderRadius.circular(5),
                                                child: Container(
                                                  child: Column(
                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      mainAxisSize: MainAxisSize.max,
                                                      children: [
                                                        Row(
                                                          children: [
                                                            Container(
                                                              padding: EdgeInsets.all(0.0),
                                                              width: 180,
                                                              height: 180,
                                                              child: Stack(
                                                                children: [
                                                                  Positioned.fill(
                                                                    child: Container(
                                                                      padding: EdgeInsets.all(10.0),
                                                                      decoration: BoxDecoration(
                                                                        borderRadius: BorderRadius.all(Radius.circular(0)),
                                                                        boxShadow: [
                                                                          BoxShadow(
                                                                            color: Colors.blueGrey,
                                                                            blurRadius: 5,
                                                                            offset: Offset(0, 7),
                                                                          ),
                                                                        ],
                                                                        image:
                                                                            /* NetworkImage(getYourOrders[index].restaurantLogo)*/
                                                                            DecorationImage(
                                                                                image: NetworkImage(getYourOrders[index].restaurantLogo),
                                                                                fit: BoxFit.cover),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  Positioned.fill(
                                                                    top: 110.0,
                                                                    child: Container(
                                                                      padding: EdgeInsets.all(5),
                                                                      decoration: BoxDecoration(
                                                                        borderRadius: BorderRadius.all(Radius.circular(0)),
                                                                        gradient: LinearGradient(
                                                                          colors: [
                                                                            Colors.black,
                                                                            Colors.transparent,
                                                                            Colors.transparent,
                                                                            Colors.black
                                                                          ],
                                                                          begin: Alignment.topCenter,
                                                                          end: Alignment.bottomCenter,
                                                                          stops: [0, 0, 0, 0.5],
                                                                        ),
                                                                      ),
                                                                      child: Column(
                                                                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                                        children: [
                                                                          Padding(
                                                                              padding: EdgeInsets.fromLTRB(5, 0, 0, 5),
                                                                              child: Text(
                                                                                getYourOrders[index].restaurantName,
                                                                                textAlign: TextAlign.center,
                                                                                maxLines: 2,
                                                                                overflow: TextOverflow.clip,
                                                                                style: TextStyle(
                                                                                  fontSize: 15,
                                                                                  color: Colors.white,
                                                                                  fontFamily: 'Poppins',
                                                                                  fontWeight: FontWeight.w800,
                                                                                ),
                                                                              )),
                                                                          Padding(
                                                                              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                                              child: Text(
                                                                                "Sacramento - California",
                                                                                textAlign: TextAlign.center,
                                                                                maxLines: 1,
                                                                                overflow: TextOverflow.clip,
                                                                                style: TextStyle(
                                                                                  fontSize: 13,
                                                                                  color: Colors.white,
                                                                                  fontFamily: 'Poppins',
                                                                                  fontWeight: FontWeight.w600,
                                                                                ),
                                                                              ))
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                            Container(
                                                              margin: EdgeInsets.fromLTRB(5, 5, 20, 0),
                                                              child: Column(
                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                children: [
                                                                  Padding(
                                                                      padding: EdgeInsets.fromLTRB(5, 0, 0, 5),
                                                                      child: Text(
                                                                        "Items",
                                                                        textAlign: TextAlign.left,
                                                                        style: TextStyle(
                                                                          fontSize: 16,
                                                                          color: login_passcode_text,
                                                                          fontFamily: 'Poppins',
                                                                          fontWeight: FontWeight.w600,
                                                                        ),
                                                                      )),
                                                                  Padding(
                                                                      padding: EdgeInsets.fromLTRB(5, 0, 0, 5),
                                                                      child: Text(
                                                                        getYourOrders[index].itemsList[0].quantity.toString() +
                                                                            " x " +
                                                                            getYourOrders[index].itemsList[0].itemName,
                                                                        maxLines: 2,
                                                                        overflow: TextOverflow.ellipsis,
                                                                        softWrap: false,
                                                                        style: TextStyle(
                                                                          fontSize: 14,
                                                                          color: login_passcode_text,
                                                                          fontFamily: 'Poppins',
                                                                          fontWeight: FontWeight.w400,
                                                                        ),
                                                                      )),
                                                                  /*Padding(
                                              padding: EdgeInsets.fromLTRB(5, 0, 0, 5),
                                              child: Text(
                                                getYourOrders[index].itemsList[0].itemName,
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                  fontSize: 14,
                                                  color: login_passcode_text,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w400,
                                                ),
                                              )),*/
                                                                  Padding(
                                                                      padding: EdgeInsets.fromLTRB(5, 10, 0, 5),
                                                                      child: Text(
                                                                        "Ordered On",
                                                                        textAlign: TextAlign.center,
                                                                        style: TextStyle(
                                                                          fontSize: 16,
                                                                          color: login_passcode_text,
                                                                          fontFamily: 'Poppins',
                                                                          fontWeight: FontWeight.w600,
                                                                        ),
                                                                      )),
                                                                  Padding(
                                                                      padding: EdgeInsets.fromLTRB(5, 0, 0, 5),
                                                                      child: Text(
                                                                        DateTimeConverter(getYourOrders[index].createdOn),
                                                                        textAlign: TextAlign.center,
                                                                        style: TextStyle(
                                                                          fontSize: 14,
                                                                          color: login_passcode_text,
                                                                          fontFamily: 'Poppins',
                                                                          fontWeight: FontWeight.w400,
                                                                        ),
                                                                      )),
                                                                  Padding(
                                                                      padding: EdgeInsets.fromLTRB(5, 10, 0, 5),
                                                                      child: Text(
                                                                        "Total Amount",
                                                                        textAlign: TextAlign.center,
                                                                        style: TextStyle(
                                                                          fontSize: 16,
                                                                          color: login_passcode_text,
                                                                          fontFamily: 'Poppins',
                                                                          fontWeight: FontWeight.w600,
                                                                        ),
                                                                      )),
                                                                  Padding(
                                                                      padding: EdgeInsets.fromLTRB(5, 0, 0, 5),
                                                                      child: Text(
                                                                        new String.fromCharCodes(new Runes('\u0024')) +
                                                                            getYourOrders[index].totalAmount.toStringAsFixed(2),
                                                                        textAlign: TextAlign.center,
                                                                        style: TextStyle(
                                                                          fontSize: 14,
                                                                          color: login_passcode_text,
                                                                          fontFamily: 'Poppins',
                                                                          fontWeight: FontWeight.w400,
                                                                        ),
                                                                      ))
                                                                ],
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                        Material(
                                                          color: dashboard_bg,
                                                          child: Container(
                                                              margin: EdgeInsets.fromLTRB(2, 0, 0, 0),
                                                              alignment: Alignment.centerRight,
                                                              child: InkWell(
                                                                child: Row(
                                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                    mainAxisSize: MainAxisSize.max,
                                                                    children: [
                                                                      Padding(
                                                                        padding: EdgeInsets.fromLTRB(15, 0, 0, 0),
                                                                        child: Image.asset(
                                                                          'images/timer.png',
                                                                          height: 20,
                                                                          width: 20,
                                                                        ),
                                                                      ),
                                                                      Padding(
                                                                        padding: EdgeInsets.fromLTRB(15, 5, 15, 5),
                                                                        child: Text("REPEAT ORDER",
                                                                            textAlign: TextAlign.center,
                                                                            style: TextStyle(
                                                                                fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                                                fontFamily: 'Poppins',
                                                                                fontWeight: FontWeight.w600,
                                                                                color: login_passcode_text)),
                                                                      )
                                                                    ]),
                                                                onTap: () {
                                                                  print("ORDERID" + getYourOrders[index].id);
                                                                  Navigator.push(
                                                                      context,
                                                                      MaterialPageRoute(
                                                                          builder: (context) => CartScreen("Cash", "", "", "", "", "", "",
                                                                              getYourOrders[index].restaurantId, getYourOrders[index].id)));
                                                                },
                                                              )),
                                                        )
                                                      ]),
                                                ),
                                              )),
                                        ));
                                  }))
                              : Expanded(child: Container(
                                  margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                  alignment: Alignment.center,
                                  padding: EdgeInsets.only(left: 0, top: 0),
                                  child: Text("No Orders Found",
                                      style: TextStyle(color: login_passcode_text, fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w600)),
                                )),
                    ]),
              ),
            ));
  }

  DateTimeConverter(String createddate) {
    print("CREATEDDATE" + createddate.toString());
    var now = DateFormat('E, d MMM yyyy HH:mm:ss').parse(createddate);
    String formattedTime = DateFormat.Hms().format(now);
    print("DATETIME======" + formattedTime);
    return formattedTime.toString();
  }
}
