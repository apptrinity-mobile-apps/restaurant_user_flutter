import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

Future<bool> onBackPressed(BuildContext context) async {
  Navigator.of(context).pop(true);
  return true;
}