import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

void snackBarLight(BuildContext context, String message) {
  final snackBar = SnackBar(
      backgroundColor: Colors.white,
      behavior: SnackBarBehavior.floating,
      content: Text(
        message,
        style: TextStyle(color: Colors.black, fontFamily: 'Poppins', fontWeight: FontWeight.w600, letterSpacing: 0.2, fontSize: 16),
      ));
  ScaffoldMessenger.of(context).clearSnackBars();
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}

void snackBarDark(BuildContext context, String message) {
  final snackBar = SnackBar(
      backgroundColor: Colors.black,
      behavior: SnackBarBehavior.floating,
      content: Text(
        message,
        style: TextStyle(color: Colors.white, fontFamily: 'Poppins', fontWeight: FontWeight.w600, letterSpacing: 0.2, fontSize: 16),
      ));
  ScaffoldMessenger.of(context).clearSnackBars();
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}
