import 'package:flutter/material.dart';

class EditTextStyle extends StatelessWidget {
  EditTextStyle(
      {@required this.hint_lable,
      @required this.valid_lable,
      this.globacontroller,
      this.obscure,
      this.fType});

  String hint_lable;
  String valid_lable;
  TextEditingController globacontroller;
  bool obscure;
  TextInputType fType;

  //  IconData left_icon;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 5.0,
          ),
          TextFormField(
            keyboardType: fType,
            controller: globacontroller,
            validator: (value) {
              if (value.isEmpty) {
                return valid_lable;
              }
              return null;
            },
            obscureText: obscure,
            decoration: InputDecoration(
                hintText: hint_lable,
                contentPadding: EdgeInsets.only(
                  bottom: 30 / 2,
                  left: 50 / 2, // HERE THE IMPORTANT PART
                  // HERE THE IMPORTANT PART
                ),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0))),
          ),
        ],
      ),
    );
  }
}
