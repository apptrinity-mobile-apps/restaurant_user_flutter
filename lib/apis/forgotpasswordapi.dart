import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:http/http.dart' as http;
import 'package:restaurant_user/model/loginresponse.dart';
import 'package:restaurant_user/model/signupresponse.dart';
import 'package:restaurant_user/utils/all_constans.dart';
class ForgotPasswordRepository {
  Future<loginresponse> forgotPassword(String otp,String email,String newPassword,String confirmNewPassword) async {
    var body = json.encode({ 'otp': otp,'email': email,'newPassword': newPassword,'confirmNewPassword': confirmNewPassword});
    print(base_url + "FORGOTPASSWORD" + "FORGOTPASSWORD_REQUEST"+ body.toString());

    dynamic response = await Requests.post(base_url + "forgot_password",
        body: body, headers: {'Content-type': 'application/json'});

    loginresponse signupresult_data =
    loginresponse.fromJson(jsonDecode(response));
    return signupresult_data;
  }


}

