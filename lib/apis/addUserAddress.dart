import 'dart:convert';
import 'dart:developer' as dev;

import 'package:requests/requests.dart';
import 'package:restaurant_user/model/get_addresslistresponse.dart';
import 'package:restaurant_user/model/get_restaurantsresponse.dart';
import 'package:restaurant_user/utils/all_constans.dart';

import 'addAddressResponse.dart';

class AddAddressRepository {

  Future<addAddressresponse> addUserAddress(String latitude,String longitude,String userId,bool isDefault,String fullAddress,String floor,String howToReach,String addressType) async {
    print(base_url + "add_user_address");
    var body = json.encode({'latitude': latitude,'longitude': longitude,'userId': userId,'isDefault': isDefault,'fullAddress': fullAddress,'floor': floor,'howToReach': howToReach,'addressType': addressType,});

    dynamic response = await Requests.post(base_url + "add_user_address", body: body, headers: {'Content-type': 'application/json'});
    print(body);
    print(base_url + "ADDADDRESSRESPONSEWITHCODE"+ response.toString());

    addAddressresponse loginresult_data =
    addAddressresponse.fromJson(jsonDecode(response));
    return loginresult_data;

  }

}


