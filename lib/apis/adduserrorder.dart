import 'dart:convert';
import 'dart:developer' as dev;

import 'package:requests/requests.dart';
import 'package:restaurant_user/model/adduserorderresponse.dart';
import 'package:restaurant_user/utils/all_constans.dart';
class AddUserOrderRepository {

  Future<adduserorderresponse> adduserorder(orderItems, String tipAmount,double discountAmount,double subTotal,double taxAmount,double totalAmount,String restaurantId,int creditsUsed, customerDetails,deliveryInfo,String dineInOptionId,String dineInOptionName,String dineInBehaviour,int saveType , bool saveAddress, bool isDefaultAddress ,String amount, String deliveryInstructions,int paymentType,int serviceChargesAmount) async {

    var body = json.encode({'orderItems':orderItems,'tipAmount':tipAmount,'discountAmount':discountAmount,'subTotal':subTotal,'taxAmount':taxAmount,'totalAmount':totalAmount,'restaurantId':restaurantId,'creditsUsed':creditsUsed,'customerDetails':customerDetails,'deliveryInfo':deliveryInfo,'dineInOptionId':dineInOptionId,'dineInOptionName':dineInOptionName,'dineInBehaviour':dineInBehaviour,'saveType':saveType,'saveAddress':saveAddress,'isDefaultAddress':isDefaultAddress,'amount':amount,'deliveryInstructions':deliveryInstructions,'paymentType':paymentType,'serviceChargesAmount':serviceChargesAmount});

    dev.log(base_url + "ADDUSERORDERREQUEST"+ body.toString());


    dynamic response = await Requests.post(base_url + "add_user_app_order",
        body: body, headers: {'Content-type': 'application/json'});

    adduserorderresponse loginresult_data =
    adduserorderresponse.fromJson(jsonDecode(response));

    print(loginresult_data.result);
    print(loginresult_data.orderId);
    return loginresult_data;

  }


}




