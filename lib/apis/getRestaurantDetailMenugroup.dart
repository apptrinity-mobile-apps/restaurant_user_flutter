import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:restaurant_user/model/get_restaurantdetailresponse.dart';
import 'package:restaurant_user/utils/all_constans.dart';

class GetRestaurantDetailMenuGroup {
 /* Future<List<Menus>> getRestaurantDetailmenu(
      String res_id,
      ) async {

    var body = json.encode({
      'restaurantId': res_id
    });
    print(body);
    dynamic response = await Requests.post(base_url + "get_menus",
        body: body, headers: {'Content-type': 'application/json'});

    final res = json.decode(response);
    Iterable list = res['menus'];
    return list.map((model) => Menus.fromJson(model)).toList();

  }*/

  Future<GetRestaurantDetailResponse> getRestaurantDetailmenu(String res_id) async {
    var body = json.encode({
      'restaurantId': res_id
    });
    print(body);
    dynamic response = await Requests.post(base_url + "get_menus",
        body: body, headers: {'Content-type': 'application/json'});

    GetRestaurantDetailResponse restaurantdetailresponse_data =
    GetRestaurantDetailResponse.fromJson(jsonDecode(response));
    return restaurantdetailresponse_data;
  }


}