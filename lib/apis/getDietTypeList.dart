import 'dart:convert';
import 'dart:developer' as dev;

import 'package:requests/requests.dart';
import 'package:restaurant_user/model/get_diettypelistresponse.dart';
import 'package:restaurant_user/model/get_restaurantsresponse.dart';
import 'package:restaurant_user/utils/all_constans.dart';

class DietTypeListRepository {

  Future<List<DietTypeLists>> getDietTypeList() async {
    print("http://3.14.187.24/api/restaurant_super_admin/" + "view_all_diet_types");

    dynamic response = await Requests.get("http://18.190.55.150/api/restaurant_super_admin/" + "view_all_diet_types",
         headers: {'Content-type': 'application/json'});

    print(base_url + "view_all_diet_types"+ response.toString());
   final res = json.decode(response);
    print("GETALLDIETTYPES--"+ res['dietTypeLists'].toString());
    Iterable list = res['dietTypeLists'];
    return list.map((model) => DietTypeLists.fromJson(model)).toList();

  }



}


