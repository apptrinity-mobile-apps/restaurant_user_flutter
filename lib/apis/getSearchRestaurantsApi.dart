import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:restaurant_user/model/getsearchrestaurantresponse.dart';
import 'package:restaurant_user/utils/all_constans.dart';
class GetSearchRestaurantsApi {

  Future<getsearchrestaurantresponse> searchrestaurantbynmae(String searchName) async {
    var body = json.encode({'searchName':searchName});

    print(base_url + "search_restaurant_by_name"+ "SEARCHRESTAURANTAPIREQUEST"+searchName.toString());

    dynamic response = await Requests.post(base_url + "search_restaurant_by_name",
        body: body, headers: {'Content-type': 'application/json'});

    print(base_url + ""+ response.toString());
    getsearchrestaurantresponse searchrestaurant_data =
    getsearchrestaurantresponse.fromJson(jsonDecode(response));
    return searchrestaurant_data;
  }
}

