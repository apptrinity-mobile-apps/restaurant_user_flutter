import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:http/http.dart' as http;
import 'package:restaurant_user/model/loginresponse.dart';
import 'package:restaurant_user/model/signupresponse.dart';
import 'package:restaurant_user/utils/all_constans.dart';
class ResetPasswordRepository {
  Future<loginresponse> resetPassword(String email) async {
    var body = json.encode({ 'email': email});
    print(base_url + "RESETPASSWORD" + "RESETPASSWORD_REQUEST"+ body.toString());

    dynamic response = await Requests.post(base_url + "password_reset",
        body: body, headers: {'Content-type': 'application/json'});

    loginresponse signupresult_data =
    loginresponse.fromJson(jsonDecode(response));
    return signupresult_data;
  }


}

