import 'dart:convert';
import 'dart:developer' as dev;

import 'package:requests/requests.dart';
import 'package:restaurant_user/model/get_restaurantsonlineorderingresponse.dart';
import 'package:restaurant_user/model/get_restaurantsresponse.dart';
import 'package:restaurant_user/utils/all_constans.dart';

class RestaurantOnlineOrderingRulesRepository {

  Future<GetRestaurantsOnlineOrderingresponse> getRestaurantOnlineOrderingRules(String res_id) async {
    var body = json.encode({'restaurantId':res_id});

    print(base_url +  "RESTUARANTONLINERULESAPIREQUEST----"+res_id);

    dynamic response = await Requests.post(base_url + "get_restaurant_online_ordering_rules",
        body: body, headers: {'Content-type': 'application/json'});

    print(base_url + ""+ response.toString());
    GetRestaurantsOnlineOrderingresponse searchrestaurantmenu_data =
    GetRestaurantsOnlineOrderingresponse.fromJson(jsonDecode(response));
    return searchrestaurantmenu_data;
  }



}


