import 'dart:convert';
import 'dart:developer' as dev;

import 'package:requests/requests.dart';
import 'package:restaurant_user/model/get_addresslistresponse.dart';
import 'package:restaurant_user/model/get_restaurantsresponse.dart';
import 'package:restaurant_user/utils/all_constans.dart';

class AddressListRepository {

  Future<List<UserAddressBookList>> getAddressLists(String customerId) async {
    print(base_url + "get_all_addresslist");
    var body = json.encode({'customerId': customerId});

    dynamic response = await Requests.post(base_url + "view_user_address_book", body: body, headers: {'Content-type': 'application/json'});

    print(body);
    print(base_url + "view_user_address_book"+ response.toString());
    final res = json.decode(response);
    print("GETADDRESSLISTS--"+ res['userAddressBookList'].toString());
    Iterable list = res['userAddressBookList'];
    return list.map((model) => UserAddressBookList.fromJson(model)).toList();

  }

}


