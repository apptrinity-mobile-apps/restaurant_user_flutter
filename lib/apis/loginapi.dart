import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:http/http.dart' as http;
import 'package:restaurant_user/model/loginresponse.dart';
import 'package:restaurant_user/model/signupresponse.dart';
import 'package:restaurant_user/utils/all_constans.dart';
class LoginRepository {
  Future<loginresponse> login(String email,String password,String deviceToken,String deviceType, ) async {
    var body = json.encode({ 'email': email, 'password': password,'deviceToken':deviceToken, 'deviceType': deviceType,});
    print(base_url + "LOGIN" + "LOGIN_REQUEST"+ body.toString());

    dynamic response = await Requests.post(base_url + "user_login",
        body: body, headers: {'Content-type': 'application/json'});

    loginresponse signupresult_data =
    loginresponse.fromJson(jsonDecode(response));
    return signupresult_data;
  }


}

