import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:restaurant_user/model/get_restaurantdetailresponse.dart';
import 'package:restaurant_user/utils/all_constans.dart';

class GetPopularDishesApi {

 /* Future<List<PopularDishes>> getPopularDishes(String restaurantId) async {
    print(base_url + "view_popular_dishes");
    var body = json.encode({'restaurantId': restaurantId});

    dynamic response = await Requests.post(base_url + "view_popular_dishes", body: body, headers: {'Content-type': 'application/json'});

    print(base_url + "popularDishes"+ response.toString());
    final res = json.decode(response);
    print("GETPOPULARDISH--"+ res['popularDishes'].toString());
    Iterable list = res['popularDishes'];
    return list.map((model) => PopularDishes.fromJson(model)).toList();

  }*/


  Future<GetRestaurantDetailResponse> getPopularDishes(String res_id,List dietTypelist) async {
    var body = json.encode({
      'restaurantId': res_id,
      'dietTypeList': dietTypelist
    });
    print(body);
    dynamic response = await Requests.post(base_url + "view_popular_dishes",
        body: body, headers: {'Content-type': 'application/json'});

    GetRestaurantDetailResponse restaurantdetailresponse_data =
    GetRestaurantDetailResponse.fromJson(jsonDecode(response));
    return restaurantdetailresponse_data;
  }


}
