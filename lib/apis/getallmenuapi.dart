import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:restaurant_user/model/getallmenus.dart';
import 'package:restaurant_user/utils/all_constans.dart';

class GetAllMenu {
  Future<List<MenuList>> getmenu(
      String res_id,
      List dietTypelist
      ) async {

    var body = json.encode({
      'restaurantId': res_id,
      'dietTypeList': dietTypelist
    });
    print(body);
    dynamic response = await Requests.post(base_url + "get_all_menus",
        body: body, headers: {'Content-type': 'application/json'});

    final res = json.decode(response);
    Iterable list = res['menuList'];
    return list.map((model) => MenuList.fromJson(model)).toList();

  }
}