import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:restaurant_user/model/getyourordersresponse.dart';
import 'package:restaurant_user/utils/all_constans.dart';

class GetYourOrdersApi {

  Future<List<OrdersList>> getYourOrders(String userId) async {
    print(base_url + "your_orders");
    var body = json.encode({'userId': userId});

    dynamic response = await Requests.post(base_url + "your_orders", body: body, headers: {'Content-type': 'application/json'});

    print(base_url + "ordersList"+ response.toString());
    final res = json.decode(response);
    print("GETRESTAURANTS--"+ res['ordersList'].toString());
    Iterable list = res['ordersList'];
    return list.map((model) => OrdersList.fromJson(model)).toList();

  }


}
