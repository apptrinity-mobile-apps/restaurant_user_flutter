import 'dart:convert';
import 'dart:developer' as dev;

import 'package:requests/requests.dart';
import 'package:restaurant_user/model/getViewOrderDetailResponse.dart';
import 'package:restaurant_user/model/get_restaurantsresponse.dart';
import 'package:restaurant_user/utils/all_constans.dart';

class OrderDetailRepository {


  Future<getViewOrderdetailResponse> getOrderDetails(String orderId,String userId) async {
    var body = json.encode({'orderId':orderId,'userId':userId});
    dynamic response = await Requests.post(base_url + "view_order_details",
        body: body, headers: {'Content-type': 'application/json'});

    print(base_url + "view_order_details"+ response.toString());
    print(body);
    getViewOrderdetailResponse searchrestaurantmenu_data =
    getViewOrderdetailResponse.fromJson(jsonDecode(response));
    return searchrestaurantmenu_data;
  }


}


