import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:restaurant_user/model/getsearchrestaurantmenuresponse.dart';
import 'package:restaurant_user/utils/all_constans.dart';

class GetSearchRestaurantMenuApi {

  Future<getsearchrestaurantmenuresponse> searchrestaurantmenubynmae(String res_id,String searchName) async {
    var body = json.encode({'restaurantId':res_id,'searchName':searchName});

    print(base_url +  "SEARCHRESTAURANTMENUAPIREQUEST----"+res_id+"==="+searchName.toString());

    dynamic response = await Requests.post(base_url + "search_menu",
        body: body, headers: {'Content-type': 'application/json'});

    print(base_url + ""+ response.toString());
    getsearchrestaurantmenuresponse searchrestaurantmenu_data =
    getsearchrestaurantmenuresponse.fromJson(jsonDecode(response));
    return searchrestaurantmenu_data;
  }
}

