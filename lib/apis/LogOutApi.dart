import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:http/http.dart' as http;
import 'package:restaurant_user/model/loginresponse.dart';
import 'package:restaurant_user/utils/all_constans.dart';
class LogoutRepository {
  Future<loginresponse> logOut(String userId) async {
    var body = json.encode({'userId': userId});
    print(base_url + "LOGOUT" + "LOGOUT_REQUEST"+ body.toString());

    dynamic response = await Requests.post(base_url + "logout",
        body: body, headers: {'Content-type': 'application/json'});


    loginresponse loginresult_data =
    loginresponse.fromJson(jsonDecode(response));
    return loginresult_data;
  }


}


