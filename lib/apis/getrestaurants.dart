import 'dart:convert';
import 'dart:developer' as dev;

import 'package:requests/requests.dart';
import 'package:restaurant_user/model/get_restaurantsresponse.dart';
import 'package:restaurant_user/utils/all_constans.dart';

class RestaurantRepository {

  Future<List<Restaurants>> getRestaurants() async {
    print(base_url + "get_all_restaurants");

    dynamic response = await Requests.get(base_url + "get_all_restaurants",
         headers: {'Content-type': 'application/json'});

    print(base_url + "getRestaurants"+ response.toString());
   final res = json.decode(response);
    print("GETRESTAURANTS--"+ res['Restaurants'].toString());
    Iterable list = res['Restaurants'];
    return list.map((model) => Restaurants.fromJson(model)).toList();

  }



}


