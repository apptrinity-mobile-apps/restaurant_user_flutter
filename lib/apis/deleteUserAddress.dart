import 'dart:convert';
import 'dart:developer' as dev;

import 'package:requests/requests.dart';
import 'package:restaurant_user/model/get_addresslistresponse.dart';
import 'package:restaurant_user/model/get_restaurantsresponse.dart';
import 'package:restaurant_user/utils/all_constans.dart';

import 'addAddressResponse.dart';

class DeleteAddressRepository {

  Future<addAddressresponse> deleteUserAddress(String userId,String addressId) async {
    print(base_url + "delete_user_address");

    var body = json.encode({'userId': userId,'addressId': addressId,});

    print(body);
    dynamic response = await Requests.post(base_url + "delete_user_address", body: body, headers: {'Content-type': 'application/json'});

    print(base_url + "DELETEADDRESSRESPONSE"+ response.toString());

    addAddressresponse loginresult_data =
    addAddressresponse.fromJson(jsonDecode(response));
    return loginresult_data;

  }

}


