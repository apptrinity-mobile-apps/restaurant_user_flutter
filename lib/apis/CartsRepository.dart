import 'dart:convert';

import 'package:restaurant_user/model/cartmodelitemsapi.dart';
import 'package:shared_preferences/shared_preferences.dart';


class CartsRepository {

  Future<List<MenuCartItemapi>> getcartslisting() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final key = 'save_to_cartlist';
    final value = prefs.getString(key) ?? "[]";
    if(value==""){
      // print('read: $value');
      Iterable list = json.decode(value);
      return list;
    }else{
      // print('read: $value');
      Iterable list = json.decode(value);
      return list.map((model) => MenuCartItemapi.fromJson(model)).toList();
    }

  }



 /* static Future ClearCartSession() async {
    print("CLEARCART============");
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.remove('save_to_cartlist');
  }
*/
  static Future removesaved_in_cart() async {
    print("CLEARCART============");
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.remove('save_to_cartlist');
  }




}