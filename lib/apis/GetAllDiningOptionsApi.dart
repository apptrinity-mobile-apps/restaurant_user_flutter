import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:http/http.dart' as http;
import 'package:restaurant_user/model/getalldinningoptionsresponse.dart';
import 'package:restaurant_user/utils/all_constans.dart';
class GetAllDinningOptionsApiRepository {

  Future<List<DineInList>> getAllDinningOptions(String restaurant_id) async {
    var body = json.encode({'restaurantId':restaurant_id});
    print(base_url + "get_all_dinningoptions"+ body.toString());

    dynamic response = await Requests.post(base_url + "get_dinein_options",
        body: body, headers: {'Content-type': 'application/json'});

    print(base_url + "GETALLDINNINGRESPONSE"+ response.toString());
    final res = json.decode(response);
    Iterable list = res['dineInList'];
    return list.map((model) => DineInList.fromJson(model)).toList();

  }


}



