class addAddressresponse {
  int responseStatus;
  String result;

  addAddressresponse({this.responseStatus, this.result});

  addAddressresponse.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

