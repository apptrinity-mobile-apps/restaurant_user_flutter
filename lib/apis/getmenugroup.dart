import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:restaurant_user/model/getmenugroups.dart';
import 'package:restaurant_user/utils/all_constans.dart';

class GetMenuGroups {
  Future<List<MenugroupsList>> getmenu(
      String res_id,
      String menu_id,
      List dietTypelist
      ) async {

    var body = json.encode({
      'restaurantId': res_id,
      'menuId': menu_id,
      'dietTypeList': dietTypelist
    });
    print(body);
    dynamic response = await Requests.post(base_url + "get_all_menu_groups",
        body: body, headers: {'Content-type': 'application/json'});

    final res = json.decode(response);
    Iterable list = res['menugroupsList'];
    return list.map((model) => MenugroupsList.fromJson(model)).toList();

  }
}