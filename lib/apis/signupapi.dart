import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:http/http.dart' as http;
import 'package:restaurant_user/model/signupresponse.dart';
import 'package:restaurant_user/utils/all_constans.dart';
class SignUpRepository {
  Future<signupresponse> signup(String firstName,String lastName, String email, String phoneNumber, String password) async {
    var body = json.encode({'firstName':firstName,'lastName': lastName, 'email': email, 'phoneNumber':phoneNumber, 'password': password,});
    print(base_url + "SIGNUP" + "SIGNUP_REQUEST"+ body.toString());

    dynamic response = await Requests.post(base_url + "singup_user",
        body: body, headers: {'Content-type': 'application/json'});


    signupresponse signupresult_data =
    signupresponse.fromJson(jsonDecode(response));
    return signupresult_data;
  }


}

