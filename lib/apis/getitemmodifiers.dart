import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:restaurant_user/model/getitemmodifiersresponse.dart';
import 'package:restaurant_user/utils/all_constans.dart';

class GetItemModifiers {
  Future<List<ModifiersGroupsList>> getmenu(
      String res_id,
      String menu_item_id,
      String userId,
      ) async {

    var body = json.encode({
      'restaurantId': res_id,
      'menuItemId': menu_item_id,
      'userId': userId
    });
    print(base_url+"===="+body);
    dynamic response = await Requests.post(base_url + "get_modifer_and_modier_groups_for_menu_item",
        body: body, headers: {'Content-type': 'application/json'});

    final res = json.decode(response);
    Iterable list = res['modifiers_groups_list'];
    return list.map((model) => ModifiersGroupsList.fromJson(model)).toList();

  }
}