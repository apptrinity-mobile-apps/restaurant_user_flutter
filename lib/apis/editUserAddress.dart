import 'dart:convert';
import 'dart:developer' as dev;

import 'package:requests/requests.dart';
import 'package:restaurant_user/model/get_addresslistresponse.dart';
import 'package:restaurant_user/model/get_restaurantsresponse.dart';
import 'package:restaurant_user/utils/all_constans.dart';

import 'addAddressResponse.dart';

class EditAddressRepository {

  Future<addAddressresponse> editUserAddress(String fullAddress,String floor,String howToReach,String addressType,String latitude,String longitude,String userId,bool isDefault,String addressTag,String addressId) async {
    print(base_url + "edit_user_address");

    var body = json.encode({'fullAddress': fullAddress,'floor': floor,'howToReach': howToReach,'addressType': addressType,'latitude': latitude,'longitude': longitude,'userId': userId,'isDefault': isDefault,'addressTag': addressTag,'addressId': addressId,});

    print(body);
    dynamic response = await Requests.post(base_url + "edit_user_address", body: body, headers: {'Content-type': 'application/json'});

    print(base_url + "EDITADDRESSRESPONSE"+ response.toString());

    addAddressresponse loginresult_data =
    addAddressresponse.fromJson(jsonDecode(response));
    return loginresult_data;

  }

}


