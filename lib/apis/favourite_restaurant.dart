import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:http/http.dart' as http;
import 'package:restaurant_user/model/loginresponse.dart';
import 'package:restaurant_user/model/signupresponse.dart';
import 'package:restaurant_user/utils/all_constans.dart';
class FavouriteRestaurantRepository {
  Future<loginresponse> login(String orderId,String userId,String isFavorite) async {
    var body = json.encode({ 'orderId': orderId, 'userId': userId,'isFavorite':isFavorite});
    print(base_url + "FAVOURITE" + "FAVOURITE_REQUEST"+ body.toString());

    dynamic response = await Requests.post(base_url + "user_login",
        body: body, headers: {'Content-type': 'application/json'});

    loginresponse signupresult_data =
    loginresponse.fromJson(jsonDecode(response));
    return signupresult_data;
  }


}

