import 'dart:convert';

import 'package:requests/requests.dart';
import 'package:restaurant_user/model/getfavouriteordersresponse.dart';
import 'package:restaurant_user/model/getyourordersresponse.dart';
import 'package:restaurant_user/utils/all_constans.dart';

class GetFavouriteOrdersApi {

  Future<List<FavoriteOrders>> getFavouriteOrders(String customerId) async {
    print(base_url + "favorite_orders");
    var body = json.encode({'customerId': customerId});

    dynamic response = await Requests.post(base_url + "favorite_orders", body: body, headers: {'Content-type': 'application/json'});

    print(base_url + "favoriteOrders"+ response.toString());
    final res = json.decode(response);
    print("GETFAVOURITEORDERS--"+ res['favoriteOrders'].toString());
    Iterable list = res['favoriteOrders'];
    return list.map((model) => FavoriteOrders.fromJson(model)).toList();

  }


}
