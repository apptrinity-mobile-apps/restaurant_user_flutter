import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:restaurant_user/apis/getrestaurants.dart';
import 'package:restaurant_user/restaurant_search_screen2.dart';
import 'package:restaurant_user/restaurantdetail_screen.dart';
import 'package:restaurant_user/utils/all_constans.dart';
import 'package:restaurant_user/utils/sizeconfig.dart';

import 'utils/dottedline.dart';
import 'location_search.dart';
import 'model/get_restaurantsresponse.dart';

class ShowOrdersScreen extends StatefulWidget {
  final String current_latitude;
  final String current_longitude;
  final String current_location;

  const ShowOrdersScreen(this.current_latitude, this.current_longitude, this.current_location, {Key key}) : super(key: key);

  @override
  _ShowOrdersScreenState createState() => _ShowOrdersScreenState();
}

class _ShowOrdersScreenState extends State<ShowOrdersScreen> {
  int cart_count = 0;
  bool enable_searchlist = false;
  String userid = "";
  String employee_id = "";
  String first_name = "";
  String last_name = "";
  String restaurant_id = "";
  String servicearea_id = "";
  String user_id = "";
  double screenheight = 0.0;
  String returnVal = "";
  bool hasTimerStopped = false;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  String showgoutview = "order_location_icon.png";
  String orderscanner = "order_scanner.png";
  String restaurantsquare = "restaurant_square.png";
  String noorders = "no_order_dash.png";
  TextEditingController search_Controller = TextEditingController();
  List<Restaurants> getRestaurants = new List();
  final myController = TextEditingController();
  bool _loading = true;

  @override
  void initState() {
    super.initState();

    print("CURRENTLOCATION====" + widget.current_latitude + "=====" + widget.current_longitude + "=======" + widget.current_location);

    myController.text = widget.current_location;

    RestaurantRepository().getRestaurants().then((result_showalldayview) {
      setState(() {
        _loading = false;
        getRestaurants = result_showalldayview;
        print("RESTAURANTLENGTH=====" + getRestaurants.length.toString());
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    //  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: login_passcode_bg1));
    print("Height" + SizeConfig.screenHeight.toString());
    final space = SizedBox(height: 20);
    if (SizeConfig.screenHeight >= 600 && SizeConfig.screenHeight < 800) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 550 && SizeConfig.screenHeight < 600) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 800) {
      screenheight = 550;
    }
    final screen_height = MediaQuery.of(context).size.height;
    print(screen_height);

    return /*WillPopScope(
        onWillPop: () async => _onBackPressed(context),
        child: */SafeArea(
            child: Scaffold(
          resizeToAvoidBottomInset: true,
          backgroundColor: Colors.white,
          /*appBar: */ /*YourAppbar( height: SizeConfig.safeBlockHorizontal * 25,)*/ /*
          AppBar(
        toolbarHeight: 140,
        backgroundColor: order_appbar_bg,
        title: Column(//crossAxisAlignment: CrossAxisAlignment.start,
           children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Image.asset(
                'images/' + showgoutview,
                height: 30,
                width: 30,
              ),
              Column(
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.all(4.0),
                    width: 300,
                    child: Row(
                      children: <Widget>[
                        Flexible(
                          child: RichText(
                            overflow: TextOverflow.ellipsis,
                            text: TextSpan(
                                style: TextStyle(
                                  fontSize: 18,
                                  color: login_passcode_text,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w600,
                                ),
                                text: widget.current_location),
                          ),
                        ),
                      ],
                    ),
                  ),
                  DotWidget(
                    dashColor: Colors.black,
                    dashHeight: 1,
                    dashWidth: 10,
                    emptyWidth: 5,
                    //totalWidth: 220,
                  )
                ],
              ),
              InkWell(
                onTap: () {
                  */ /*Navigator.of(context).pushReplacement(MaterialPageRoute(
                      builder: (BuildContext context) => QRViewExample()));*/ /*
                },
                child: Container(
                    child: Image.asset(
                  'images/' + orderscanner,
                  height: 30,
                  width: 30,
                )),
              )
            ],
          ),
          space,
          InkWell(
            onTap: () {
              setState(() {
                print("SEARCH");
                Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (BuildContext context) => RestaurantSearchScreen2()));
              });
            },
            child: TextFormField(
              controller: search_Controller,
              autofocus: false,
              enabled: false,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(
                      color: location_border,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide(
                      color: login_form_hint,
                      width: 1.0,
                    ),
                  ),
                  filled: true,
                  fillColor: Colors.white,
                  prefixIcon: new Image.asset(
                    'images/search.png',
                    height: 10,
                    width: 10,
                    scale: 2.0,
                  ),
                  hintText: "Restaurant name , cuisine , or a dish...",
                  hintStyle: TextStyle(
                    color: location_text,
                    fontSize: 16,
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.w400,
                  ),
                  contentPadding: EdgeInsets.only(
                    bottom: 30 / 2,
                    left: 15 / 2,
                    right: 15 / 2, // HERE THE IMPORTANT PART
                    // HERE THE IMPORTANT PART
                  ),
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))),
            ),
          )
        ]),
      ),*/
          body: Container(
            color: order_appbar_bg,
            margin: EdgeInsets.fromLTRB(0, 0, 0, 80),
            child: Column(children: [
              Container(
                margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
                height: 120,
                child: Column(//crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Image.asset(
                        'images/' + showgoutview,
                        height: 30,
                        width: 30,
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => LocationSearch()),
                          );
                        },
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.all(4.0),
                              width: 300,
                              child: Row(
                                children: <Widget>[
                                  Flexible(
                                    child: RichText(
                                      overflow: TextOverflow.ellipsis,
                                      text: TextSpan(
                                          style: TextStyle(
                                            fontSize: 18,
                                            color: login_passcode_text,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w600,
                                          ),
                                          text: widget.current_location),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            DotWidget(
                              dashColor: Colors.black,
                              dashHeight: 1,
                              dashWidth: 10,
                              emptyWidth: 5,
                              //totalWidth: 220,
                            )
                          ],
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          /*Navigator.of(context).pushReplacement(MaterialPageRoute(
                      builder: (BuildContext context) => QRViewExample()));*/
                        },
                        child: Container(
                            child: Image.asset(
                          'images/' + orderscanner,
                          height: 30,
                          width: 30,
                        )),
                      )
                    ],
                  ),
                  space,
                  Container(
                    margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          print("SEARCH");
                          // Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (BuildContext context) => RestaurantSearchScreen2()));
                          Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => RestaurantSearchScreen2()));
                        });
                      },
                      child: TextFormField(
                        controller: search_Controller,
                        autofocus: false,
                        enabled: false,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: location_border,
                              ),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                color: login_form_hint,
                                width: 1.0,
                              ),
                            ),
                            filled: true,
                            fillColor: Colors.white,
                            prefixIcon: new Image.asset(
                              'images/search.png',
                              height: 10,
                              width: 10,
                              scale: 2.0,
                            ),
                            hintText: "Restaurant name , cuisine , or a dish...",
                            hintStyle: TextStyle(
                              color: location_text,
                              fontSize: 16,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w400,
                            ),
                            contentPadding: EdgeInsets.only(
                              bottom: 30 / 2,
                              left: 15 / 2,
                              right: 15 / 2, // HERE THE IMPORTANT PART
                              // HERE THE IMPORTANT PART
                            ),
                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8.0))),
                      ),
                    ),
                  )
                ]),
              ),
              _loading
                  ? Expanded(
                      child: SpinKitCircle(color: Colors.lightBlueAccent),
                    )
                  : getRestaurants.length == 0
                      ? Expanded(
                          child: Image.asset(
                            'images/' + noorders,
                            height: 500,
                            width: 500,
                          ),
                        )
                      : Expanded(
                          child: Container(
                              color: Colors.white,
                              child: ListView.builder(
                                  scrollDirection: Axis.vertical,
                                  itemCount: getRestaurants.length,
                                  itemBuilder: (context, index) {
                                    return Container(
                                        margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: getRestaurants[index].restaurantLogo != ""
                                                  ? NetworkImage(
                                                      getRestaurants[index].restaurantLogo,
                                                    )
                                                  : AssetImage(
                                                      'images/food_restaurant.png',
                                                    ),
                                              fit: BoxFit.cover),
                                        ),
                                        child: InkWell(
                                            onTap: () {
                                              setState(() {
                                                print("MANIBABU");
                                                // Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (BuildContext context) => RestaurantDetailScreen(getRestaurants[index].id)));
                                                Navigator.of(context).push(MaterialPageRoute(
                                                    builder: (BuildContext context) => RestaurantDetailScreen(getRestaurants[index].id)));
                                              });
                                            },
                                            child: Stack(
                                              overflow: Overflow.visible,
                                              children: [
                                                Container(
                                                    margin: EdgeInsets.only(top: 0, left: 0, right: 0),
                                                    width: double.infinity,
                                                    height: 300,
                                                    child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
                                                      Container(
                                                        height: 140,
                                                        alignment: Alignment.bottomCenter,
                                                        margin: EdgeInsets.only(top: 150, left: 10, right: 10),
                                                        child: Card(
                                                            margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                            color: Colors.white,
                                                            elevation: 3,
                                                            child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                                                              Padding(
                                                                  padding: EdgeInsets.fromLTRB(15, 10, 0, 5),
                                                                  child: Text(
                                                                    getRestaurants[index].restaurantName,
                                                                    textAlign: TextAlign.left,
                                                                    style: TextStyle(
                                                                      fontSize: 18,
                                                                      color: login_passcode_text,
                                                                      fontFamily: 'Poppins',
                                                                      fontWeight: FontWeight.w600,
                                                                    ),
                                                                  )),
                                                              Padding(
                                                                  padding: EdgeInsets.fromLTRB(15, 0, 0, 5),
                                                                  child: Text(
                                                                    getRestaurants[index].restaurantDescription,
                                                                    textAlign: TextAlign.start,
                                                                    style: TextStyle(
                                                                      fontSize: 14,
                                                                      color: login_passcode_text,
                                                                      fontFamily: 'Poppins',
                                                                      fontWeight: FontWeight.w400,
                                                                    ),
                                                                  )),
                                                              Container(
                                                                margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
                                                                child: Row(
                                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                  children: [
                                                                    Row(
                                                                      children: [
                                                                        Padding(
                                                                          padding: EdgeInsets.fromLTRB(15, 0, 0, 5),
                                                                          child: Image.asset(
                                                                            'images/star.png',
                                                                            height: 15,
                                                                            width: 15,
                                                                          ),
                                                                        ),
                                                                        Padding(
                                                                            padding: EdgeInsets.fromLTRB(2, 0, 0, 5),
                                                                            child: Text(
                                                                              "4.5/5",
                                                                              textAlign: TextAlign.left,
                                                                              style: TextStyle(
                                                                                fontSize: 14,
                                                                                color: login_passcode_text,
                                                                                fontFamily: 'Poppins',
                                                                                fontWeight: FontWeight.w600,
                                                                              ),
                                                                            )),
                                                                      ],
                                                                    ),
                                                                    Row(
                                                                      children: [
                                                                        Padding(
                                                                          padding: EdgeInsets.fromLTRB(15, 0, 0, 5),
                                                                          child: Image.asset(
                                                                            'images/timer.png',
                                                                            height: 15,
                                                                            width: 15,
                                                                          ),
                                                                        ),
                                                                        Padding(
                                                                            padding: EdgeInsets.fromLTRB(2, 0, 0, 5),
                                                                            child: Text(
                                                                              "15-20 Min",
                                                                              textAlign: TextAlign.left,
                                                                              style: TextStyle(
                                                                                fontSize: 14,
                                                                                color: login_passcode_text,
                                                                                fontFamily: 'Poppins',
                                                                                fontWeight: FontWeight.w600,
                                                                              ),
                                                                            )),
                                                                      ],
                                                                    ),
                                                                    Row(
                                                                      children: [
                                                                        Padding(
                                                                          padding: EdgeInsets.fromLTRB(15, 0, 0, 5),
                                                                          child: Text(
                                                                            "20.00",
                                                                            textAlign: TextAlign.left,
                                                                            style: TextStyle(
                                                                              fontSize: 14,
                                                                              color: login_passcode_text,
                                                                              fontFamily: 'Poppins',
                                                                              fontWeight: FontWeight.w800,
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        Padding(
                                                                            padding: EdgeInsets.fromLTRB(2, 0, 15, 5),
                                                                            child: Text(
                                                                              "For One",
                                                                              textAlign: TextAlign.left,
                                                                              style: TextStyle(
                                                                                fontSize: 14,
                                                                                color: login_passcode_text,
                                                                                fontFamily: 'Poppins',
                                                                                fontWeight: FontWeight.w400,
                                                                              ),
                                                                            )),
                                                                      ],
                                                                    )
                                                                  ],
                                                                ),
                                                              ),
                                                              Padding(
                                                                padding: EdgeInsets.fromLTRB(10, 5, 0, 0),
                                                                child: DotWidget(
                                                                  dashColor: login_passcode_text,
                                                                  dashHeight: 1,
                                                                  dashWidth: 10,
                                                                  emptyWidth: 2,
                                                                  totalWidth: 330,
                                                                ),
                                                              ),
                                                              Row(
                                                                children: [
                                                                  Padding(
                                                                    padding: EdgeInsets.fromLTRB(20, 10, 0, 5),
                                                                    child: Image.asset(
                                                                      'images/user.png',
                                                                      height: 12,
                                                                      width: 12,
                                                                    ),
                                                                  ),
                                                                  Padding(
                                                                      padding: EdgeInsets.fromLTRB(8, 5, 0, 0),
                                                                      child: Text(
                                                                        "2150+ people ordered from here since lockdown",
                                                                        textAlign: TextAlign.left,
                                                                        style: TextStyle(
                                                                          fontSize: 13,
                                                                          color: login_passcode_text,
                                                                          fontFamily: 'Poppins',
                                                                          fontWeight: FontWeight.w400,
                                                                        ),
                                                                      )),
                                                                ],
                                                              )
                                                            ])),
                                                      ),
                                                    ])),
                                                Positioned(
                                                  top: -2,
                                                  right: 30,
                                                  child: InkWell(
                                                      onTap: () {
                                                        setState(() {
                                                          print("WISHLIST");
                                                        });
                                                      },
                                                      child: Container(
                                                        margin: EdgeInsets.only(top: 0),
                                                        decoration: BoxDecoration(
                                                          color: Colors.white,
                                                          borderRadius: BorderRadius.only(
                                                              topLeft: Radius.circular(0),
                                                              bottomLeft: Radius.circular(30),
                                                              topRight: Radius.circular(0),
                                                              bottomRight: Radius.circular(30)),
                                                        ),
                                                        alignment: Alignment.center,
                                                        width: 40,
                                                        height: 50,
                                                        child: Image.asset(
                                                          'images/wishlist.png',
                                                          width: 20,
                                                          height: 50,
                                                        ),
                                                      )),
                                                ),
                                              ],
                                            )));
                                  })),
                        )
            ]),
          ),
        ))/*)*/;
  }
}

Future<bool> _onBackPressed(BuildContext context) async {
  exit(0);
}