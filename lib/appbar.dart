import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'cart.dart';

class YourAppbar extends StatelessWidget implements PreferredSizeWidget {
  final double height;
  final String type;
  final int count;

  const YourAppbar({
    Key key,
    @required this.height,
    this.type,
    this.count,
  }) : super(key: key);

  @override
  Size get preferredSize => Size.fromHeight(height);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          AppBar(
            toolbarHeight: height,
            //toolbarHeight:  height-30.00,
            automaticallyImplyLeading: false,
            elevation: 0.0,
            backgroundColor: Colors.white,
            centerTitle: true,
            leading: Builder(
              builder: (BuildContext context) {
                return IconButton(
                  padding: EdgeInsets.only(left: 12.0),
                  icon: Image.asset("images/back_arrow.png", width: 20, height: 20),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                );
              },
            ),
            actions: [
              new Padding(
                padding: const EdgeInsets.only(right: 25),
                child: new Container(
                    alignment: Alignment.centerRight,
                    height: 48.0,
                    width: 48.0,
                    child: new GestureDetector(
                      onTap: () {
                        Navigator.push(
                              context,MaterialPageRoute(builder: (context) => CartScreen("Cash","","","","","","","","")));
                      },
                      child: new Stack(
                        children: <Widget>[
                          Image.asset("images/cart_icon.png"),
                          1 == 0
                              ? new Container()
                              : new Positioned(
                                  top: -2.0,
                                  right: 0,
                                  child: new Stack(
                                    children: <Widget>[
                                      new Icon(Icons.brightness_1, size: 28.0, color: Colors.lightBlueAccent),
                                      new Positioned(
                                          top: 5.0,
                                          right: 12.0,
                                          child: new Center(
                                            child: new Text(
                                              '$count',
                                              style: new TextStyle(color: Colors.white, fontSize: 11.0, fontWeight: FontWeight.bold),
                                            ),
                                          )),
                                    ],
                                  )),
                        ],
                      ),
                    )),
              )
            ],
          ),
        ],
      ),
    );
  }
}
