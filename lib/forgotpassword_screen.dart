import 'dart:io';
import 'dart:ui';

import 'package:async/async.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:restaurant_user/apis/forgotpasswordapi.dart';
import 'package:restaurant_user/apis/resetpasswordapi.dart';
import 'package:restaurant_user/login_screen.dart';
import 'package:restaurant_user/signup_screen.dart';
import 'package:restaurant_user/utils/all_constans.dart';
import 'package:restaurant_user/utils/back_press.dart';
import 'package:restaurant_user/utils/sizeconfig.dart';
import 'package:toast/toast.dart';

import 'apis/loginapi.dart';
import 'apis/signupapi.dart';
import 'location_search.dart';

class ForgotPassword extends StatefulWidget {
  /*LoginPage({Key key, this.title}) : super(key: key);
  final String title;*/

  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> with TickerProviderStateMixin {
  final _formKey = GlobalKey<FormState>();
  final _pinPutFocusNode = FocusNode();
  final _pinPutController = TextEditingController();
  List<double> _stops = [0.0, 0.7];
  CancelableOperation cancelableOperation;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  String restaurant_id = "";
  String restaurant_code = "";
  bool _loading = true;
  String deviceName = "";
  int _currentIndex = 0;
  String is_platformtype = "";

  List<String> widgetList = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '', '0', '*'];
  final BoxDecoration pinPutDecoration = BoxDecoration(
    color: const Color.fromRGBO(235, 236, 237, 1),
    borderRadius: BorderRadius.circular(0.0),
  );

  TextEditingController otp_Controller = TextEditingController();
  TextEditingController email_Controller = TextEditingController();
  TextEditingController newpassword_Controller = TextEditingController();
  TextEditingController confirmnewpassword_Controller = TextEditingController();
  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  TabController _tabController;

  @override
  void initState() {
    super.initState();

    initPlatformState();
  }

  Future<void> initPlatformState() async {
    Map<String, dynamic> deviceData;
    try {
      if (Platform.isAndroid) {
        is_platformtype = "android";
        //print("DEVICEDATA"+deviceData.toString());
        var build = await deviceInfoPlugin.androidInfo;
        deviceName = build.device.toString();
        print("DEVICENAME" + deviceName.toString() + "--------" + is_platformtype);
      } else if (Platform.isIOS) {
        is_platformtype = "ios";
        var build = await deviceInfoPlugin.iosInfo;
        deviceName = build.name.toString();
        print("DEVICENAME" + deviceName.toString() + "-------" + is_platformtype);
      }
    } on PlatformException {
      deviceData = <String, dynamic>{'Error:': 'Failed to get platform version.'};
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return WillPopScope(
        onWillPop: () async => onBackPressed(context),
        child:Scaffold(
        appBar: AppBar(
          toolbarHeight: 56,
          //toolbarHeight:  height-30.00,
          automaticallyImplyLeading: false,
          elevation: 0.0,
          backgroundColor: Colors.white,
          centerTitle: true,
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                padding: EdgeInsets.only(left: 12.0),
                icon: Image.asset("images/back_arrow.png", width: 20, height: 20),
                onPressed: () {
                  //Navigator.of(context).pop();
                  // Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (BuildContext context) => LoginPage()));
                  onBackPressed(context);
                },
              );
            },
          ),
        ),
        body: Center(
          child: Column(
            children: <Widget>[
              SizedBox(height: 50),
              Text(
                'FORGOT PASSWORD',
                style: TextStyle(
                  color: add_food_item_bg,
                  fontSize: SizeConfig.blockSizeHorizontal * 6,
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w600,
                ),
              ),
              SizedBox(height: 0),
              Expanded(child: _username_widget())
            ],
          ),
        )));
  }

  Widget _username_widget() {
    return SingleChildScrollView(
        child: Container(
            margin: EdgeInsets.fromLTRB(10.00, 30.00, 10.00, 0.00),
            child: Column(children: [
              Container(
                child: new Form(
                  key: _formKey, //padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
                  child: new Column(
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                height: 5.0,
                              ),
                              Container(
                                  margin: EdgeInsets.only(bottom: 8),
                                  child: Text(
                                    'Otp',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      color: text_hint_color,
                                      fontSize: SizeConfig.blockSizeHorizontal * 2.8,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w500,
                                    ),
                                  )),
                              TextFormField(
                                validator: (val) {
                                  if (val.isEmpty) return 'Enter Otp';
                                  return null;
                                },
                                controller: otp_Controller,
                                obscureText: false,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0.0),
                                      borderSide: BorderSide(
                                        color: login_passcode_bg2,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0.0),
                                      borderSide: BorderSide(
                                        color: login_form_hint,
                                        width: 1.0,
                                      ),
                                    ),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: "Enter Otp",
                                    hintStyle: TextStyle(
                                      color: login_form_hint,
                                      fontSize: SizeConfig.blockSizeHorizontal * 2.8,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w400,
                                    ),
                                    contentPadding: EdgeInsets.only(
                                      bottom: 30 / 2,
                                      left: 50 / 2, // HERE THE IMPORTANT PART
                                      // HERE THE IMPORTANT PART
                                    ),
                                    border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))),
                              ),
                              Container(
                                  margin: EdgeInsets.only(top: 10, bottom: 8),
                                  child: Text(
                                    'Email',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      color: text_hint_color,
                                      fontSize: SizeConfig.blockSizeHorizontal * 2.8,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w500,
                                    ),
                                  )),
                              TextFormField(
                                validator: (val) {
                                  if (val.isEmpty) return 'Enter Email';
                                  return null;
                                },
                                controller: email_Controller,
                                obscureText: false,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0.0),
                                      borderSide: BorderSide(
                                        color: login_passcode_bg2,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0.0),
                                      borderSide: BorderSide(
                                        color: login_form_hint,
                                        width: 1.0,
                                      ),
                                    ),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: "Enter Email",
                                    hintStyle: TextStyle(
                                      color: login_form_hint,
                                      fontSize: SizeConfig.blockSizeHorizontal * 2.8,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w400,
                                    ),
                                    contentPadding: EdgeInsets.only(
                                      bottom: 30 / 2,
                                      left: 50 / 2, // HERE THE IMPORTANT PART
                                      // HERE THE IMPORTANT PART
                                    ),
                                    border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))),
                              ),
                              Container(
                                  margin: EdgeInsets.only(top: 10, bottom: 8),
                                  child: Text(
                                    'New Password',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      color: text_hint_color,
                                      fontSize: SizeConfig.blockSizeHorizontal * 2.8,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w500,
                                    ),
                                  )),
                              TextFormField(
                                validator: (val) {
                                  if (val.isEmpty) return 'Enter New Password';
                                  return null;
                                },
                                controller: newpassword_Controller,
                                obscureText: false,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0.0),
                                      borderSide: BorderSide(
                                        color: login_passcode_bg2,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0.0),
                                      borderSide: BorderSide(
                                        color: login_form_hint,
                                        width: 1.0,
                                      ),
                                    ),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: "Enter New Password",
                                    hintStyle: TextStyle(
                                      color: login_form_hint,
                                      fontSize: SizeConfig.blockSizeHorizontal * 2.8,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w400,
                                    ),
                                    contentPadding: EdgeInsets.only(
                                      bottom: 30 / 2,
                                      left: 50 / 2, // HERE THE IMPORTANT PART
                                      // HERE THE IMPORTANT PART
                                    ),
                                    border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))),
                              ),
                              Container(
                                  margin: EdgeInsets.only(top: 10, bottom: 8),
                                  child: Text(
                                    'Confirm New Password',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      color: text_hint_color,
                                      fontSize: SizeConfig.blockSizeHorizontal * 2.8,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w500,
                                    ),
                                  )),
                              TextFormField(
                                validator: (val) {
                                  if (val.isEmpty) return 'Enter Confirm New Password';
                                  return null;
                                },
                                controller: confirmnewpassword_Controller,
                                obscureText: false,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0.0),
                                      borderSide: BorderSide(
                                        color: login_passcode_bg2,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(0.0),
                                      borderSide: BorderSide(
                                        color: login_form_hint,
                                        width: 1.0,
                                      ),
                                    ),
                                    filled: true,
                                    fillColor: Colors.white,
                                    hintText: "Enter Confirm new Password",
                                    hintStyle: TextStyle(
                                      color: login_form_hint,
                                      fontSize: SizeConfig.blockSizeHorizontal * 2.8,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w400,
                                    ),
                                    contentPadding: EdgeInsets.only(
                                      bottom: 30 / 2,
                                      left: 50 / 2, // HERE THE IMPORTANT PART
                                      // HERE THE IMPORTANT PART
                                    ),
                                    border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))),
                              ),
                            ],
                          )),
                      Container(
                          margin: EdgeInsets.fromLTRB(25, 25, 25, 0),
                          // height: 50,
                          width: double.infinity,
                          // alignment: Alignment.center,
                          decoration: BoxDecoration(color: login_passcode_bg1, borderRadius: BorderRadius.circular(0)),
                          child: TextButton(
                              // minWidth: double.infinity,
                              // height: double.infinity,
                              child: Text("SUBMIT",
                                  style: TextStyle(fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w800, color: Colors.white)),
                              onPressed: () {
                                if (_formKey.currentState.validate()) {
                                  _formKey.currentState.save();
                                  // SignDialogs.showLoadingDialog(context, "Signing in", _keyLoader);
                                  cancelableOperation?.cancel();
                                  CancelableOperation.fromFuture(Future.delayed(Duration(seconds: 1), () {
                                    ForgotPasswordRepository()
                                        .forgotPassword(otp_Controller.text.toString(), email_Controller.text.toString(),
                                            newpassword_Controller.text.toString(), confirmnewpassword_Controller.text.toString())
                                        .then((value) {
                                      if (value.responseStatus == 0) {
                                        // Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
                                        print(value.result);
                                        Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                                      } else if (value.responseStatus == 1) {
                                        print(value.result);
                                        Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                                        Navigator.of(context)
                                            .pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => LoginPage()), (route) => false);
                                      } else {
                                        Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                                      }
                                    });
                                  }));
                                }
                              })),
                      Container(
                        margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                // alignment: Alignment.center,
                                child: TextButton(
                                    child: Text("Sign up!",
                                        style: TextStyle(fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w800, color: receipt_header)),
                                    onPressed: () {
                                      Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => SignUpPage()));
                                    })),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ])));
  }
}
