import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geocoder/geocoder.dart';
import 'package:restaurant_user/dashboard.dart';
import 'package:restaurant_user/session/userRepository.dart';
import 'package:restaurant_user/utils/all_constans.dart';
import 'package:restaurant_user/utils/back_press.dart';

class SearchLocationData extends StatefulWidget {
  @override
  _SearchLocationDataState createState() => _SearchLocationDataState();
}

class _SearchLocationDataState extends State<SearchLocationData> {
  BitmapDescriptor pinLocationIcon;
  Set<Marker> _markers = {};
  Completer<GoogleMapController> _controller = Completer();

  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Position _currentPosition;
  LatLng pinPosition;
  TextEditingController location_Controller = TextEditingController();
  var current_address;
  var current_lat;
  var current_long;
  CameraPosition initialLocation;
  final Marker marker = Marker(icon: BitmapDescriptor.fromAsset('images/current_location.png'));

  BitmapDescriptor customIcon;

  _getCurrentLocation() {
    geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.best).then((Position position) {
      setState(() {
        _currentPosition = position;
        if (_currentPosition.latitude != null && _currentPosition.longitude != null) {
          pinPosition = LatLng(_currentPosition.latitude, _currentPosition.longitude);
          initialLocation = CameraPosition(zoom: 16, bearing: 30, target: pinPosition);
        }
        });
      _getLocation();
      setCustomMapPin();
    }).catchError((e) {
      print(e);
    });
  }

  @override
  void initState() {
    super.initState();

    if (_currentPosition != null || _currentPosition != 0) {
      _getCurrentLocation();
    }
  }

  /*_getAddressFromLatLng() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() {
        print("Locationname"+place.name+""+place.subLocality+""+_currentPosition.latitude.toString()+"------"+_currentPosition.longitude.toString());
        */ /*_currentAddress =
        "${place.locality}, ${place.postalCode}, ${place.country}";*/ /*
      });
    } catch (e) {
      print(e);
    }
  }
*/

  _getLocation() async {
    Position position = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    debugPrint('location: ${position.latitude}');
    final coordinates = new Coordinates(position.latitude, position.longitude);
    var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;

    print("${first.featureName} : ${first.addressLine}");
    debugPrint('LOCATIONADDRESS: ${first.featureName}+"======="+${first.addressLine}');

    current_address = first.addressLine;
    location_Controller.text = first.addressLine.toString();
    current_lat = position.latitude.toString();
    current_long = position.longitude.toString();

    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(target: current_address, zoom: 16),
      ),
    );
  }

  _getLocation2(double draglatitude, double draglongitude) async {
    print("DRAGLATLONGS" + draglatitude.toString() + "======" + draglongitude.toString());

    final coordinates = new Coordinates(draglatitude, draglongitude);
    var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;

    print("${first.featureName} : ${first.addressLine}");
    debugPrint('LOCATIONADDRESS: ${first.featureName}+"======="+${first.addressLine}');

    current_address = first.addressLine;
    location_Controller.text = first.addressLine.toString();
    current_lat = draglatitude.toString();
    current_long = draglongitude.toString();

    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(target: current_address, zoom: 16),
      ),
    );
  }

  void setCustomMapPin() async {
    pinLocationIcon = await BitmapDescriptor.fromAsset('images/current_location_icon.png');
  }

  /*Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png)).buffer.asUint8List();
  }
*/

  /* void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }*/

  @override
  Widget build(BuildContext context) {
    print("CURRENT_LOCATION " + _currentPosition.latitude.toString() + "========" + _currentPosition.longitude.toString());
    /*if (_currentPosition.latitude != null && _currentPosition.longitude != null) {
      pinPosition = LatLng(_currentPosition.latitude, _currentPosition.longitude);
      initialLocation = CameraPosition(zoom: 16, bearing: 30, target: pinPosition);
    }*/

    void getLocation() async {
      setState(() {
        if (_currentPosition.latitude != null && _currentPosition.longitude != null) {
          pinPosition = LatLng(_currentPosition.latitude, _currentPosition.longitude);
        }
      });
    }

    // these are the minimum required values to set
    // the camera position
    //  initialLocation = CameraPosition(zoom: 16, bearing: 30, target: pinPosition);

    return WillPopScope(
        onWillPop: () async => onBackPressed(context),
        child: MaterialApp(
          home: Scaffold(
            bottomSheet: Container(
                margin: EdgeInsets.fromLTRB(40, 15, 40, 20),
                width: double.infinity,
                alignment: Alignment.center,
                height: 200,
                child: Column(
                  children: [
                    Align(
                        alignment: Alignment.topLeft,
                        child: Text(
                          'Select your location',
                          style: new TextStyle(color: login_passcode_text, fontSize: 18.0, fontWeight: FontWeight.bold),
                        )),
                    SizedBox(
                      height: 0,
                    ),
                    Container(
                        alignment: Alignment.topLeft,
                        margin: EdgeInsets.only(bottom: 15, top: 10),
                        child: Text(
                          'Your Location',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: text_hint_color,
                            fontSize: 14,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w500,
                          ),
                        )),
                    TextFormField(
                      controller: location_Controller,
                      obscureText: false,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(0.0),
                            borderSide: BorderSide(
                              color: location_border,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(0.0),
                            borderSide: BorderSide(
                              color: login_form_hint,
                              width: 1.0,
                            ),
                          ),
                          filled: true,
                          fillColor: Colors.white,
                          prefixIcon: new Image.asset(
                            'images/current_location_icon.png',
                            height: 0,
                            width: 0,
                            scale: 3.0,
                          ),
                          hintText: location_Controller.text,
                          hintStyle: TextStyle(
                            color: location_text,
                            fontSize: 16,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w400,
                          ),
                          contentPadding: EdgeInsets.only(
                            bottom: 30 / 2,
                            left: 15 / 2,
                            right: 15 / 2, // HERE THE IMPORTANT PART
                            // HERE THE IMPORTANT PART
                          ),
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))),
                    ),
                    Container(
                        margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                        // height: 50,
                        width: double.infinity,
                        // alignment: Alignment.center,
                        decoration: BoxDecoration(color: login_passcode_bg1, borderRadius: BorderRadius.circular(0)),
                        child: TextButton(
                            // minWidth: double.infinity,
                            // height: double.infinity,
                            child: Text("CONFIRM LOCATION & PROCEED",
                                style: TextStyle(fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w800, color: Colors.white)),
                            onPressed: () {
                              UserRepository.save_location(current_lat, current_long, location_Controller.text);
                              Navigator.of(context).pushReplacement(MaterialPageRoute(
                                  builder: (BuildContext context) => HomePage(current_lat, current_long, location_Controller.text)));
                              /*if (_formKey.currentState.validate()) {
                              _formKey.currentState.save();
                              // SignDialogs.showLoadingDialog(context, "Signing in", _keyLoader);
                              cancelableOperation?.cancel();
                              CancelableOperation.fromFuture(Future.delayed(Duration(seconds: 1), () {

                              }));
                            }*/
                            }))
                  ],
                )),
            body: _currentPosition == null
                ? Center(
              child: SpinKitPulse(color: Colors.lightBlueAccent),
            )
                : GoogleMap(
                myLocationEnabled: true,
                // compassEnabled: true,
                markers: _markers,
                scrollGesturesEnabled: true,
                zoomGesturesEnabled: true,
                rotateGesturesEnabled: true,
                tiltGesturesEnabled: true,
                initialCameraPosition: initialLocation,
                onCameraMove: ((_position) => _updatePosition(_position)),
                onMapCreated: (GoogleMapController controller) {
                  //controller.
                  _controller.complete(controller);

                  setState(() {
                    _markers.add(Marker(markerId: MarkerId('<MARKER_ID>'), position: pinPosition, icon: pinLocationIcon));
                  });
                }),
          ),
        ));
  }

  void _updatePosition(CameraPosition _position) {
    print('inside updatePosition ${_position.target.latitude} ${_position.target.longitude}');
    Marker marker = _markers.firstWhere((p) => p.markerId == MarkerId('marker_2'), orElse: () => null);

    _markers.remove(marker);
    _markers.add(
      Marker(
        markerId: MarkerId('marker_2'),
        position: LatLng(_position.target.latitude, _position.target.longitude),
        draggable: true,
        icon: pinLocationIcon,
      ),
    );
    setState(() {
      _getLocation2(_position.target.latitude, _position.target.longitude);
    });
  }
}

class Utils {
  static String mapStyles = '''[
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
]''';
}
