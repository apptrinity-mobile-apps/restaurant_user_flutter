import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:requests/requests.dart';
import 'package:restaurant_user/appbar_cart.dart';
import 'package:restaurant_user/model/get_customerdetailsresponse.dart';
import 'package:restaurant_user/payment_types_screen.dart';
import 'package:restaurant_user/session/userRepository.dart';
import 'package:restaurant_user/success_screen.dart';
import 'package:restaurant_user/utils/Globals.dart';
import 'package:restaurant_user/utils/all_constans.dart';
import 'package:restaurant_user/utils/back_press.dart';
import 'package:restaurant_user/utils/sizeconfig.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:toast/toast.dart';

import 'apis/CartsRepository.dart';
import 'apis/adduserrorder.dart';
import 'apis/getAddressLists.dart';
import 'apis/getOrderDetailApi.dart';
import 'apis/getrestaurantsOnlineOrderingRules.dart';
import 'utils/dottedline.dart';
import 'model/cartmodelitemsapi.dart';
import 'model/getViewOrderDetailResponse.dart';
import 'model/get_addaddressresponse.dart';
import 'model/get_addresslistresponse.dart';
import 'model/get_restaurantsonlineorderingresponse.dart';
import 'model/get_restaurantsresponse.dart';
import 'model/getallmenus.dart';
import 'model/getitemmodifiersresponse.dart';
import 'model/getitemmodifierssubmitresponse.dart';
import 'model/getmenugroups.dart';
import 'model/getmenuitems.dart';

class GetMenuItems {
  Future<List<MenuItem>> getmenu(
    String menu_id,
    String menu_groupid,
    String res_id,
  ) async {
    var body = json.encode({'menuId': menu_id, 'menuGroupId': menu_groupid, 'restaurantId': res_id});
    print(body);
    dynamic response = await Requests.post(base_url_waiter + "menu_items", body: body, headers: {'Content-type': 'application/json'});
    print("resultadditem---" + response);
    final res = json.decode(response);
    //print("result---"+res);
    if (res['responseStatus'] == 1) {
      Iterable list = res['menuItem'];
      return list.map((model) => MenuItem.fromJson(model)).toList();
    } else {
      return [];
    }
  }
}

class CartScreen extends StatefulWidget {
  final String payment_type;
  final String dine_in_id;
  final String dine_in_name;
  final String dine_in_behaviour;
  final String delivery_location;
  final String delivery_lattitude;
  final String delivery_longitude;
  final String restaurant_id;
  final String order_id;

  const CartScreen(this.payment_type, this.dine_in_id, this.dine_in_name, this.dine_in_behaviour, this.delivery_location, this.delivery_lattitude,
      this.delivery_longitude, this.restaurant_id, this.order_id,
      {Key key})
      : super(key: key);

  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  int cart_count = 0;
  bool enable_searchlist = false;
  String employee_id = "";
  String first_name = "";
  String last_name = "";
  String restaurant_id = "";
  String servicearea_id = "";
  String payment_type = "";
  String user_id = "";
  double screenheight = 0.0;
  String returnVal = "";
  bool hasTimerStopped = false;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  String showgoutview = "order_location_icon.png";
  String downarrow = "down_arrow.png";
  String restaurantsquare = "restaurant_square.png";
  String noorders = "no_order_dash.png";
  TextEditingController search_Controller = TextEditingController();
  List<Restaurants> getRestaurants = new List();
  bool isSwitched = false;
  bool isSwitchedveg = false;

  List<MenuList> _get_all_menus = new List();
  List<MenugroupsList> _get_menu_groups = new List();
  bool _loading = true;

  int selected_pos = -1;
  String res_id = "60e41f7f59263dad5bcb64a7";
  String menu_id = "";
  String menu_groupid = "";
  List<MenuItem> _get_all_menuitems = new List();
  List<ModifiersGroupsList> _get_all_menuitemsmodifiersgrouplist;
  List<ModifierList> _get_all_menuitemsmodifierslist;
  List<String> selectedCheckbox = [];

  int selected = 0;
  var counter_value = 1;

  List<MenuCartItemapi> __cart_items_list = new List();

  double _items_cart_itemtotal = 0.0;
  double _items_cart_discounttotal = 0.0;
  double _items_cart_modifier_price = 0.0;
  double _items_cart_special_price = 0.0;
  double _items_cart_subtotal = 0.0;
  double _items_cart_taxtotal = 0.0;
  double _items_cart_taxrate = 0.0;
  double _items_cart_grandtotal = 0.0;
  double _items_cart_grandroundup = 0.0;

  double total_discountitemprice = 0.0;
  double total_discountitemfinalprice = 0.0;
  double max_item_discount_amount = 0.0;
  List<ApprovalRules> _approved_list = new List();
  List<OrderItems> _viewOrder_list = new List();

  String del_dineinid = "";
  String del_dinename = "";
  String del_dineinbehaviour = "";

  final List<String> numbers = [
    "1",
    "2",
    "5",
    "7",
    "8",
    "10",
    "12",
    "15",
    "20",
  ];
  double selected_tip = 0.0;
  double _payment_grandroundup = 0.0;
  List<UserAddressBookList> getAddressLists = new List();

  bool restaurant_rules_DeliveryStatus = false;
  bool restaurant_rules_OrderAcceptanceStatus = false;
  bool restaurant_rules_Enabled_Status = false;
  bool restaurant_rules_TakeOutStatus = false;

  Widget _incrementButton(int index) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.lightBlueAccent,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(15), bottomLeft: Radius.circular(15), topRight: Radius.circular(15), bottomRight: Radius.circular(15)),
      ),
      margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
      child: InkWell(
        splashColor: login_passcode_bg2,
        // inkwell color
        child: SizedBox(
            width: 30,
            height: 30,
            child: Icon(
              Icons.add,
              color: login_passcode_text,
              size: 17,
            )),
        onTap: () {
          setState(() {
            __cart_items_list[index].quantity = (int.parse(__cart_items_list[index].quantity) + 1).toString();
            print("INCREMENTQUANTITY" + __cart_items_list[index].quantity);
            _savecartList(__cart_items_list);
            _cart_itemscalculation();
          });
        },
      ),
    );
  }

  Widget _increment_YourOrderButton(int index) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.lightBlueAccent,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(15), bottomLeft: Radius.circular(15), topRight: Radius.circular(15), bottomRight: Radius.circular(15)),
      ),
      margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
      child: InkWell(
        splashColor: login_passcode_bg2,
        // inkwell color
        child: SizedBox(
            width: 30,
            height: 30,
            child: Icon(
              Icons.add,
              color: login_passcode_text,
              size: 17,
            )),
        onTap: () {
          setState(() {
            print("INCREMENTYOURORDER" + _viewOrder_list[index].quantity.toString());
            _viewOrder_list[index].quantity = (_viewOrder_list[index].quantity + 1);
            print("INCREMENTQUANTITY" + _viewOrder_list[index].quantity.toString());
            _savecartList(_viewOrder_list);
            _cart_youritemscalculation();
          });
        },
      ),
    );
  }

  Widget _decrementButton(int index) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.lightBlueAccent,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(15), bottomLeft: Radius.circular(15), topRight: Radius.circular(15), bottomRight: Radius.circular(15)),
      ),
      margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
      child: InkWell(
        splashColor: login_passcode_bg2,
        // inkwell color
        child: SizedBox(
            width: 30,
            height: 30,
            child: Icon(
              Icons.remove,
              color: login_passcode_text,
              size: 17,
            )),
        onTap: () {
          setState(() {
            if (int.parse(__cart_items_list[index].quantity) > 1) {
              __cart_items_list[index].quantity = (int.parse(__cart_items_list[index].quantity) - 1).toString();
              //_savecartList(__cart_items_list);

            } else {
              __cart_items_list.removeAt(index);
              getCartItemModelapi.removeAt(index);
              //__cart_items_list.removeAt(index);

            }
            _savecartList(__cart_items_list);

            _cart_itemscalculation();
          });
        },
      ),
    );
  }

  Widget _decrementYourorderButton(int index) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.lightBlueAccent,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(15), bottomLeft: Radius.circular(15), topRight: Radius.circular(15), bottomRight: Radius.circular(15)),
      ),
      margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
      child: InkWell(
        splashColor: login_passcode_bg2,
        // inkwell color
        child: SizedBox(
            width: 30,
            height: 30,
            child: Icon(
              Icons.remove,
              color: login_passcode_text,
              size: 17,
            )),
        onTap: () {
          setState(() {
            if (_viewOrder_list[index].quantity > 1) {
              _viewOrder_list[index].quantity = (_viewOrder_list[index].quantity - 1);
              //_savecartList(__cart_items_list);

            } else {
              _viewOrder_list.removeAt(index);
              getCartItem_YourModel.removeAt(index);
              //__cart_items_list.removeAt(index);

            }
            _savecartList(_viewOrder_list);

            _cart_youritemscalculation();
          });
        },
      ),
    );
  }

  @override
  void initState() {
    super.initState();

    print("CARTDINEIN====" + widget.dine_in_id + "======" + widget.dine_in_name + "=======" + widget.dine_in_behaviour);

    Future.delayed(Duration(seconds: 2), () async {
      setState(() {
        UserRepository().getuserdetails().then((getuserdetails) {
          setState(() {
            user_id = getuserdetails[0];
            print("USERID====" + user_id + "====RESTID===" + widget.restaurant_id + "===OREDERID====" + widget.order_id);
            CartsRepository().getcartslisting().then((cartList) {
              setState(() {
                _loading = false;
                __cart_items_list = cartList;
                cart_count = __cart_items_list.length;
                print("CARTSESSIONLENGTH" + cart_count.toString() + "" + __cart_items_list.toString());
                _cart_itemscalculation();
                _loading = false;

                if (selected_tip > 0) {
                  _payment_grandroundup = selected_tip + double.parse(_items_cart_itemtotal.toStringAsFixed(2));
                } else {
                  _payment_grandroundup = double.parse(_items_cart_itemtotal.toStringAsFixed(2));
                }
                print("SELECTEDTIP" + selected_tip.toString() + "======" + _payment_grandroundup.toStringAsFixed(2));

                AddressListRepository().getAddressLists(user_id).then((result_addresslist) {
                  setState(() {
                    getAddressLists = result_addresslist;
                    print("ORDERSLENGTH=====" + getAddressLists.length.toString());

                    RestaurantOnlineOrderingRulesRepository().getRestaurantOnlineOrderingRules(widget.restaurant_id).then((value) {
                      print("SEARCHRESTAURANT RESPSTATUS  " + value.responseStatus.toString());
                      debugPrint(base_url +
                          "------" +
                          value.responseStatus.toString() +
                          "-------" +
                          value.result.toString() +
                          "-----" +
                          value.rulesData.toString());
                      if (value.responseStatus == 1) {
                        setState(() {
                          _loading = false;

                          for (int t = 0; t < value.rulesData.approvalRules.length; t++) {
                            _approved_list.add(value.rulesData.approvalRules[t]);
                          }

                          restaurant_rules_OrderAcceptanceStatus = value.rulesData.ordersAcceptanceStatus;
                          restaurant_rules_DeliveryStatus = value.rulesData.deliveryStatus;
                          restaurant_rules_Enabled_Status = value.rulesData.rulesEnabled;
                          restaurant_rules_TakeOutStatus = value.rulesData.takeOutStatus;

                          print("ORDERACCEPTANCE====" + restaurant_rules_OrderAcceptanceStatus.toString());
                        });
                      } else if (value.responseStatus == 0) {
                        setState(() {
                          _loading = false;
                          Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                        });
                      } else {
                        setState(() {
                          _loading = false;
                          Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                        });
                      }
                    });

                    if (widget.order_id != "" /* || widget.order_id != null*/) {
                      print("ORDERID====" + widget.order_id + "==== USERID =====" + user_id);
                      OrderDetailRepository().getOrderDetails(widget.order_id, user_id).then((value) {
                        debugPrint(base_url + "------" + value.responseStatus.toString() + "-------" + value.result.toString() + "-----");
                        if (value.responseStatus == 1) {
                          setState(() {
                            _loading = false;

                            del_dineinid = value.viewOrderDetails.dineInOption;
                            del_dinename = value.viewOrderDetails.dineInOptionName;
                            del_dineinbehaviour = value.viewOrderDetails.dineInBehaviour;

                            for (int t = 0; t < value.viewOrderDetails.orderItems.length; t++) {
                              _viewOrder_list.add(value.viewOrderDetails.orderItems[t]);
                            }
                            _cart_youritemscalculation();
                          });
                        } else if (value.responseStatus == 0) {
                          setState(() {
                            _loading = false;
                            Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                          });
                        } else {
                          setState(() {
                            _loading = false;
                            Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                          });
                        }
                      });
                    }
                  });
                });
              });
            });
          });
        });
      });
    });

    payment_type = "Cash";
    if (widget.payment_type == "Credit") {
      payment_type = "Credit";
    } else if (widget.payment_type == "Debit") {
      payment_type = "Debit";
    } else if (widget.payment_type == "Banking") {
      payment_type = "NetBanking";
    } else if (widget.payment_type == "Cash") {
      payment_type = "Cash";
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    //  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: login_passcode_bg1));
    print("Height" + SizeConfig.screenHeight.toString());
    final space = SizedBox(height: 20);
    if (SizeConfig.screenHeight >= 600 && SizeConfig.screenHeight < 800) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 550 && SizeConfig.screenHeight < 600) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 800) {
      screenheight = 550;
    }
    final screen_height = MediaQuery.of(context).size.height;
    print(screen_height);

    return WillPopScope(
        onWillPop: () async => onBackPressed(context),
        child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: Colors.white,
        appBar: AppBar(
          toolbarHeight: 56,
          //toolbarHeight:  height-30.00,
          automaticallyImplyLeading: false,
          elevation: 0.0,
          backgroundColor: Colors.white,
          centerTitle: true,
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                padding: EdgeInsets.only(left: 12.0),
                icon: Image.asset("images/back_arrow.png", width: 20, height: 20),
                onPressed: () {
                  onBackPressed(context);
                },
              );
            },
          ),
          actions: [

          ],
        ),
        body: _loading
            ? Center(
                child: SpinKitCircle(color: Colors.lightBlueAccent),
              )
            : SingleChildScrollView(
                child: _viewOrder_list.length > 0
                    ? Container(
                        margin: EdgeInsets.fromLTRB(15, 0, 0, 0),
                        //height: SizeConfig.safeBlockHorizontal * 155,
                        child: Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisSize: MainAxisSize.min, children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Padding(
                                  padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                  child: Text(
                                    "Delivery At ",
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: login_passcode_text,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w400,
                                    ),
                                  )),
                              Column(
                                //crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    padding: EdgeInsets.all(0.0),
                                    width: 250,
                                    child: Row(
                                      children: <Widget>[
                                        Flexible(
                                          child: RichText(
                                            overflow: TextOverflow.ellipsis,
                                            text: TextSpan(
                                                style: TextStyle(
                                                  fontSize: 18,
                                                  color: login_passcode_text,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w600,
                                                ),
                                                text: widget.delivery_location),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    gewinner();

                                    /*showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      title: Container(child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text('Address List',style: TextStyle(color: login_passcode_text),),
                                      ),),
                                      content: setupAlertDialoadContainer(context),
                                    );
                                  }
                              );*/
                                  });
                                },
                                child: Container(
                                    margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                    child: Image.asset(
                                      'images/' + downarrow,
                                      height: 15,
                                      width: 15,
                                    )),
                              )
                            ],
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(10, 10, 0, 0),
                            child: DotWidget(
                              dashColor: location_border,
                              dashHeight: 1,
                              dashWidth: 10,
                              emptyWidth: 2,
                              totalWidth: 380,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                            child: Row(
                              children: [
                                Padding(
                                  padding: EdgeInsets.fromLTRB(15, 0, 0, 5),
                                  child: Image.asset(
                                    'images/timer.png',
                                    height: 15,
                                    width: 15,
                                  ),
                                ),
                                Padding(
                                    padding: EdgeInsets.fromLTRB(10, 0, 0, 5),
                                    child: Text(
                                      "Delivery in 26 min with live tracking",
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontSize: 14,
                                        color: login_passcode_text,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.w400,
                                      ),
                                    )),
                              ],
                            ),
                          ),
                          ListView.builder(
                              physics: ClampingScrollPhysics(),
                              shrinkWrap: true,
                              primary: false,
                              scrollDirection: Axis.vertical,
                              itemCount: _viewOrder_list.length,
                              itemBuilder: (context, index) {
                                return Container(
                                  margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(
                                        height: 120,
                                        width: 120,
                                        decoration: BoxDecoration(
                                          color: login_passcode_text,
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(5),
                                              bottomLeft: Radius.circular(5),
                                              topRight: Radius.circular(5),
                                              bottomRight: Radius.circular(5)),
                                        ),
                                        child: Padding(
                                          padding: EdgeInsets.fromLTRB(1, 1, 1, 1),
                                          child: Image.asset(
                                            "images/food_restaurant.png",
                                            height: 120,
                                            width: 120,
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          SizedBox(
                                            width: 200.0,
                                            child: Padding(
                                                padding: EdgeInsets.fromLTRB(10, 0, 0, 5),
                                                child: Text(
                                                  _viewOrder_list[index].itemName,
                                                  maxLines: 2,
                                                  overflow: TextOverflow.ellipsis,
                                                  softWrap: false,
                                                  style: TextStyle(
                                                    fontSize: 18,
                                                    color: login_passcode_text,
                                                    fontFamily: 'Poppins',
                                                    fontWeight: FontWeight.w600,
                                                  ),
                                                )),
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            children: [
                                              Padding(
                                                padding: EdgeInsets.fromLTRB(10, 0, 5, 5),
                                                child: SmoothStarRating(
                                                  isReadOnly: true,
                                                  color: Colors.amber,
                                                  borderColor: Colors.grey,
                                                  rating: 3.5,
                                                  size: 25,
                                                  starCount: 5,
                                                ),
                                              ),
                                              Padding(
                                                  padding: EdgeInsets.fromLTRB(0, 0, 0, 5),
                                                  child: Text(
                                                    "250 votes",
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                      fontSize: 14,
                                                      color: login_passcode_text,
                                                      fontFamily: 'Poppins',
                                                      fontWeight: FontWeight.w600,
                                                    ),
                                                  )),
                                            ],
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Padding(
                                                  padding: EdgeInsets.fromLTRB(10, 0, 0, 5),
                                                  child: Text(
                                                    "250 votes",
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                      fontSize: 14,
                                                      color: login_passcode_text,
                                                      fontFamily: 'Poppins',
                                                      fontWeight: FontWeight.w600,
                                                    ),
                                                  )),
                                              Container(
                                                margin: EdgeInsets.only(left: 35, top: 0),
                                                decoration: BoxDecoration(
                                                  color: dashboard_bg,
                                                  borderRadius: BorderRadius.only(
                                                      topLeft: Radius.circular(30),
                                                      bottomLeft: Radius.circular(30),
                                                      topRight: Radius.circular(0),
                                                      bottomRight: Radius.circular(0)),
                                                ),
                                                alignment: Alignment.center,
                                                width: 60,
                                                height: 30,
                                                child: Text(
                                                  new String.fromCharCodes(new Runes('\u0024')) + _viewOrder_list[index].unitPrice,
                                                  textAlign: TextAlign.left,
                                                  style: TextStyle(
                                                    fontSize: 14,
                                                    color: login_passcode_text,
                                                    fontFamily: 'Poppins',
                                                    fontWeight: FontWeight.w600,
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                  margin: EdgeInsets.only(left: 5, top: 0, right: 0),
                                                  decoration: BoxDecoration(
                                                    color: Colors.lightBlueAccent,
                                                    borderRadius: BorderRadius.only(
                                                        topLeft: Radius.circular(15),
                                                        bottomLeft: Radius.circular(15),
                                                        topRight: Radius.circular(15),
                                                        bottomRight: Radius.circular(15)),
                                                  ),
                                                  alignment: Alignment.centerRight,
                                                  height: 30,
                                                  child: Row(
                                                    children: [
                                                      _decrementYourorderButton(index),
                                                      Container(
                                                          margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                          color: Colors.lightBlueAccent,
                                                          width: 20,
                                                          child: Padding(
                                                            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                            child: Text(
                                                              '${_viewOrder_list[index].quantity}',
                                                              style: TextStyle(
                                                                  color: login_passcode_text,
                                                                  fontSize: 14,
                                                                  fontFamily: 'Poppins',
                                                                  fontWeight: FontWeight.w400),
                                                              textAlign: TextAlign.center,
                                                            ),
                                                          )),
                                                      _increment_YourOrderButton(index)
                                                    ],
                                                  ))
                                            ],
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                );
                              }),
                          Container(
                              height: 40,
                              margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(color: login_passcode_text, borderRadius: BorderRadius.circular(2)),
                              child: InkWell(
                                  child: FlatButton(
                                      child: Text("ADD Cooking Instructions ( Optional )",
                                          style: TextStyle(fontSize: 14, fontFamily: 'Poppins', fontWeight: FontWeight.w800, color: Colors.white)),
                                      onPressed: () {}))),
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 5, 5, 0),
                            alignment: Alignment.center,
                            child: Card(
                              elevation: 5,
                              color: order_appbar_bg,
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(color: order_appbar_bg),
                                  borderRadius: BorderRadius.horizontal(left: Radius.circular(0.00), right: Radius.zero),
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                        padding: EdgeInsets.fromLTRB(10, 10, 0, 0),
                                        child: Text(
                                          "SUPPORT YOUR RIDER",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                            fontSize: 18,
                                            color: login_passcode_text,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w800,
                                          ),
                                        )),
                                    SizedBox(height: 5),
                                    Padding(
                                        padding: EdgeInsets.fromLTRB(10, 2, 0, 0),
                                        child: Text(
                                          "Our riders are risking their lives to serve the nation. While we're doing our best to support them. We'd request you to tip them generously in these difficult times, if you can afford it.",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                            fontSize: 14,
                                            color: login_passcode_text,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w400,
                                          ),
                                        )),
                                    Container(
                                      margin: EdgeInsets.only(left: 0, right: 15),
                                      padding: EdgeInsets.symmetric(horizontal: 0.0, vertical: 5.0),
                                      height: 70,
                                      child: ListView.builder(
                                          scrollDirection: Axis.horizontal,
                                          itemCount: numbers.length,
                                          itemBuilder: (context, index) {
                                            print("DEFAULTSELECTEDTIP" + selected_tip.toString());
                                            return Container(
                                              margin: EdgeInsets.all(5),
                                              width: MediaQuery.of(context).size.width * 0.2,
                                              child: InkWell(
                                                  onTap: () {
                                                    setState(() {
                                                      selected_pos = index;
                                                      //menu_id = _get_all_menus[index].id;
                                                      print(numbers[index]);
                                                      selected_tip = double.parse(numbers[index]);

                                                      if (selected_tip > 0) {
                                                        _payment_grandroundup =
                                                            selected_tip + double.parse(_items_cart_grandtotal.toStringAsFixed(2));
                                                      } else {
                                                        _payment_grandroundup = double.parse(_items_cart_grandtotal.toStringAsFixed(2));
                                                      }

                                                      print("SELECTEDTIP" +
                                                          selected_tip.toString() +
                                                          "======" +
                                                          _payment_grandroundup.toStringAsFixed(2));
                                                    });
                                                  },
                                                  child: Card(
                                                    elevation: 2,
                                                    color: selected_pos == index ? Colors.lightBlueAccent : Colors.white,
                                                    shape: RoundedRectangleBorder(
                                                      borderRadius: BorderRadius.circular(5),
                                                    ),
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                        border: Border.all(
                                                          color: login_passcode_bg2,
                                                        ),
                                                        borderRadius: BorderRadius.all(Radius.circular(5)),
                                                      ),
                                                      child: Column(
                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                        children: <Widget>[
                                                          Text(
                                                            new String.fromCharCodes(new Runes('\u0024')) + numbers[index],
                                                            style: TextStyle(color: add_food_item_bg, fontSize: 16),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  )),
                                            );
                                          }),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Container(
                              height: 60,
                              margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(color: login_passcode_text, borderRadius: BorderRadius.circular(2)),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                          padding: EdgeInsets.fromLTRB(10, 10, 0, 0),
                                          child: Text(
                                            "OFFERS",
                                            textAlign: TextAlign.left,
                                            style: TextStyle(
                                              fontSize: 14,
                                              color: Colors.white,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                            ),
                                          )),
                                      Padding(
                                          padding: EdgeInsets.fromLTRB(10, 5, 0, 0),
                                          child: Text(
                                            "Select a promo code",
                                            textAlign: TextAlign.left,
                                            style: TextStyle(
                                              fontSize: 14,
                                              color: Colors.white,
                                              fontFamily: 'Poppins',
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ))
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      InkWell(
                                          child: FlatButton(
                                              child: Text("View Offers",
                                                  style: TextStyle(
                                                      fontSize: 14, fontFamily: 'Poppins', fontWeight: FontWeight.w400, color: Colors.white)),
                                              onPressed: () {})),
                                      Image.asset(
                                        "images/back_arrow.png",
                                        color: Colors.white,
                                        height: 20,
                                        width: 20,
                                        fit: BoxFit.cover,
                                      )
                                    ],
                                  )
                                ],
                              )),
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 5, 5, 0),
                            alignment: Alignment.center,
                            child: Card(
                              elevation: 5,
                              color: order_appbar_bg,
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(color: order_appbar_bg),
                                  borderRadius: BorderRadius.horizontal(left: Radius.circular(0.00), right: Radius.zero),
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.fromLTRB(0, 10, 10, 0),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Padding(
                                              padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                              child: Text(
                                                "Item total",
                                                textAlign: TextAlign.left,
                                                style: TextStyle(
                                                  fontSize: 14,
                                                  color: login_passcode_text,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w600,
                                                ),
                                              )),
                                          Padding(
                                              padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                              child: Text(
                                                new String.fromCharCodes(new Runes('\u0024')) + _items_cart_subtotal.toStringAsFixed(2),
                                                textAlign: TextAlign.left,
                                                style: TextStyle(
                                                  fontSize: 14,
                                                  color: login_passcode_text,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w600,
                                                ),
                                              )),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.fromLTRB(0, 10, 10, 0),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Padding(
                                              padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                              child: Text(
                                                "Delivery charge",
                                                textAlign: TextAlign.left,
                                                style: TextStyle(
                                                  fontSize: 14,
                                                  color: login_passcode_text,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w600,
                                                ),
                                              )),
                                          Padding(
                                              padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                              child: Text(
                                                new String.fromCharCodes(new Runes('\u0024')) + "0.00",
                                                textAlign: TextAlign.left,
                                                style: TextStyle(
                                                  fontSize: 14,
                                                  color: login_passcode_text,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w600,
                                                ),
                                              )),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.fromLTRB(0, 10, 10, 0),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Padding(
                                              padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                              child: Text(
                                                "Taxes",
                                                textAlign: TextAlign.left,
                                                style: TextStyle(
                                                  fontSize: 14,
                                                  color: login_passcode_text,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w600,
                                                ),
                                              )),
                                          Padding(
                                              padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                              child: Text(
                                                new String.fromCharCodes(new Runes('\u0024')) + _items_cart_taxtotal.toStringAsFixed(2),
                                                textAlign: TextAlign.left,
                                                style: TextStyle(
                                                  fontSize: 14,
                                                  color: login_passcode_text,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w600,
                                                ),
                                              )),
                                        ],
                                      ),
                                    ),
                                    selected_tip > 0
                                        ? Container(
                                            margin: EdgeInsets.fromLTRB(0, 10, 10, 0),
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: [
                                                Padding(
                                                    padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                    child: Text(
                                                      "Tip Amount",
                                                      textAlign: TextAlign.left,
                                                      style: TextStyle(
                                                        fontSize: 14,
                                                        color: login_passcode_text,
                                                        fontFamily: 'Poppins',
                                                        fontWeight: FontWeight.w600,
                                                      ),
                                                    )),
                                                Padding(
                                                    padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                    child: Text(
                                                      new String.fromCharCodes(new Runes('\u0024')) + selected_tip.toStringAsFixed(2),
                                                      textAlign: TextAlign.left,
                                                      style: TextStyle(
                                                        fontSize: 14,
                                                        color: login_passcode_text,
                                                        fontFamily: 'Poppins',
                                                        fontWeight: FontWeight.w600,
                                                      ),
                                                    )),
                                              ],
                                            ),
                                          )
                                        : SizedBox(),
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(10, 10, 0, 0),
                                      child: DotWidget(
                                        dashColor: location_border,
                                        dashHeight: 1,
                                        dashWidth: 10,
                                        emptyWidth: 2,
                                        totalWidth: 360,
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.fromLTRB(0, 20, 10, 10),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Padding(
                                              padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                              child: Text(
                                                "Grand Total",
                                                textAlign: TextAlign.left,
                                                style: TextStyle(
                                                  fontSize: 16,
                                                  color: login_passcode_text,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w800,
                                                ),
                                              )),
                                          Padding(
                                              padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                              child: Text(
                                                new String.fromCharCodes(new Runes('\u0024')) + _payment_grandroundup.toStringAsFixed(2),
                                                textAlign: TextAlign.left,
                                                style: TextStyle(
                                                  fontSize: 16,
                                                  color: login_passcode_text,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w800,
                                                ),
                                              )),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Container(
                              alignment: Alignment.center,
                              color: order_appbar_bg,
                              child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                                Container(
                                  decoration: BoxDecoration(
                                    color: login_passcode_text,
                                    border: Border.all(color: order_appbar_bg),
                                    borderRadius: BorderRadius.horizontal(left: Radius.circular(5.00), right: Radius.circular(5.00)),
                                  ),
                                  height: 60,
                                  margin: EdgeInsets.fromLTRB(10, 10, 0, 10),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                        Padding(
                                            padding: EdgeInsets.fromLTRB(10, 10, 20, 0),
                                            child: Text(
                                              "Pay Using",
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontSize: 14,
                                                color: order_appbar_bg,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w400,
                                              ),
                                            )),
                                        Padding(
                                            padding: EdgeInsets.fromLTRB(10, 5, 20, 5),
                                            child: Text(
                                              payment_type,
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.white,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w400,
                                              ),
                                            ))
                                      ]),
                                      Container(
                                        width: 30,
                                        height: 70,
                                        decoration: BoxDecoration(
                                          color: order_appbar_bg,
                                          border: Border.all(color: order_appbar_bg),
                                          borderRadius: BorderRadius.horizontal(left: Radius.circular(5.00), right: Radius.circular(5.00)),
                                        ),
                                        margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                        child: InkWell(
                                            child: FlatButton(
                                                child: Padding(
                                                  padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                                  child: Image.asset(
                                                    "images/back_arrow.png",
                                                    height: 60,
                                                    width: 60,
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                                onPressed: () {
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) => PaymentTypesScreen(
                                                              widget.restaurant_id,
                                                              widget.delivery_location,
                                                              widget.delivery_lattitude,
                                                              widget.delivery_longitude,
                                                              widget.dine_in_id,
                                                              widget.dine_in_name,
                                                              widget.dine_in_behaviour)));
                                                })),
                                      )
                                    ],
                                  ),
                                ),
                                restaurant_rules_OrderAcceptanceStatus == true
                                    ? Container(
                                        height: 60,
                                        margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(color: login_passcode_text, borderRadius: BorderRadius.circular(5)),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Padding(
                                                    padding: EdgeInsets.fromLTRB(10, 10, 0, 0),
                                                    child: Text(
                                                      "Total",
                                                      textAlign: TextAlign.left,
                                                      style: TextStyle(
                                                        fontSize: 14,
                                                        color: Colors.white,
                                                        fontFamily: 'Poppins',
                                                        fontWeight: FontWeight.w400,
                                                      ),
                                                    )),
                                                Padding(
                                                    padding: EdgeInsets.fromLTRB(10, 5, 0, 0),
                                                    child: Text(
                                                      new String.fromCharCodes(new Runes('\u0024')) + _payment_grandroundup.toStringAsFixed(2),
                                                      textAlign: TextAlign.left,
                                                      style: TextStyle(
                                                        fontSize: 14,
                                                        color: Colors.white,
                                                        fontFamily: 'Poppins',
                                                        fontWeight: FontWeight.w800,
                                                      ),
                                                    ))
                                              ],
                                            ),
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: [
                                                InkWell(
                                                    child: FlatButton(
                                                        child: Text("Place Order",
                                                            style: TextStyle(
                                                                fontSize: 14,
                                                                fontFamily: 'Poppins',
                                                                fontWeight: FontWeight.w400,
                                                                color: Colors.white)),
                                                        onPressed: () {
                                                          if (_viewOrder_list.length > 0) {
                                                            DeliveryInfo useraddress;
                                                            CustomerDetails customedetails;
                                                            if (del_dinename == "Delivery") {
                                                              useraddress = DeliveryInfo("test", "0", widget.delivery_lattitude,
                                                                  widget.delivery_longitude, "home", widget.delivery_location);
                                                              customedetails = CustomerDetails(user_id);
                                                            } else {
                                                              useraddress = DeliveryInfo("", "", "", "", "", "");
                                                              customedetails = CustomerDetails(user_id);
                                                            }

                                                            /* DeliveryInfo useraddress = DeliveryInfo("test","0",widget.delivery_lattitude,widget.delivery_longitude,"home",widget.delivery_location);
                                            CustomerDetails customedetails = CustomerDetails(user_id);*/

                                                            AddUserOrderRepository()
                                                                .adduserorder(
                                                                    _viewOrder_list,
                                                                    selected_tip.toString(),
                                                                    0.0,
                                                                    double.parse(_items_cart_grandtotal.toStringAsFixed(2)),
                                                                    double.parse(_items_cart_taxtotal.toStringAsFixed(2)),
                                                                    double.parse(_payment_grandroundup.toStringAsFixed(2)),
                                                                    widget.restaurant_id,
                                                                    0,
                                                                    customedetails,
                                                                    useraddress,
                                                                    del_dineinid,
                                                                    del_dinename,
                                                                    del_dineinbehaviour,
                                                                    1,
                                                                    false,
                                                                    false,
                                                                    _payment_grandroundup.toStringAsFixed(2),
                                                                    "",
                                                                    1,
                                                                    1)
                                                                .then((result) {
                                                              print("CARTSAVING" + result.toString());
                                                              if (result.responseStatus == 0) {
                                                                _loading = false;
                                                                Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
                                                                Toast.show("No Data Found", context,
                                                                    duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                                                              } else if (result.responseStatus == 1) {
                                                                setState(() {
                                                                  print("ADDWAITERORDERID" + result.orderId);
                                                                  _loading = false;
                                                                  CartsRepository.removesaved_in_cart();
                                                                  getCartItemModelapi.clear();
                                                                  Navigator.pushAndRemoveUntil(
                                                                      context,
                                                                      MaterialPageRoute(builder: (context) => SuccessScreen()),
                                                                      (Route<dynamic> route) => false);
                                                                });
                                                              } else {
                                                                Toast.show(result.result, context,
                                                                    duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                                                              }
                                                            });
                                                          } else {
                                                            print("cart empty");
                                                          }
                                                        })),
                                                Image.asset(
                                                  "images/back_arrow.png",
                                                  color: Colors.white,
                                                  height: 20,
                                                  width: 20,
                                                  fit: BoxFit.cover,
                                                )
                                              ],
                                            )
                                          ],
                                        ))
                                    : Container(
                                        height: 60,
                                        margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(color: edit_text_border_color, borderRadius: BorderRadius.circular(5)),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Padding(
                                                    padding: EdgeInsets.fromLTRB(10, 10, 0, 0),
                                                    child: Text(
                                                      "Total",
                                                      textAlign: TextAlign.left,
                                                      style: TextStyle(
                                                        fontSize: 14,
                                                        color: Colors.white,
                                                        fontFamily: 'Poppins',
                                                        fontWeight: FontWeight.w400,
                                                      ),
                                                    )),
                                                Padding(
                                                    padding: EdgeInsets.fromLTRB(10, 5, 0, 0),
                                                    child: Text(
                                                      new String.fromCharCodes(new Runes('\u0024')) + _payment_grandroundup.toStringAsFixed(2),
                                                      textAlign: TextAlign.left,
                                                      style: TextStyle(
                                                        fontSize: 14,
                                                        color: Colors.white,
                                                        fontFamily: 'Poppins',
                                                        fontWeight: FontWeight.w800,
                                                      ),
                                                    ))
                                              ],
                                            ),
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: [
                                                InkWell(
                                                    child: FlatButton(
                                                        disabledTextColor: edit_text_border_color,
                                                        disabledColor: login_form_hint,
                                                        child: Text("Place Order",
                                                            style: TextStyle(
                                                                fontSize: 14,
                                                                fontFamily: 'Poppins',
                                                                fontWeight: FontWeight.w400,
                                                                color: Colors.white)),
                                                        onPressed: () {
                                                          Toast.show("Restaurant Not Accepting Orders", context,
                                                              duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                                                        })),
                                                Image.asset(
                                                  "images/back_arrow.png",
                                                  color: Colors.white,
                                                  height: 20,
                                                  width: 20,
                                                  fit: BoxFit.cover,
                                                )
                                              ],
                                            )
                                          ],
                                        )),
                              ]))
                        ]),
                      )
                    : __cart_items_list.length > 0
                        ? Container(
                            margin: EdgeInsets.fromLTRB(15, 0, 0, 0),
                            //height: SizeConfig.safeBlockHorizontal * 155,
                            child: Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisSize: MainAxisSize.min, children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Padding(
                                      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                      child: Text(
                                        "Delivery At ",
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          fontSize: 14,
                                          color: login_passcode_text,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w400,
                                        ),
                                      )),
                                  Column(
                                    //crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        padding: EdgeInsets.all(0.0),
                                        width: 250,
                                        child: Row(
                                          children: <Widget>[
                                            Flexible(
                                              child: RichText(
                                                overflow: TextOverflow.ellipsis,
                                                text: TextSpan(
                                                    style: TextStyle(
                                                      fontSize: 18,
                                                      color: login_passcode_text,
                                                      fontFamily: 'Poppins',
                                                      fontWeight: FontWeight.w600,
                                                    ),
                                                    text: widget.delivery_location),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  InkWell(
                                    onTap: () {
                                      setState(() {
                                        gewinner();

                                        /*showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      title: Container(child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text('Address List',style: TextStyle(color: login_passcode_text),),
                                      ),),
                                      content: setupAlertDialoadContainer(context),
                                    );
                                  }
                              );*/
                                      });
                                    },
                                    child: Container(
                                        margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                        child: Image.asset(
                                          'images/' + downarrow,
                                          height: 15,
                                          width: 15,
                                        )),
                                  )
                                ],
                              ),
                              Padding(
                                padding: EdgeInsets.fromLTRB(10, 10, 0, 0),
                                child: DotWidget(
                                  dashColor: location_border,
                                  dashHeight: 1,
                                  dashWidth: 10,
                                  emptyWidth: 2,
                                  totalWidth: 380,
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                child: Row(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(15, 0, 0, 5),
                                      child: Image.asset(
                                        'images/timer.png',
                                        height: 15,
                                        width: 15,
                                      ),
                                    ),
                                    Padding(
                                        padding: EdgeInsets.fromLTRB(10, 0, 0, 5),
                                        child: Text(
                                          "Delivery in 26 min with live tracking",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                            fontSize: 14,
                                            color: login_passcode_text,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w400,
                                          ),
                                        )),
                                  ],
                                ),
                              ),
                              ListView.builder(
                                  physics: ClampingScrollPhysics(),
                                  shrinkWrap: true,
                                  primary: false,
                                  scrollDirection: Axis.vertical,
                                  itemCount: __cart_items_list.length,
                                  itemBuilder: (context, index) {
                                    return Container(
                                      margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          Container(
                                            height: 120,
                                            width: 120,
                                            decoration: BoxDecoration(
                                              color: login_passcode_text,
                                              borderRadius: BorderRadius.only(
                                                  topLeft: Radius.circular(5),
                                                  bottomLeft: Radius.circular(5),
                                                  topRight: Radius.circular(5),
                                                  bottomRight: Radius.circular(5)),
                                            ),
                                            child: Padding(
                                              padding: EdgeInsets.fromLTRB(1, 1, 1, 1),
                                              child: Image.asset(
                                                "images/food_restaurant.png",
                                                height: 120,
                                                width: 120,
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ),
                                          Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              SizedBox(
                                                width: 200.0,
                                                child: Padding(
                                                    padding: EdgeInsets.fromLTRB(10, 0, 0, 5),
                                                    child: Text(
                                                      __cart_items_list[index].itemName,
                                                      maxLines: 2,
                                                      overflow: TextOverflow.ellipsis,
                                                      softWrap: false,
                                                      style: TextStyle(
                                                        fontSize: 18,
                                                        color: login_passcode_text,
                                                        fontFamily: 'Poppins',
                                                        fontWeight: FontWeight.w600,
                                                      ),
                                                    )),
                                              ),
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                children: [
                                                  Padding(
                                                    padding: EdgeInsets.fromLTRB(10, 0, 5, 5),
                                                    child: SmoothStarRating(
                                                      isReadOnly: true,
                                                      color: Colors.amber,
                                                      borderColor: Colors.grey,
                                                      rating: 3.5,
                                                      size: 25,
                                                      starCount: 5,
                                                    ),
                                                  ),
                                                  Padding(
                                                      padding: EdgeInsets.fromLTRB(0, 0, 0, 5),
                                                      child: Text(
                                                        "250 votes",
                                                        textAlign: TextAlign.left,
                                                        style: TextStyle(
                                                          fontSize: 14,
                                                          color: login_passcode_text,
                                                          fontFamily: 'Poppins',
                                                          fontWeight: FontWeight.w600,
                                                        ),
                                                      )),
                                                ],
                                              ),
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Padding(
                                                      padding: EdgeInsets.fromLTRB(10, 0, 0, 5),
                                                      child: Text(
                                                        "250 votes",
                                                        textAlign: TextAlign.left,
                                                        style: TextStyle(
                                                          fontSize: 14,
                                                          color: login_passcode_text,
                                                          fontFamily: 'Poppins',
                                                          fontWeight: FontWeight.w600,
                                                        ),
                                                      )),
                                                  Container(
                                                    margin: EdgeInsets.only(left: 35, top: 0),
                                                    decoration: BoxDecoration(
                                                      color: dashboard_bg,
                                                      borderRadius: BorderRadius.only(
                                                          topLeft: Radius.circular(30),
                                                          bottomLeft: Radius.circular(30),
                                                          topRight: Radius.circular(0),
                                                          bottomRight: Radius.circular(0)),
                                                    ),
                                                    alignment: Alignment.center,
                                                    width: 60,
                                                    height: 30,
                                                    child: Text(
                                                      new String.fromCharCodes(new Runes('\u0024')) + __cart_items_list[index].unitPrice,
                                                      textAlign: TextAlign.left,
                                                      style: TextStyle(
                                                        fontSize: 14,
                                                        color: login_passcode_text,
                                                        fontFamily: 'Poppins',
                                                        fontWeight: FontWeight.w600,
                                                      ),
                                                    ),
                                                  ),
                                                  Container(
                                                      margin: EdgeInsets.only(left: 5, top: 0, right: 0),
                                                      decoration: BoxDecoration(
                                                        color: Colors.lightBlueAccent,
                                                        borderRadius: BorderRadius.only(
                                                            topLeft: Radius.circular(15),
                                                            bottomLeft: Radius.circular(15),
                                                            topRight: Radius.circular(15),
                                                            bottomRight: Radius.circular(15)),
                                                      ),
                                                      alignment: Alignment.centerRight,
                                                      height: 30,
                                                      child: Row(
                                                        children: [
                                                          _decrementButton(index),
                                                          Container(
                                                              margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                              color: Colors.lightBlueAccent,
                                                              width: 20,
                                                              child: Padding(
                                                                padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                                child: Text(
                                                                  '${__cart_items_list[index].quantity}',
                                                                  style: TextStyle(
                                                                      color: login_passcode_text,
                                                                      fontSize: 14,
                                                                      fontFamily: 'Poppins',
                                                                      fontWeight: FontWeight.w400),
                                                                  textAlign: TextAlign.center,
                                                                ),
                                                              )),
                                                          _incrementButton(index)
                                                        ],
                                                      ))
                                                ],
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    );
                                  }),
                              Container(
                                  height: 40,
                                  margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(color: login_passcode_text, borderRadius: BorderRadius.circular(2)),
                                  child: InkWell(
                                      child: FlatButton(
                                          child: Text("ADD Cooking Instructions ( Optional )",
                                              style:
                                                  TextStyle(fontSize: 14, fontFamily: 'Poppins', fontWeight: FontWeight.w800, color: Colors.white)),
                                          onPressed: () {}))),
                              Container(
                                margin: EdgeInsets.fromLTRB(5, 5, 5, 0),
                                alignment: Alignment.center,
                                child: Card(
                                  elevation: 5,
                                  color: order_appbar_bg,
                                  child: Container(
                                    decoration: BoxDecoration(
                                      border: Border.all(color: order_appbar_bg),
                                      borderRadius: BorderRadius.horizontal(left: Radius.circular(0.00), right: Radius.zero),
                                    ),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Padding(
                                            padding: EdgeInsets.fromLTRB(10, 10, 0, 0),
                                            child: Text(
                                              "SUPPORT YOUR RIDER",
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontSize: 18,
                                                color: login_passcode_text,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w800,
                                              ),
                                            )),
                                        SizedBox(height: 5),
                                        Padding(
                                            padding: EdgeInsets.fromLTRB(10, 2, 0, 0),
                                            child: Text(
                                              "Our riders are risking their lives to serve the nation. While we're doing our best to support them. We'd request you to tip them generously in these difficult times, if you can afford it.",
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontSize: 14,
                                                color: login_passcode_text,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w400,
                                              ),
                                            )),
                                        /*Container(
                          margin: EdgeInsets.only(left: 5, top: 10, right: 0, bottom: 10),
                          child: Row(
                            children: [
                              Container(
                                  margin: EdgeInsets.only(left: 5, top: 0, right: 0),
                                  decoration: BoxDecoration(
                                    color: Colors.lightBlueAccent,
                                    borderRadius: BorderRadius.only(topLeft: Radius.circular(5), bottomLeft: Radius.circular(5), topRight: Radius.circular(5), bottomRight: Radius.circular(5)),
                                  ),
                                  alignment: Alignment.center,
                                  height: 30,
                                  child: Row(
                                    children: [
                                      Container(
                                          margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                          color: Colors.lightBlueAccent,
                                          child: Padding(
                                            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                            child: Text(
                                              "+ &2.00.",
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontSize: 14,
                                                color: login_passcode_text,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w400,
                                              ),
                                            ),
                                          )),
                                    ],
                                  )),
                              Container(
                                  margin: EdgeInsets.only(left: 10, top: 0, right: 0),
                                  decoration: BoxDecoration(
                                    color: Colors.lightBlueAccent,
                                    borderRadius: BorderRadius.only(topLeft: Radius.circular(5), bottomLeft: Radius.circular(5), topRight: Radius.circular(5), bottomRight: Radius.circular(5)),
                                  ),
                                  alignment: Alignment.center,
                                  height: 30,
                                  child: Row(
                                    children: [
                                      Container(
                                          margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                          color: Colors.lightBlueAccent,
                                          child: Padding(
                                            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                            child: Text(
                                              "+ &2.00.",
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontSize: 14,
                                                color: login_passcode_text,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w400,
                                              ),
                                            ),
                                          )),
                                    ],
                                  )),
                              Container(
                                  margin: EdgeInsets.only(left: 10, top: 0, right: 0),
                                  decoration: BoxDecoration(
                                    color: Colors.lightBlueAccent,
                                    borderRadius: BorderRadius.only(topLeft: Radius.circular(5), bottomLeft: Radius.circular(5), topRight: Radius.circular(5), bottomRight: Radius.circular(5)),
                                  ),
                                  alignment: Alignment.center,
                                  height: 30,
                                  child: Row(
                                    children: [
                                      Container(
                                          margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                          color: Colors.lightBlueAccent,
                                          child: Padding(
                                            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                            child: Text(
                                              "+ &2.00.",
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                fontSize: 14,
                                                color: login_passcode_text,
                                                fontFamily: 'Poppins',
                                                fontWeight: FontWeight.w400,
                                              ),
                                            ),
                                          )),
                                    ],
                                  ))
                            ],
                          ),
                        ),*/

                                        Container(
                                          margin: EdgeInsets.only(left: 0, right: 15),
                                          padding: EdgeInsets.symmetric(horizontal: 0.0, vertical: 5.0),
                                          height: 70,
                                          child: ListView.builder(
                                              scrollDirection: Axis.horizontal,
                                              itemCount: numbers.length,
                                              itemBuilder: (context, index) {
                                                print("DEFAULTSELECTEDTIP" + selected_tip.toString());
                                                return Container(
                                                  margin: EdgeInsets.all(5),
                                                  width: MediaQuery.of(context).size.width * 0.2,
                                                  child: InkWell(
                                                      onTap: () {
                                                        setState(() {
                                                          selected_pos = index;
                                                          //menu_id = _get_all_menus[index].id;
                                                          print(numbers[index]);
                                                          selected_tip = double.parse(numbers[index]);

                                                          if (selected_tip > 0) {
                                                            _payment_grandroundup =
                                                                selected_tip + double.parse(_items_cart_grandtotal.toStringAsFixed(2));
                                                          } else {
                                                            _payment_grandroundup = double.parse(_items_cart_grandtotal.toStringAsFixed(2));
                                                          }

                                                          print("SELECTEDTIP" +
                                                              selected_tip.toString() +
                                                              "======" +
                                                              _payment_grandroundup.toStringAsFixed(2));
                                                        });
                                                      },
                                                      child: Card(
                                                        elevation: 2,
                                                        color: selected_pos == index ? Colors.lightBlueAccent : Colors.white,
                                                        shape: RoundedRectangleBorder(
                                                          borderRadius: BorderRadius.circular(5),
                                                        ),
                                                        child: Container(
                                                          decoration: BoxDecoration(
                                                            border: Border.all(
                                                              color: login_passcode_bg2,
                                                            ),
                                                            borderRadius: BorderRadius.all(Radius.circular(5)),
                                                          ),
                                                          child: Column(
                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                            children: <Widget>[
                                                              Text(
                                                                new String.fromCharCodes(new Runes('\u0024')) + numbers[index],
                                                                style: TextStyle(color: add_food_item_bg, fontSize: 16),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      )),
                                                );
                                              }),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                  height: 60,
                                  margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(color: login_passcode_text, borderRadius: BorderRadius.circular(2)),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                              padding: EdgeInsets.fromLTRB(10, 10, 0, 0),
                                              child: Text(
                                                "OFFERS",
                                                textAlign: TextAlign.left,
                                                style: TextStyle(
                                                  fontSize: 14,
                                                  color: Colors.white,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w400,
                                                ),
                                              )),
                                          Padding(
                                              padding: EdgeInsets.fromLTRB(10, 5, 0, 0),
                                              child: Text(
                                                "Select a promo code",
                                                textAlign: TextAlign.left,
                                                style: TextStyle(
                                                  fontSize: 14,
                                                  color: Colors.white,
                                                  fontFamily: 'Poppins',
                                                  fontWeight: FontWeight.w400,
                                                ),
                                              ))
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          InkWell(
                                              child: FlatButton(
                                                  child: Text("View Offers",
                                                      style: TextStyle(
                                                          fontSize: 14, fontFamily: 'Poppins', fontWeight: FontWeight.w400, color: Colors.white)),
                                                  onPressed: () {})),
                                          Image.asset(
                                            "images/back_arrow.png",
                                            color: Colors.white,
                                            height: 20,
                                            width: 20,
                                            fit: BoxFit.cover,
                                          )
                                        ],
                                      )
                                    ],
                                  )),
                              Container(
                                margin: EdgeInsets.fromLTRB(5, 5, 5, 0),
                                alignment: Alignment.center,
                                child: Card(
                                  elevation: 5,
                                  color: order_appbar_bg,
                                  child: Container(
                                    decoration: BoxDecoration(
                                      border: Border.all(color: order_appbar_bg),
                                      borderRadius: BorderRadius.horizontal(left: Radius.circular(0.00), right: Radius.zero),
                                    ),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          margin: EdgeInsets.fromLTRB(0, 10, 10, 0),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Padding(
                                                  padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                  child: Text(
                                                    "Item total",
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                      fontSize: 14,
                                                      color: login_passcode_text,
                                                      fontFamily: 'Poppins',
                                                      fontWeight: FontWeight.w600,
                                                    ),
                                                  )),
                                              Padding(
                                                  padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                  child: Text(
                                                    new String.fromCharCodes(new Runes('\u0024')) + _items_cart_subtotal.toStringAsFixed(2),
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                      fontSize: 14,
                                                      color: login_passcode_text,
                                                      fontFamily: 'Poppins',
                                                      fontWeight: FontWeight.w600,
                                                    ),
                                                  )),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.fromLTRB(0, 10, 10, 0),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Padding(
                                                  padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                  child: Text(
                                                    "Delivery charge",
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                      fontSize: 14,
                                                      color: login_passcode_text,
                                                      fontFamily: 'Poppins',
                                                      fontWeight: FontWeight.w600,
                                                    ),
                                                  )),
                                              Padding(
                                                  padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                  child: Text(
                                                    new String.fromCharCodes(new Runes('\u0024')) + "0.00",
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                      fontSize: 14,
                                                      color: login_passcode_text,
                                                      fontFamily: 'Poppins',
                                                      fontWeight: FontWeight.w600,
                                                    ),
                                                  )),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.fromLTRB(0, 10, 10, 0),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Padding(
                                                  padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                  child: Text(
                                                    "Taxes",
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                      fontSize: 14,
                                                      color: login_passcode_text,
                                                      fontFamily: 'Poppins',
                                                      fontWeight: FontWeight.w600,
                                                    ),
                                                  )),
                                              Padding(
                                                  padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                  child: Text(
                                                    new String.fromCharCodes(new Runes('\u0024')) + _items_cart_taxtotal.toStringAsFixed(2),
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                      fontSize: 14,
                                                      color: login_passcode_text,
                                                      fontFamily: 'Poppins',
                                                      fontWeight: FontWeight.w600,
                                                    ),
                                                  )),
                                            ],
                                          ),
                                        ),
                                        selected_tip > 0
                                            ? Container(
                                                margin: EdgeInsets.fromLTRB(0, 10, 10, 0),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Padding(
                                                        padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                        child: Text(
                                                          "Tip Amount",
                                                          textAlign: TextAlign.left,
                                                          style: TextStyle(
                                                            fontSize: 14,
                                                            color: login_passcode_text,
                                                            fontFamily: 'Poppins',
                                                            fontWeight: FontWeight.w600,
                                                          ),
                                                        )),
                                                    Padding(
                                                        padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                        child: Text(
                                                          new String.fromCharCodes(new Runes('\u0024')) + selected_tip.toStringAsFixed(2),
                                                          textAlign: TextAlign.left,
                                                          style: TextStyle(
                                                            fontSize: 14,
                                                            color: login_passcode_text,
                                                            fontFamily: 'Poppins',
                                                            fontWeight: FontWeight.w600,
                                                          ),
                                                        )),
                                                  ],
                                                ),
                                              )
                                            : SizedBox(),
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(10, 10, 0, 0),
                                          child: DotWidget(
                                            dashColor: location_border,
                                            dashHeight: 1,
                                            dashWidth: 10,
                                            emptyWidth: 2,
                                            totalWidth: 360,
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.fromLTRB(0, 20, 10, 10),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Padding(
                                                  padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                  child: Text(
                                                    "Grand Total",
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                      fontSize: 16,
                                                      color: login_passcode_text,
                                                      fontFamily: 'Poppins',
                                                      fontWeight: FontWeight.w800,
                                                    ),
                                                  )),
                                              Padding(
                                                  padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                  child: Text(
                                                    new String.fromCharCodes(new Runes('\u0024')) + _payment_grandroundup.toStringAsFixed(2),
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                      fontSize: 16,
                                                      color: login_passcode_text,
                                                      fontFamily: 'Poppins',
                                                      fontWeight: FontWeight.w800,
                                                    ),
                                                  )),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                  alignment: Alignment.center,
                                  color: order_appbar_bg,
                                  child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                                    Container(
                                      decoration: BoxDecoration(
                                        color: login_passcode_text,
                                        border: Border.all(color: order_appbar_bg),
                                        borderRadius: BorderRadius.horizontal(left: Radius.circular(5.00), right: Radius.circular(5.00)),
                                      ),
                                      height: 60,
                                      margin: EdgeInsets.fromLTRB(10, 10, 0, 10),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                                            Padding(
                                                padding: EdgeInsets.fromLTRB(10, 10, 20, 0),
                                                child: Text(
                                                  "Pay Using",
                                                  textAlign: TextAlign.left,
                                                  style: TextStyle(
                                                    fontSize: 14,
                                                    color: order_appbar_bg,
                                                    fontFamily: 'Poppins',
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                )),
                                            Padding(
                                                padding: EdgeInsets.fromLTRB(10, 5, 20, 5),
                                                child: Text(
                                                  payment_type,
                                                  textAlign: TextAlign.left,
                                                  style: TextStyle(
                                                    fontSize: 16,
                                                    color: Colors.white,
                                                    fontFamily: 'Poppins',
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                ))
                                          ]),
                                          Container(
                                            width: 30,
                                            height: 70,
                                            decoration: BoxDecoration(
                                              color: order_appbar_bg,
                                              border: Border.all(color: order_appbar_bg),
                                              borderRadius: BorderRadius.horizontal(left: Radius.circular(5.00), right: Radius.circular(5.00)),
                                            ),
                                            margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                            child: InkWell(
                                                child: FlatButton(
                                                    child: Padding(
                                                      padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                                      child: Image.asset(
                                                        "images/back_arrow.png",
                                                        height: 60,
                                                        width: 60,
                                                        fit: BoxFit.cover,
                                                      ),
                                                    ),
                                                    onPressed: () {
                                                      Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                              builder: (context) => PaymentTypesScreen(
                                                                  widget.restaurant_id,
                                                                  widget.delivery_location,
                                                                  widget.delivery_lattitude,
                                                                  widget.delivery_longitude,
                                                                  widget.dine_in_id,
                                                                  widget.dine_in_name,
                                                                  widget.dine_in_behaviour)));
                                                    })),
                                          )
                                        ],
                                      ),
                                    ),
                                    restaurant_rules_OrderAcceptanceStatus == true
                                        ? Container(
                                            height: 60,
                                            margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                            alignment: Alignment.center,
                                            decoration: BoxDecoration(color: login_passcode_text, borderRadius: BorderRadius.circular(5)),
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: [
                                                Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    Padding(
                                                        padding: EdgeInsets.fromLTRB(10, 10, 0, 0),
                                                        child: Text(
                                                          "Total",
                                                          textAlign: TextAlign.left,
                                                          style: TextStyle(
                                                            fontSize: 14,
                                                            color: Colors.white,
                                                            fontFamily: 'Poppins',
                                                            fontWeight: FontWeight.w400,
                                                          ),
                                                        )),
                                                    Padding(
                                                        padding: EdgeInsets.fromLTRB(10, 5, 0, 0),
                                                        child: Text(
                                                          new String.fromCharCodes(new Runes('\u0024')) + _payment_grandroundup.toStringAsFixed(2),
                                                          textAlign: TextAlign.left,
                                                          style: TextStyle(
                                                            fontSize: 14,
                                                            color: Colors.white,
                                                            fontFamily: 'Poppins',
                                                            fontWeight: FontWeight.w800,
                                                          ),
                                                        ))
                                                  ],
                                                ),
                                                Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    InkWell(
                                                        child: FlatButton(
                                                            child: Text("Place Order",
                                                                style: TextStyle(
                                                                    fontSize: 14,
                                                                    fontFamily: 'Poppins',
                                                                    fontWeight: FontWeight.w400,
                                                                    color: Colors.white)),
                                                            onPressed: () {
                                                              print("CARTLISTLENGTH" +
                                                                  __cart_items_list.length.toString() +
                                                                  "=======" +
                                                                  _viewOrder_list.length.toString());
                                                              if (__cart_items_list.length > 0) {
                                                                String del_dineinid = "";
                                                                String del_dinename = "";
                                                                String del_dineinbehaviour = "";

                                                                DeliveryInfo useraddress;
                                                                CustomerDetails customedetails;
                                                                if (widget.dine_in_name == "Delivery") {
                                                                  useraddress = DeliveryInfo("test", "0", widget.delivery_lattitude,
                                                                      widget.delivery_longitude, "home", widget.delivery_location);
                                                                  customedetails = CustomerDetails(user_id);
                                                                  del_dineinid = widget.dine_in_id;
                                                                  del_dinename = widget.dine_in_name;
                                                                  del_dineinbehaviour = widget.dine_in_behaviour;
                                                                } else {
                                                                  useraddress = DeliveryInfo("", "", "", "", "", "");
                                                                  customedetails = CustomerDetails(user_id);
                                                                  del_dineinid = widget.dine_in_id;
                                                                  del_dinename = widget.dine_in_name;
                                                                  del_dineinbehaviour = widget.dine_in_behaviour;
                                                                }

                                                                AddUserOrderRepository()
                                                                    .adduserorder(
                                                                        __cart_items_list,
                                                                        selected_tip.toString(),
                                                                        0.0,
                                                                        double.parse(_items_cart_grandtotal.toStringAsFixed(2)),
                                                                        double.parse(_items_cart_taxtotal.toStringAsFixed(2)),
                                                                        double.parse(_payment_grandroundup.toStringAsFixed(2)),
                                                                        widget.restaurant_id,
                                                                        0,
                                                                        customedetails,
                                                                        useraddress,
                                                                        del_dineinid,
                                                                        del_dinename,
                                                                        del_dineinbehaviour,
                                                                        1,
                                                                        false,
                                                                        false,
                                                                        _payment_grandroundup.toStringAsFixed(2),
                                                                        "",
                                                                        1,
                                                                        1)
                                                                    .then((result) {
                                                                  print("CARTSAVING" + result.toString());
                                                                  if (result.responseStatus == 0) {
                                                                    _loading = false;
                                                                    Navigator.of(_keyLoader.currentContext, rootNavigator: true).pop();
                                                                    Toast.show("No Data Found", context,
                                                                        duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                                                                  } else if (result.responseStatus == 1) {
                                                                    setState(() {
                                                                      print("ADDWAITERORDERID" + result.orderId);
                                                                      _loading = false;
                                                                      CartsRepository.removesaved_in_cart();
                                                                      getCartItemModelapi.clear();
                                                                      Navigator.pushAndRemoveUntil(
                                                                          context,
                                                                          MaterialPageRoute(builder: (context) => SuccessScreen()),
                                                                          (Route<dynamic> route) => false);
                                                                    });
                                                                  } else {
                                                                    Toast.show(result.result, context,
                                                                        duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                                                                  }
                                                                });
                                                              }
                                                            })),
                                                    Image.asset(
                                                      "images/back_arrow.png",
                                                      color: Colors.white,
                                                      height: 20,
                                                      width: 20,
                                                      fit: BoxFit.cover,
                                                    )
                                                  ],
                                                )
                                              ],
                                            ))
                                        : Container(
                                            height: 60,
                                            margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                            alignment: Alignment.center,
                                            decoration: BoxDecoration(color: edit_text_border_color, borderRadius: BorderRadius.circular(5)),
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: [
                                                Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    Padding(
                                                        padding: EdgeInsets.fromLTRB(10, 10, 0, 0),
                                                        child: Text(
                                                          "Total",
                                                          textAlign: TextAlign.left,
                                                          style: TextStyle(
                                                            fontSize: 14,
                                                            color: Colors.white,
                                                            fontFamily: 'Poppins',
                                                            fontWeight: FontWeight.w400,
                                                          ),
                                                        )),
                                                    Padding(
                                                        padding: EdgeInsets.fromLTRB(10, 5, 0, 0),
                                                        child: Text(
                                                          new String.fromCharCodes(new Runes('\u0024')) + _payment_grandroundup.toStringAsFixed(2),
                                                          textAlign: TextAlign.left,
                                                          style: TextStyle(
                                                            fontSize: 14,
                                                            color: Colors.white,
                                                            fontFamily: 'Poppins',
                                                            fontWeight: FontWeight.w800,
                                                          ),
                                                        ))
                                                  ],
                                                ),
                                                Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    InkWell(
                                                        child: FlatButton(
                                                            disabledTextColor: edit_text_border_color,
                                                            disabledColor: login_form_hint,
                                                            child: Text("Place Order",
                                                                style: TextStyle(
                                                                    fontSize: 14,
                                                                    fontFamily: 'Poppins',
                                                                    fontWeight: FontWeight.w400,
                                                                    color: Colors.white)),
                                                            onPressed: () {
                                                              Toast.show("Restaurant Not Accepting Orders", context,
                                                                  duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                                                            })),
                                                    Image.asset(
                                                      "images/back_arrow.png",
                                                      color: Colors.white,
                                                      height: 20,
                                                      width: 20,
                                                      fit: BoxFit.cover,
                                                    )
                                                  ],
                                                )
                                              ],
                                            )),
                                  ]))
                            ]),
                          )
                        : Container(
                            margin: EdgeInsets.fromLTRB(0, 100, 0, 0),
                            alignment: Alignment.center,
                            padding: EdgeInsets.only(left: 15, top: 10),
                            child: Column(
                              children: [
                                Image.asset(
                                  "images/no_order_dash.png",
                                  height: 500,
                                  width: 500,
                                ),
                                Padding(
                                    padding: EdgeInsets.only(top: 20, right: 0, bottom: 0),
                                    child: Text("Cart is empty.",
                                        style:
                                            TextStyle(color: login_passcode_text, fontSize: 24, fontFamily: 'Poppins', fontWeight: FontWeight.w400)))
                              ],
                            ),
                          ),
              )));
  }

  _cart_itemscalculation() {
    CartsRepository().getcartslisting().then((cartList) {
      setState(() {
        print(cartList.length);
        cart_count = cartList.length;
        //_items_cart_subtotal = 0.0;
        List<MenuCartItemapi> _cart_list = cartList;
        if (_cart_list.length < 0) {
          _items_cart_subtotal = 0.0;
          _items_cart_grandtotal = 0.0;
          _items_cart_discounttotal = 0.0;
          _items_cart_taxtotal = 0.0;
        } else {
          _items_cart_subtotal = 0.0;
          _items_cart_grandtotal = 0.0;
          _items_cart_discounttotal = 0.0;
          _items_cart_taxtotal = 0.0;

          for (var i = 0; i < _cart_list.length; i++) {
            _items_cart_itemtotal = 0.0;
            _items_cart_itemtotal = (double.parse(_cart_list[i].unitPrice) * double.parse(_cart_list[i].quantity));

            _payment_grandroundup = _items_cart_itemtotal + selected_tip;

            for (var t = 0; t < _cart_list[i].taxesList.length; t++) {
              _items_cart_taxrate = _cart_list[i].taxesList[t].taxRate;
              print("ITEMTAXRATE" + _items_cart_taxrate.toString());
              // _items_cart_taxtotal =  _items_cart_taxtotal +  (_cart_list[i].taxesList[t].tax);

              _items_cart_taxrate = _items_cart_itemtotal * (_items_cart_taxrate / 100);
              print("ITEMTOTALANDTAXRATE" + _items_cart_taxrate.toStringAsFixed(2) + "=====" + _items_cart_taxtotal.toStringAsFixed(2));

              _items_cart_taxtotal = _items_cart_taxtotal + _items_cart_taxrate;

              print("CARTTAXTOTAL" +
                  _items_cart_itemtotal.toString() +
                  "------" +
                  _items_cart_taxtotal.toStringAsFixed(2) +
                  "TAXRATEPERCENT" +
                  _items_cart_taxrate.toStringAsFixed(2));
            }

            _items_cart_discounttotal = _items_cart_discounttotal + _cart_list[i].discountAmount;

            print(i.toString() + "--" + _cart_list[i].discountAmount.toString());

            _items_cart_modifier_price = 0.0;
            for (int m = 0; m < _cart_list[i].modifiersList.length; m++) {
              _items_cart_modifier_price =
                  (_cart_list[i].modifiersList[m].modifierTotalPrice * double.parse(_cart_list[i].quantity)) + _items_cart_modifier_price;
            }
            _items_cart_special_price = 0.0;
            for (int s = 0; s < _cart_list[i].specialRequestList.length; s++) {
              _items_cart_special_price = _cart_list[i].specialRequestList[s].requestPrice + _items_cart_special_price;
            }

            print("BEFOREADD=====" +
                _items_cart_subtotal.toString() +
                "-----" +
                _items_cart_modifier_price.toString() +
                "-----" +
                _items_cart_special_price.toString());

            _items_cart_itemtotal = _items_cart_itemtotal + _items_cart_modifier_price + _items_cart_special_price;

            _items_cart_subtotal = _items_cart_subtotal + _items_cart_itemtotal;

            _items_cart_grandtotal = _items_cart_subtotal - _items_cart_discounttotal + _items_cart_taxtotal;

            _payment_grandroundup = _items_cart_grandtotal + selected_tip;
          }
          print(_items_cart_discounttotal.toString() +
              "--" +
              _items_cart_grandtotal.toString() +
              "TAXRATE" +
              _items_cart_taxrate.toString() +
              "TAXTOTAL" +
              _items_cart_taxtotal.toStringAsFixed(2) +
              _payment_grandroundup.toStringAsFixed(2));
        }
      });
    });
  }

  _cart_youritemscalculation() {
    setState(() {
      print("YOURITEMCALCULATION" + _viewOrder_list.length.toString());
      List<OrderItems> _cart_list = _viewOrder_list;
      if (_cart_list.length < 0) {
        _items_cart_subtotal = 0.0;
        _items_cart_grandtotal = 0.0;
        _items_cart_discounttotal = 0.0;
        _items_cart_taxtotal = 0.0;
      } else {
        _items_cart_subtotal = 0.0;
        _items_cart_grandtotal = 0.0;
        _items_cart_discounttotal = 0.0;
        _items_cart_taxtotal = 0.0;

        for (var i = 0; i < _cart_list.length; i++) {
          _items_cart_itemtotal = 0.0;
          _items_cart_itemtotal = (double.parse(_cart_list[i].unitPrice) * double.parse(_cart_list[i].quantity.toString()));

          _payment_grandroundup = _items_cart_itemtotal + selected_tip;

          /*for (var t = 0; t < _cart_list[i].taxesList.length; t++) {
            _items_cart_taxrate = _cart_list[i].taxesList[t].taxRate;
            print("ITEMTAXRATE" + _items_cart_taxrate.toString());
            // _items_cart_taxtotal =  _items_cart_taxtotal +  (_cart_list[i].taxesList[t].tax);

            _items_cart_taxrate = _items_cart_itemtotal * (_items_cart_taxrate / 100);
            print("ITEMTOTALANDTAXRATE" + _items_cart_taxrate.toStringAsFixed(2) + "=====" + _items_cart_taxtotal.toStringAsFixed(2));

            _items_cart_taxtotal = _items_cart_taxtotal + _items_cart_taxrate;

            print(
                "YOURITEMCALCULATIONCARTTAXTOTAL" + _items_cart_itemtotal.toString() + "------" + _items_cart_taxtotal.toStringAsFixed(2) + "TAXRATEPERCENT" + _items_cart_taxrate.toStringAsFixed(2));
          }*/

          _items_cart_discounttotal = _items_cart_discounttotal + double.parse(_cart_list[i].discountAmount);

          print(i.toString() + "--" + _cart_list[i].discountAmount.toString());

          _items_cart_modifier_price = 0.0;
          for (int m = 0; m < _cart_list[i].modifiersList.length; m++) {
            _items_cart_modifier_price =
                (double.parse(_cart_list[i].modifiersList[m].modifierTotalPrice) * double.parse(_cart_list[i].quantity.toString())) +
                    _items_cart_modifier_price;
          }
          _items_cart_special_price = 0.0;
          for (int s = 0; s < _cart_list[i].specialRequestList.length; s++) {
            _items_cart_special_price = _cart_list[i].specialRequestList[s].requestPrice + _items_cart_special_price;
          }

          print("YOURITEMCALCULATIONBEFOREADD=====" +
              _items_cart_subtotal.toString() +
              "-----" +
              _items_cart_modifier_price.toString() +
              "-----" +
              _items_cart_special_price.toString());

          _items_cart_itemtotal = _items_cart_itemtotal + _items_cart_modifier_price + _items_cart_special_price;

          _items_cart_subtotal = _items_cart_subtotal + _items_cart_itemtotal;

          _items_cart_grandtotal = _items_cart_subtotal - _items_cart_discounttotal + _items_cart_taxtotal;

          _payment_grandroundup = _items_cart_grandtotal + selected_tip;
        }
        print(_items_cart_discounttotal.toString() +
            "--" +
            _items_cart_grandtotal.toString() +
            "TAXRATE" +
            _items_cart_taxrate.toString() +
            "TAXTOTAL" +
            _items_cart_taxtotal.toStringAsFixed(2) +
            _payment_grandroundup.toStringAsFixed(2));
      }
    });
  }

  Future gewinner() async {
    returnVal = await showDialog(
        context: context,
        builder: (context) {
          return _MyDialog(
            restaurant_id: res_id,
            user_id: user_id,
          );
        });
    print("RETURNVALUEINSIDE" + returnVal);

    if (returnVal == "Success") {
      Future.delayed(Duration(seconds: 2), () async {
        setState(() {});
      });
    }
  }
}

_savecartList(cartlist) async {
  final prefs = await SharedPreferences.getInstance();

  final key1 = 'save_to_cartlist';
  final cartlist_value = jsonEncode(cartlist);
  prefs.setString(key1, cartlist_value);
  print('savedCART===== $cartlist_value');

  /*if(cartlist == [] || cartlist.isEmpty || cartlist.length == ""){
    //await prefs.clear();
    print('savedCART' +"EMPTYCART");
    await prefs.remove(key1);
  }*/
}

class _MyDialog extends StatefulWidget {
  _MyDialog({
    this.restaurant_id,
    this.user_id,
  });

  final String restaurant_id;
  final String user_id;

  @override
  _MyDialogState createState() => _MyDialogState();
}

class _MyDialogState extends State<_MyDialog> {
  var _isChecked = false;
  bool _loading = true;
  List<ModifierListApi> modifier_arraylist = new List();
  List<UserAddressBookList> getAddressLists = new List();

  @override
  void initState() {
    super.initState();
    AddressListRepository().getAddressLists(widget.user_id).then((result_addresslist) {
      setState(() {
        getAddressLists = result_addresslist;
        print("ORDERSLENGTH=====" + getAddressLists.length.toString());
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.white,
      insetPadding: EdgeInsets.all(20),
      child: Column(
        children: [
          Expanded(
              child: SingleChildScrollView(
                  child: Container(
                      //height: MediaQuery.of(context).size.height,
                      child: Column(mainAxisSize: MainAxisSize.max, children: [
            Container(
                height: 56,
                color: Colors.lightBlueAccent,
                width: double.infinity,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                        flex: 5,
                        child: Padding(
                            padding: EdgeInsets.only(left: 10, bottom: 0),
                            child: Text(
                              "AddressList",
                              style: TextStyle(color: Colors.white, fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w600),
                              textAlign: TextAlign.start,
                            ))),
                    Expanded(
                        flex: 1,
                        child: InkWell(
                            child: Padding(
                                padding: EdgeInsets.only(right: 15, bottom: 0),
                                child: Image.asset(
                                  "images/cancel.png",
                                  height: 25,
                                  width: 25,
                                  color: Colors.white,
                                )),
                            onTap: () {
                              Navigator.pop(context);
                            })),
                  ],
                )),
            ListView.builder(
                physics: ClampingScrollPhysics(),
                shrinkWrap: true,
                primary: false,
                scrollDirection: Axis.vertical,
                itemCount: getAddressLists.length,
                itemBuilder: (context, index) {
                  return Container(
                    margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    alignment: Alignment.center,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Expanded(
                          child: Card(
                              margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                              color: Colors.white,
                              elevation: 5,
                              child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Padding(
                                        padding: EdgeInsets.fromLTRB(10, 10, 0, 5),
                                        child: Text(
                                          getAddressLists[index].addressType,
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                            fontSize: 16,
                                            color: login_passcode_text,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w600,
                                          ),
                                        )),
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(15, 10, 10, 5),
                                      child: Image.asset(
                                        'images/timer.png',
                                        height: 15,
                                        width: 15,
                                      ),
                                    ),
                                  ],
                                ),
                                Padding(
                                    padding: EdgeInsets.fromLTRB(10, 10, 10, 5),
                                    child: Text(
                                      getAddressLists[index].fullAddress,
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontSize: 16,
                                        color: login_passcode_text,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.w600,
                                      ),
                                    )),
                                Container(
                                    margin: EdgeInsets.fromLTRB(10, 20, 15, 10),
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(color: login_passcode_text, borderRadius: BorderRadius.circular(5)),
                                    child: InkWell(
                                        child: FlatButton(
                                            child: Text("ADD DELIVERY INSTRUCTIONS",
                                                style: TextStyle(
                                                    fontSize: SizeConfig.safeBlockHorizontal * 4,
                                                    fontFamily: 'Poppins',
                                                    fontWeight: FontWeight.w400,
                                                    color: Colors.white)),
                                            onPressed: () {}))),
                              ])),
                        )
                      ],
                    ),
                  );
                })
          ])))),
        ],
      ),
    );
  }
}

/*Widget setupAlertDialoadContainer(context) {
  return Column(
    mainAxisSize: MainAxisSize.min,
    children: [
      Container(
        color: Colors.grey,
        height: 350.0, // Change as per your requirement
        width: 500.0, // Change as per your requirement
        child:  ListView.builder(
            physics: ClampingScrollPhysics(),
            shrinkWrap: true,
            primary: false,
            scrollDirection: Axis.vertical,
            itemCount: getAddressLists.length,
            itemBuilder: (context, index) {
              return Container(
                margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                alignment: Alignment.center,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Expanded(
                      child: Card(
                          margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          color: Colors.white,
                          elevation: 5,
                          child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                                    padding: EdgeInsets.fromLTRB(10, 10, 0, 5),
                                    child: Text(
                                      getAddressLists[index].addressType,
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontSize: 16,
                                        color: login_passcode_text,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.w600,
                                      ),
                                    )),
                                Padding(
                                  padding: EdgeInsets.fromLTRB(15, 10, 10, 5),
                                  child: Image.asset(
                                    'images/timer.png',
                                    height: 15,
                                    width: 15,
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                                padding: EdgeInsets.fromLTRB(10, 10, 10, 5),
                                child: Text(
                                  getAddressLists[index].fullAddress,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 16,
                                    color: login_passcode_text,
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.w600,
                                  ),
                                )),
                            Container(
                                margin: EdgeInsets.fromLTRB(10, 20, 15, 10),
                                alignment: Alignment.center,
                                decoration: BoxDecoration(color: login_passcode_text, borderRadius: BorderRadius.circular(5)),
                                child: InkWell(
                                    child: FlatButton(
                                        child: Text("ADD DELIVERY INSTRUCTIONS",
                                            style: TextStyle(fontSize: SizeConfig.safeBlockHorizontal * 4, fontFamily: 'Poppins', fontWeight: FontWeight.w400, color: Colors.white)),
                                        onPressed: () {}))),
                          ])),
                    )
                  ],
                ),
              );
            }),
      ),
      Align(
        alignment: Alignment.bottomRight,
        child: FlatButton(

          onPressed: (){
            Navigator.pop(context);
          },child: Text("Cancel"),),
      )
    ],
  );
}*/
