import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:requests/requests.dart';
import 'package:restaurant_user/apis/getitemmodifiers.dart';
import 'package:restaurant_user/restaurant_menu_search_screen.dart';
import 'package:restaurant_user/session/userRepository.dart';
import 'package:restaurant_user/showorders_screen.dart';
import 'package:restaurant_user/utils/Globals.dart';
import 'package:restaurant_user/utils/all_constans.dart';
import 'package:restaurant_user/utils/back_press.dart';
import 'package:restaurant_user/utils/sizeconfig.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:toast/toast.dart';
import 'package:toggle_switch/toggle_switch.dart';

import 'apis/CartsRepository.dart';
import 'apis/GetAllDiningOptionsApi.dart';
import 'apis/getAllDiscountItemsApi.dart';
import 'apis/getDietTypeList.dart';
import 'apis/getPopularDishesApi.dart';
import 'apis/getallmenuapi.dart';
import 'apis/getmenugroup.dart';
import 'apis/getrestaurantsOnlineOrderingRules.dart';
import 'cart.dart';
import 'dashboard.dart';
import 'utils/dottedline.dart';
import 'model/SendTaxRatesModel.dart';
import 'model/cartmodelitemsapi.dart';
import 'model/get_diettypelistresponse.dart';
import 'model/get_restaurantdetailresponse.dart';
import 'model/get_restaurantsonlineorderingresponse.dart';
import 'model/getalldinningoptionsresponse.dart';
import 'model/getalldiscountitemresponse.dart';
import 'model/getallmenus.dart';
import 'model/getitemmodifiersresponse.dart';
import 'model/getitemmodifierssubmitresponse.dart';
import 'model/getmenugroups.dart';
import 'model/getmenuitems.dart';

class GetMenuItems {
  Future<List<MenuItem>> getmenu(String menu_id, String menu_groupid, String res_id, List dietTypelist) async {
    var body = json.encode({'menuId': menu_id, 'menuGroupId': menu_groupid, 'restaurantId': res_id, 'dietTypeList': dietTypelist});
    print(body);
    dynamic response = await Requests.post(base_url + "menu_items", body: body, headers: {'Content-type': 'application/json'});
    print("resultadditem---" + response);
    final res = json.decode(response);
    //print("result---"+res);
    if (res['responseStatus'] == 1) {
      Iterable list = res['menuItem'];
      return list.map((model) => MenuItem.fromJson(model)).toList();
    } else {
      return [];
    }
  }
}

class RestaurantDetailScreen extends StatefulWidget {
  final String restaurant_id;

  const RestaurantDetailScreen(this.restaurant_id, {Key key}) : super(key: key);

  @override
  _RestaurantDetailScreenState createState() => _RestaurantDetailScreenState();
}

class _RestaurantDetailScreenState extends State<RestaurantDetailScreen> {
  int cart_count = 0;
  bool enable_searchlist = false;
  String userid = "60e420c859263dad5bcb64b0";
  String employee_id = "";
  String first_name = "";
  String last_name = "";
  String restaurant_id = "";
  String servicearea_id = "";
  String user_id = "";
  String restaurant_detail_restaurantname = "";
  String restaurant_detail_restaurantdescription = "";
  double screenheight = 0.0;
  String returnVal = "";
  bool hasTimerStopped = false;
  final GlobalKey<State> _keyLoader = GlobalKey<State>();
  String showgoutview = "order_location_icon.png";
  String orderscanner = "order_scanner.png";
  String restaurantsquare = "restaurant_square.png";
  String noorders = "no_order_dash.png";
  TextEditingController search_Controller = TextEditingController();

  bool isSwitched = false;
  bool isSwitchedveg = false;

  List<bool> isSwitchesList = [];

  List<MenuList> _get_all_menus = [];
  List<MenugroupsList> _get_menu_groups = [];
  List<Menus> _get_restaurantdetail_menu = [];
  List<MenuGroups> _get_restaurantdetail_menu_groups = [];
  List<MenuItems> _get_restaurantdetail_menu_items = [];

  List<PopularDishes> getPopularDishes_menu = [];
  bool _loading = true, menuGroupLoading = false, menuItemsLoading = false;

  int selected_pos = 0;
  int selected_dinein_option = 0;

  // String res_id = "612f13ad5a69ba9c1950761b";
  String menu_id = "";
  String dine_in_id = "";
  String dine_in_name = "";
  String dine_in_behaviour = "";

  String menu_groupid = "";
  List<MenuItem> _get_all_menuitems = [];
  List<ModifiersGroupsList> _get_all_menuitemsmodifiersgrouplist;
  List<ModifierList> _get_all_menuitemsmodifierslist;
  List<String> selectedCheckbox = [];

  String session_latitude = "";
  String session_longitude = "";
  String session_location = "";

  int selected = 0;
  List<DietTypeLists> getDietTypeList = [];
  List<String> getDietTypelistId = [];

  bool add_to_cart = false;
  var counter_value = 1;
  List<ModifierListApi> modifier_arraylist = [];
  List<SpecialRequestListApi> special_request_arraylist = [];
  List<SendTaxRatesModel> taxes_arraylist = [];
  List<MenuCartItemapi> __cart_items_list = [];
  List<String> selected_item_quantity = [];
  List<bool> isAddtoCart = [];
  List<DineInList> _getalldineoptions = [];
  List<String> toggleSwitches = [];

  bool restaurant_rules_DeliveryStatus = false;
  bool restaurant_rules_OrderAcceptanceStatus = false;
  bool restaurant_rules_Enabled_Status = false;
  bool restaurant_rules_TakeOutStatus = false;
  List<ApprovalRules> _approved_list = [];

  @override
  void initState() {
    super.initState();

    print("RESTAURANTID====" + widget.restaurant_id);

    UserRepository().getlocationdetails().then((getlocation) {
      setState(() {
        session_latitude = getlocation[0];
        session_longitude = getlocation[1];
        session_location = getlocation[2];

        UserRepository().getuserdetails().then((getuserdetails) {
          setState(() {
            user_id = getuserdetails[0];
            print("USERID====" + user_id + "=======" + widget.restaurant_id);

            RestaurantOnlineOrderingRulesRepository().getRestaurantOnlineOrderingRules(widget.restaurant_id).then((value) {
              print("SEARCHRESTAURANT RESPSTATUS  " + value.responseStatus.toString());
              debugPrint(
                  base_url + "------" + value.responseStatus.toString() + "-------" + value.result.toString() + "-----" + value.rulesData.toString());
              if (value.responseStatus == 1) {
                setState(() {
                  _loading = false;

                  for (int t = 0; t < value.rulesData.approvalRules.length; t++) {
                    _approved_list.add(value.rulesData.approvalRules[t]);
                  }

                  restaurant_rules_OrderAcceptanceStatus = value.rulesData.ordersAcceptanceStatus;
                  restaurant_rules_DeliveryStatus = value.rulesData.deliveryStatus;
                  restaurant_rules_Enabled_Status = value.rulesData.rulesEnabled;
                  restaurant_rules_TakeOutStatus = value.rulesData.takeOutStatus;

                  print("ORDERACCEPTANCE====" + restaurant_rules_TakeOutStatus.toString() + "====" + restaurant_rules_DeliveryStatus.toString());

                  GetAllDinningOptionsApiRepository().getAllDinningOptions(widget.restaurant_id).then((getalldineoptionlist) {
                    setState(() {
                      _getalldineoptions = getalldineoptionlist;
                      setState(() {
                        _loading = false;
                        toggleSwitches.clear();
                        print("GETALLDINEOPTIONSLIST" +
                            _getalldineoptions.length.toString() +
                            "====" +
                            restaurant_rules_DeliveryStatus.toString() +
                            "====" +
                            restaurant_rules_TakeOutStatus.toString());
                        for (var i = 0; i < _getalldineoptions.length; i++) {
                          if (restaurant_rules_DeliveryStatus == true && _getalldineoptions[i].optionName == "Delivery" ||
                              restaurant_rules_TakeOutStatus == true && _getalldineoptions[i].optionName == "Takeout") {
                            toggleSwitches.add(_getalldineoptions[i].optionName);
                            // restaurant_rules_DeliveryStatus=false;
                            print("DELIVERY" + _getalldineoptions[i].optionName);
                          } else {
                            if (restaurant_rules_DeliveryStatus == false && _getalldineoptions[i].optionName == "Delivery") {
                              Toast.show("Restauarant Not Accepting Delivery Orders", context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                            } else if (restaurant_rules_TakeOutStatus == false && _getalldineoptions[i].optionName == "Takeout") {
                              Toast.show("Restauarant Not Accepting TakeOut Orders", context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                            } else if (restaurant_rules_DeliveryStatus == false &&
                                _getalldineoptions[i].optionName == "Delivery" &&
                                restaurant_rules_TakeOutStatus == false &&
                                _getalldineoptions[i].optionName == "Takeout") {
                              Toast.show("Restauarant Not Accepting Orders", context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                            }
                          }
                          /*if(restaurant_rules_TakeOutStatus == true&& _getalldineoptions[i].optionName =="Take Away"){
                            toggleSwitches.add(_getalldineoptions[i].optionName);
                           // restaurant_rules_TakeOutStatus = false;
                            print("TAKEWAY"+_getalldineoptions[i].optionName);
                          }else{
                            //Toast.show("Not Accepting  Takeout Orders", context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                          }*/
                          // toggleSwitches.add(_getalldineoptions[i].optionName);
                        }
                        print("TOGGLESWITCH" + toggleSwitches.length.toString());

                        //UserRepository.save_dineinoption_id(_getalldineoptions[0].id);
                      });
                    });
                  });
                });
              } else if (value.responseStatus == 0) {
                setState(() {
                  _loading = false;
                  Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                });
              } else {
                setState(() {
                  _loading = false;
                  Toast.show(value.result, context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                });
              }
            });
          });
        });
      });
    });

    DietTypeListRepository().getDietTypeList().then((result_diettypelist) {
      setState(() {
        _loading = false;
        getDietTypeList = result_diettypelist;
        print("DIETTYPELISTLENGTH=====" + getDietTypeList.length.toString());
        isSwitchesList = List.filled(getDietTypeList.length, false);

        GetPopularDishesApi().getPopularDishes(widget.restaurant_id, getDietTypelistId).then((result_showpopulardishes) {
          setState(() {
            _loading = false;
            getPopularDishes_menu = result_showpopulardishes.popularDishes;
            // _get_restaurantdetail_menu = result_showpopulardishes.menus;
            print("POPULAR DISH LENGTH=====" + getPopularDishes_menu.length.toString());

            isAddtoCart = List.filled(getPopularDishes_menu.length, false);

            restaurant_detail_restaurantname = result_showpopulardishes.restaurantDetails.restaurantName.toString();
            restaurant_detail_restaurantdescription = result_showpopulardishes.restaurantDetails.restaurantDescription.toString();

            GetAllMenu().getmenu(widget.restaurant_id, getDietTypelistId).then((result_allmenus) {
              setState(() {
                _loading = false;
                //isLoading = true;
                _get_all_menus = result_allmenus;
                menu_id = _get_all_menus[0].id;
                print(_get_all_menus.length);
                menuGroupLoading = true;
                GetMenuGroups().getmenu(widget.restaurant_id, menu_id, getDietTypelistId).then((result_allmenus) {
                  setState(() {
                    _loading = false;
                    //isLoading = true;
                    _get_menu_groups = result_allmenus;
                    print(_get_menu_groups.length);
                    menuGroupLoading = false;
                    _loading = false;
                    getCartItems();
                  });
                });
              });
            });
          });
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    //  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: login_passcode_bg1));
    print("Height" + SizeConfig.screenHeight.toString());
    final space = SizedBox(height: 20);
    if (SizeConfig.screenHeight >= 600 && SizeConfig.screenHeight < 800) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 550 && SizeConfig.screenHeight < 600) {
      screenheight = 550;
    }
    if (SizeConfig.screenHeight >= 800) {
      screenheight = 550;
    }
    final screen_height = MediaQuery.of(context).size.height;
    print(screen_height);

    return WillPopScope(
        onWillPop: () async => onBackPressed(context),
        child: Scaffold(
            resizeToAvoidBottomInset: true,
            backgroundColor: Colors.white,
            appBar: AppBar(
              toolbarHeight: 56,
              //toolbarHeight:  height-30.00,
              automaticallyImplyLeading: false,
              elevation: 0.0,
              backgroundColor: Colors.white,
              centerTitle: true,
              leading: Builder(
                builder: (BuildContext context) {
                  return IconButton(
                    padding: EdgeInsets.only(left: 12.0),
                    icon: Image.asset("images/back_arrow.png", width: 20, height: 20),
                    onPressed: () {
                      onBackPressed(context);
                      // Navigator.of(context).pushReplacement(
                      //     MaterialPageRoute(builder: (BuildContext context) => HomePage(session_latitude, session_longitude, session_location)));
                    },
                  );
                },
              ),
              actions: [
                Padding(
                  padding: const EdgeInsets.only(right: 25),
                  child: Container(
                      alignment: Alignment.centerRight,
                      height: 48.0,
                      width: 48.0,
                      child: GestureDetector(
                        onTap: () {
                          print("CARTSCREEN===" + dine_in_id + "=====" + dine_in_name + "=======" + dine_in_behaviour);
                          if (dine_in_id != null) {
                            print(dine_in_id + "=====" + dine_in_name + "========" + dine_in_behaviour);
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => CartScreen("Cash", dine_in_id, dine_in_name, dine_in_behaviour, session_location,
                                    session_latitude, session_longitude, widget.restaurant_id, "")));
                          }
                        },
                        child: Stack(
                          children: <Widget>[
                            Image.asset("images/cart_icon.png"),
                            cart_count == 0
                                ? Container()
                                : Positioned(
                                    top: -2.0,
                                    right: 0,
                                    child: Stack(
                                      children: <Widget>[
                                        Icon(Icons.brightness_1, size: 28.0, color: Colors.lightBlueAccent),
                                        Positioned(
                                            top: 5.0,
                                            right: 12.0,
                                            child: Center(
                                              child: Text(
                                                cart_count.toString(),
                                                style: TextStyle(color: Colors.white, fontSize: 11.0, fontWeight: FontWeight.bold),
                                              ),
                                            )),
                                      ],
                                    )),
                          ],
                        ),
                      )),
                )
              ],
            ),
            body: SafeArea(
              child: _loading
                  ? Center(
                      child: SpinKitCircle(color: Colors.lightBlueAccent),
                    )
                  : SingleChildScrollView(
                      child: Container(
                        margin: EdgeInsets.fromLTRB(15, 0, 0, 0),
                        //height: SizeConfig.safeBlockHorizontal * 155,
                        child: Column(crossAxisAlignment: CrossAxisAlignment.start, mainAxisSize: MainAxisSize.min, children: [
                          Padding(
                              padding: EdgeInsets.fromLTRB(15, 0, 0, 5),
                              child: Text(
                                restaurant_detail_restaurantname,
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontSize: 20,
                                  color: login_passcode_text,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w600,
                                ),
                              )),
                          Padding(
                              padding: EdgeInsets.fromLTRB(15, 0, 0, 5),
                              child: Text(
                                restaurant_detail_restaurantdescription,
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontSize: 14,
                                  color: login_passcode_text,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w400,
                                ),
                              )),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                padding: EdgeInsets.fromLTRB(10, 0, 5, 5),
                                child: SmoothStarRating(
                                  isReadOnly: true,
                                  color: Colors.amber,
                                  borderColor: Colors.grey,
                                  rating: 3.5,
                                  size: 25,
                                  starCount: 5,
                                ),
                              ),
                              Padding(
                                  padding: EdgeInsets.fromLTRB(0, 0, 0, 5),
                                  child: Text(
                                    "4.0",
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: login_passcode_text,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w600,
                                    ),
                                  )),
                              Padding(
                                  padding: EdgeInsets.fromLTRB(10, 0, 0, 5),
                                  child: Text(
                                    "33.6K Reviews",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: login_passcode_text,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ))
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(15, 0, 0, 5),
                                      child: Image.asset(
                                        'images/star.png',
                                        height: 15,
                                        width: 15,
                                      ),
                                    ),
                                    Padding(
                                        padding: EdgeInsets.fromLTRB(2, 0, 0, 5),
                                        child: Text(
                                          "4.5/5",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                            fontSize: 14,
                                            color: login_passcode_text,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w600,
                                          ),
                                        )),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(15, 0, 0, 5),
                                      child: Image.asset(
                                        'images/timer.png',
                                        height: 15,
                                        width: 15,
                                      ),
                                    ),
                                    Padding(
                                        padding: EdgeInsets.fromLTRB(2, 0, 0, 5),
                                        child: Text(
                                          "15-20 Min",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                            fontSize: 14,
                                            color: login_passcode_text,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w600,
                                          ),
                                        )),
                                  ],
                                ),
                                /* Row(children: [_horizontalDineOptionListView()])*/
                                Container(
                                  margin: EdgeInsets.fromLTRB(10, 0, 20, 0),
                                  child: _horizontalDineOptionListView(),
                                ),
                                /*Row(
                      children: [
                        Container(
                            height: 40,
                            margin: EdgeInsets.only(right: 15),
                            decoration: BoxDecoration(
                              color: login_passcode_text,
                              borderRadius: BorderRadius.only(topLeft: Radius.circular(5), bottomLeft: Radius.circular(5), topRight: Radius.circular(5), bottomRight: Radius.circular(5)),
                            ),
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                              child: ToggleSwitch(
                                fontSize: 10.0,
                                cornerRadius: 5,
                                inactiveFgColor: Colors.white,
                                initialLabelIndex: 0,
                                totalSwitches: 2,
                                activeBgColor: [receipt_header],
                                labels: ['Delivery', 'Takeaway'],
                                onToggle: (index) {
                                  print('switched to: $index');
                                },
                              ),
                            ))
                      ],
                    )*/
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(10, 15, 0, 0),
                            child: DotWidget(
                              dashColor: login_passcode_text,
                              dashHeight: 1,
                              dashWidth: 10,
                              emptyWidth: 2,
                              totalWidth: 380,
                            ),
                          ),
                          Row(
                            children: [
                              Padding(
                                padding: EdgeInsets.fromLTRB(20, 10, 0, 5),
                                child: Image.asset(
                                  'images/user.png',
                                  height: 12,
                                  width: 12,
                                ),
                              ),
                              Padding(
                                  padding: EdgeInsets.fromLTRB(8, 12, 0, 5),
                                  child: Text(
                                    "2150+ people ordered from here since lockdown",
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      fontSize: 13,
                                      color: login_passcode_text,
                                      fontFamily: 'Poppins',
                                      fontWeight: FontWeight.w400,
                                    ),
                                  )),
                            ],
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(10, 10, 0, 0),
                            child: DotWidget(
                              dashColor: login_passcode_text,
                              dashHeight: 1,
                              dashWidth: 10,
                              emptyWidth: 2,
                              totalWidth: 380,
                            ),
                          ),
                          Container(
                              margin: EdgeInsets.only(left: 0, right: 0, top: 20),
                              padding: EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
                              decoration: BoxDecoration(
                                color: login_passcode_text,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(0),
                                    bottomLeft: Radius.circular(0),
                                    topRight: Radius.circular(0),
                                    bottomRight: Radius.circular(0)),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  InkWell(
                                    onTap: () {
                                      setState(() {
                                        // Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (BuildContext context) => RestaurantMenuSearchScreen()));
                                        Navigator.of(context)
                                            .push(MaterialPageRoute(builder: (BuildContext context) => RestaurantMenuSearchScreen()));
                                      });
                                    },
                                    child: Padding(
                                        padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                                        child: ClipOval(
                                          child: Container(
                                            color: Colors.white,
                                            padding: EdgeInsets.symmetric(horizontal: 14, vertical: 1),
                                            child: Image.asset(
                                              'images/search.png',
                                              width: 20,
                                              height: 50,
                                            ),
                                          ),
                                        )),
                                  ),
                                  Container(
                                    margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [_horizontalDietTypeListView()],
                                    ),
                                  ),
                                  /*Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Egg',
                            style: TextStyle(color: Colors.white, fontSize: 16.0),
                          ),
                          Padding(
                              padding: EdgeInsets.fromLTRB(0, 5, 5, 5),
                              child: Switch(
                                inactiveThumbColor: receipt_header,
                                inactiveTrackColor: Colors.white,
                                activeColor: Colors.orangeAccent,
                                activeTrackColor: Colors.white,
                                value: isSwitched,
                                onChanged: toggleSwitch,
                                */ /* onChanged: (value){
                        setState(() {

                        });
                      },*/ /*
                              )),
                        ],
                      ),*/

                                  Padding(
                                      padding: EdgeInsets.fromLTRB(5, 5, 15, 5),
                                      child: IconButton(
                                        padding: EdgeInsets.only(left: 5.0, right: 10),
                                        icon: Image.asset(
                                          "images/menu.png",
                                          width: 30,
                                          height: 30,
                                          color: Colors.white,
                                        ),
                                        onPressed: () {
                                          //Scaffold.of(context).openDrawer();
                                        },
                                      ))
                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.fromLTRB(8, 12, 0, 5),
                              child: Text(
                                "Popular Dishes",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontSize: 18,
                                  color: login_passcode_text,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w600,
                                ),
                              )),
                          getPopularDishes_menu.length > 0
                              ? ListView.builder(
                                  physics: ClampingScrollPhysics(),
                                  shrinkWrap: true,
                                  primary: false,
                                  scrollDirection: Axis.vertical,
                                  itemCount: getPopularDishes_menu.length,
                                  itemBuilder: (context, index) {
                                    return Container(
                                      margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          Container(
                                            height: 120,
                                            width: 120,
                                            decoration: BoxDecoration(
                                              color: login_passcode_text,
                                              borderRadius: BorderRadius.only(
                                                  topLeft: Radius.circular(5),
                                                  bottomLeft: Radius.circular(5),
                                                  topRight: Radius.circular(5),
                                                  bottomRight: Radius.circular(5)),
                                            ),
                                            child: Padding(
                                              padding: EdgeInsets.fromLTRB(1, 1, 1, 1),
                                              child: Image.asset(
                                                "images/food_restaurant.png",
                                                height: 120,
                                                width: 120,
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ),
                                          Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Padding(
                                                  padding: EdgeInsets.fromLTRB(10, 0, 0, 5),
                                                  child: Text(
                                                    getPopularDishes_menu[index].menuName,
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                      fontSize: 14,
                                                      color: login_passcode_text,
                                                      fontFamily: 'Poppins',
                                                      fontWeight: FontWeight.w600,
                                                    ),
                                                  )),
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                children: [
                                                  Padding(
                                                    padding: EdgeInsets.fromLTRB(10, 0, 5, 5),
                                                    child: SmoothStarRating(
                                                      isReadOnly: true,
                                                      color: Colors.amber,
                                                      borderColor: Colors.grey,
                                                      rating: 3.5,
                                                      size: 25,
                                                      starCount: 5,
                                                    ),
                                                  ),
                                                  Padding(
                                                      padding: EdgeInsets.fromLTRB(0, 0, 0, 5),
                                                      child: Text(
                                                        "250 votes",
                                                        textAlign: TextAlign.left,
                                                        style: TextStyle(
                                                          fontSize: 14,
                                                          color: login_passcode_text,
                                                          fontFamily: 'Poppins',
                                                          fontWeight: FontWeight.w600,
                                                        ),
                                                      )),
                                                ],
                                              ),
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  SizedBox(
                                                    width: 80.0,
                                                    child: Padding(
                                                        padding: EdgeInsets.fromLTRB(10, 0, 0, 5),
                                                        child: Text(
                                                          getPopularDishes_menu[index].menuGroupName,
                                                          maxLines: 2,
                                                          overflow: TextOverflow.ellipsis,
                                                          softWrap: false,
                                                          style: TextStyle(
                                                            fontSize: 14,
                                                            color: login_passcode_text,
                                                            fontFamily: 'Poppins',
                                                            fontWeight: FontWeight.w600,
                                                          ),
                                                        )),
                                                  ),
                                                  /* Padding(
                                      padding: EdgeInsets.fromLTRB(10, 0, 0, 5),
                                      child: Text(
                                        getPopularDishes_menu[index].menuGroupName,
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          fontSize: 14,
                                          color: login_passcode_text,
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w600,
                                        ),
                                      )),*/
                                                  Container(
                                                      margin: EdgeInsets.only(left: 10, top: 0),
                                                      decoration: BoxDecoration(
                                                        color: dashboard_bg,
                                                        borderRadius: BorderRadius.only(
                                                            topLeft: Radius.circular(30),
                                                            bottomLeft: Radius.circular(30),
                                                            topRight: Radius.circular(0),
                                                            bottomRight: Radius.circular(0)),
                                                      ),
                                                      alignment: Alignment.center,
                                                      width: 70,
                                                      height: 30,
                                                      child: Padding(
                                                        padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                                        child: Text(
                                                          String.fromCharCodes(Runes('\u0024')) + getPopularDishes_menu[index].basePrice,
                                                          textAlign: TextAlign.left,
                                                          style: TextStyle(
                                                            fontSize: 14,
                                                            color: login_passcode_text,
                                                            fontFamily: 'Poppins',
                                                            fontWeight: FontWeight.w600,
                                                          ),
                                                        ),
                                                      )),
                                                  restaurant_rules_DeliveryStatus == true || restaurant_rules_TakeOutStatus == true
                                                      ? isAddtoCart[index] == false
                                                          ? Container(
                                                              margin: EdgeInsets.only(left: 15, top: 0),
                                                              decoration: BoxDecoration(
                                                                color: Colors.lightBlueAccent,
                                                                borderRadius: BorderRadius.only(
                                                                    topLeft: Radius.circular(15),
                                                                    bottomLeft: Radius.circular(15),
                                                                    topRight: Radius.circular(15),
                                                                    bottomRight: Radius.circular(15)),
                                                              ),
                                                              alignment: Alignment.centerRight,
                                                              height: 30,
                                                              child: TextButton(
                                                                  // minWidth: 10,
                                                                  child: Text("+ADD",
                                                                      style: TextStyle(
                                                                          fontSize: 14,
                                                                          fontFamily: 'Poppins',
                                                                          fontWeight: FontWeight.w800,
                                                                          color: Colors.white)),
                                                                  onPressed: () {
                                                                    setState(() {
                                                                      isAddtoCart[index] = true;
                                                                      if (restaurant_rules_DeliveryStatus == true ||
                                                                          restaurant_rules_TakeOutStatus == true) {
                                                                        _addtocartButton(index);
                                                                      } else {
                                                                        if (restaurant_rules_DeliveryStatus == false) {
                                                                          Toast.show("Restaurant Not Accepting Delivery Orders", context,
                                                                              duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                                                                        }
                                                                        if (restaurant_rules_TakeOutStatus == false) {
                                                                          Toast.show("Restauarant Not Accepting TakeOut Orders", context,
                                                                              duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                                                                        }
                                                                      }
                                                                      /* if(restaurant_rules_TakeOutStatus){
                                                    _addtocartButton(index);
                                                  }else{
                                                    Toast.show("Restaurant Not Accepting TakeOut Orders", context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                                                  }*/

                                                                      //  add_to_cart = true;
                                                                      // _addtocartButton(index);
                                                                      // Navigator.pop(context, "Success");
                                                                    });
                                                                  }))
                                                          : _addincrementButton(index)
                                                      : /*_toastMessage()*/ Container(),
                                                ],
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    );
                                  })
                              : Container(
                                  margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                  alignment: Alignment.center,
                                  padding: EdgeInsets.only(left: 0, top: 0),
                                  child: Text("No Dishes Found",
                                      style: TextStyle(color: login_passcode_text, fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w600)),
                                ),
                          Container(
                            margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                            color: dashboard_bg,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [_horizontalListView(), _verticalListView()],
                            ),
                          )
                        ]),
                      ),
                    ),
            )));
  }

  void getCartItems() {
    /*Future.delayed(Duration(seconds: 2), () async {
      setState(() {
        CartsRepository().getcartslisting().then((cartList) {
          setState(() {
            __cart_items_list = cartList;
            cart_count = __cart_items_list.length;
            print(cart_count);
            final mapped = __cart_items_list.fold<Map<String, Map<String, dynamic>>>({}, (p, v) {
              final name = v.itemId;
              if (p.containsKey(name)) {
                p[name]["NumberOfItems"] += int.parse(v.quantity);
              } else {
                p[name] = {"NumberOfItems": int.parse(v.quantity)};
              }
              return p;
            });
            //print(mapped.length);
            // print(mapped);
            for (var i = 0; i < _get_all_menuitems.length; i++) {
              print(mapped.containsKey(_get_all_menuitems[i].id));
              if (mapped.containsKey(_get_all_menuitems[i].id) == true) {
                //print(mapped[_get_all_menuitems[i].id]['NumberOfItems']);
                selected_item_quantity.add(mapped[_get_all_menuitems[i].id]['NumberOfItems'].toString());
              } else {
                //print("0");
                selected_item_quantity.add("0");
              }
            }
            _loading = false;
            //_loading = false;
          });
        });
      });
    });*/
    Future.delayed(Duration(seconds: 2), () async {
      setState(() {
        CartsRepository().getcartslisting().then((cartList) {
          setState(() {
            __cart_items_list = cartList;
            cart_count = __cart_items_list.length;
            print(cart_count);
            final mapped = __cart_items_list.fold<Map<String, Map<String, dynamic>>>({}, (p, v) {
              final name = v.itemId;
              if (p.containsKey(name)) {
                p[name]["NumberOfItems"] += int.parse(v.quantity);
              } else {
                p[name] = {"NumberOfItems": int.parse(v.quantity)};
              }
              return p;
            });
            print(mapped.length);
            print(mapped);
            print(_get_all_menuitems.length);
            selected_item_quantity.clear();
            for (var i = 0; i < _get_all_menuitems.length; i++) {
              print(mapped.containsKey(_get_all_menuitems[i].id));
              if (mapped.containsKey(_get_all_menuitems[i].id) == true) {
                print(mapped[_get_all_menuitems[i].id]['NumberOfItems']);
                selected_item_quantity.add(mapped[_get_all_menuitems[i].id]['NumberOfItems'].toString());
              } else {
                print("0");
                selected_item_quantity.add("0");
              }
            } //_loading = false;
          });
        });
      });
    });
  }

  _addtocartButton(int index) {
    modifier_arraylist.add(ModifierListApi("0", "0", 0, "0", 0.0, 0));
    special_request_arraylist.add(SpecialRequestListApi("", 0));
    special_request_arraylist = [];
    modifier_arraylist = [];

    getCartItemModelapi.add(MenuCartItemapi(
        getPopularDishes_menu[index].id,
        getPopularDishes_menu[index].menuId,
        getPopularDishes_menu[index].menuGroupId,
        getPopularDishes_menu[index].menuName,
        getPopularDishes_menu[index].basePrice,
        counter_value.toString(),
        getPopularDishes_menu[index].basePrice.toString(),
        "",
        modifier_arraylist,
        special_request_arraylist,
        "normal",
        "0",
        "",
        "",
        "",
        0,
        0.0,
        "",
        "",
        false,
        "",
        "",
        false,
        false,
        false,
        taxes_arraylist));
    //addcart_session.add(getCartItemModel);
    print("getCartItemModelapi" + getCartItemModelapi.length.toString());

    _savecartList(getCartItemModelapi);
  }

  Widget _addincrementButton(int index) {
    return Card(
      color: Colors.white,
      elevation: 5,
      child: Row(
        children: [
          _decrementButton(index),
          Container(
              color: Colors.white,
              width: 10,
              child: Padding(
                padding: EdgeInsets.all(6),
                child: Text(
                  '${counter_value}',
                  style: TextStyle(color: login_passcode_text, fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w600),
                  textAlign: TextAlign.center,
                )
                /*Expanded(child: TextField(controller: cart_count_text_controller,decoration: InputDecoration(hintText: '${numbers[0]}'),style:TextStyle(
                                        color: login_passcode_text,
                                        fontSize: 16,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.w600) ,),)*/
                ,
              )),
          _incrementButton(index),
        ],
      ),
    );
  }

  Widget _incrementButton(int index) {
    return Container(
      margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
      child: Material(
        color: login_passcode_text,
        // button color
        child: InkWell(
          splashColor: login_passcode_bg2,
          // inkwell color
          child: SizedBox(
              width: 28,
              height: 28,
              child: Icon(
                Icons.add,
                color: Colors.white,
                size: 17,
              )),
          onTap: () {
            setState(() {
              counter_value = counter_value + 1;
            });
          },
        ),
      ),
    );
  }

  Widget _decrementButton(int index) {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
      child: Material(
        color: login_passcode_bg1,
        // button color
        child: InkWell(
          splashColor: login_passcode_bg2,
          // inkwell color
          child: SizedBox(
              width: 28,
              height: 28,
              child: Icon(
                Icons.remove,
                color: Colors.white,
                size: 17,
              )),
          onTap: () {
            setState(() {
              if (counter_value > 1) {
                counter_value = counter_value - 1;
              } else {
                isAddtoCart[index] = false;
              }
            });
          },
        ),
      ),
    );
  }

  Widget _horizontalListView() {
    return SizedBox(
      height: 120,
      child: _get_all_menus.length > 0
          ? ListView.builder(
              physics: ClampingScrollPhysics(),
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              primary: false,
              itemCount: _get_all_menus.length,
              itemBuilder: (context, index) {
                return Container(
                  margin: EdgeInsets.all(5),
                  width: MediaQuery.of(context).size.width * 0.3,
                  child: InkWell(
                      onTap: () {
                        selected_pos = index;
                        menu_id = _get_all_menus[index].id;
                        print("RESTAURANTDETAILMENUID" + menu_id);
                        menuGroupLoading = true;
                        GetMenuGroups().getmenu(widget.restaurant_id, menu_id, getDietTypelistId).then((result_allmenus) {
                          setState(() {
                            //isLoading = true;
                            _get_menu_groups = result_allmenus;
                            menuGroupLoading = false;
                            print("LENGTH" + _get_menu_groups.length.toString());
                          });
                        });

                        /*GetPopularDishesApi().getPopularDishes("612f13ad5a69ba9c1950761b").then((result_showpopulardishes) {
                      setState(() {
                        _get_restaurantdetail_menu_groups.clear();

                        getPopularDishes_menu = result_showpopulardishes.popularDishes;
                        _get_restaurantdetail_menu = result_showpopulardishes.menus;
                        print("ORDERSLENGTH=====" + getPopularDishes_menu.length.toString());

                        for (int g = 0; g < _get_restaurantdetail_menu.length; g++) {
                          for (int h = 0; h < _get_restaurantdetail_menu[g].menuGroups.length; h++) {
                            _get_restaurantdetail_menu_groups.add(_get_restaurantdetail_menu[g].menuGroups[h]);
                            // print("RESTAURANDETAILMENUGROUPS" + _get_restaurantdetail_menu_groups.length.toString());
                          }
                        }

                        restaurant_detail_restaurantname = result_showpopulardishes.restaurantDetails.restaurantName.toString();
                        restaurant_detail_restaurantdescription = result_showpopulardishes.restaurantDetails.restaurantDescription.toString();

                        //cart_count = getPopularDishes.length;
                      });
                    });*/
                      },
                      child: Card(
                        color: selected_pos == index ? Colors.lightBlueAccent : add_food_item_bg,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.horizontal(left: Radius.circular(3.00), right: Radius.zero),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Image.asset(
                                'images/food.png',
                                height: 36,
                                width: 36,
                              ),
                              SizedBox(height: 5),
                              Text(
                                _get_all_menus[index].menuName.toUpperCase(),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.white,
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w600,
                                ),
                              )
                            ],
                          ),
                        ),
                      )),
                );
              })
          : Container(
              margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
              alignment: Alignment.center,
              padding: EdgeInsets.only(left: 0, top: 0),
              child: Text("No Menus Found",
                  style: TextStyle(color: login_passcode_text, fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w600)),
            ),
    );
  }

  Widget _horizontalDineOptionListView() {
    return Container(
        height: 50,
        decoration: BoxDecoration(
          color: login_passcode_text,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5), bottomLeft: Radius.circular(5), topRight: Radius.circular(5), bottomRight: Radius.circular(5)),
        ),
        child:
            /*ListView.builder(
            physics: ClampingScrollPhysics(),
            scrollDirection: Axis.horizontal,
            shrinkWrap: true,
            primary: false,
            itemCount: _getalldineoptions.length,
            itemBuilder: (context, index) {
              return Padding(
                padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                child: ToggleSwitch(
                  fontSize: 14.0,
                  cornerRadius: 5,
                  inactiveFgColor: Colors.white,
                  initialLabelIndex: 0,
                  totalSwitches: _getalldineoptions.length,
                  activeBgColor: [receipt_header],
                  labels: toggleSwitches,
                  onToggle: (index) {
                    print('switched to:' + _getalldineoptions[index].id);
                  },
                ),
              )
                ;
            })*/
            ListView.builder(
                physics: ClampingScrollPhysics(),
                shrinkWrap: true,
                primary: false,
                scrollDirection: Axis.horizontal,
                itemCount: toggleSwitches.length,
                itemBuilder: (context, index) {
                  print("TOGGLESWITCH" + toggleSwitches.length.toString());

                  if (selected_dinein_option == index) {
                    dine_in_id = _getalldineoptions[index].id.toString();
                    dine_in_name = _getalldineoptions[index].optionName.toString();
                    dine_in_behaviour = _getalldineoptions[index].behavior.toString();
                  }

                  //print("DINEINNAME" + dine_in_id + "=====" + dine_in_name);
                  return Container(
                    margin: EdgeInsets.all(5),
                    width: MediaQuery.of(context).size.width * 0.2,
                    child: InkWell(
                        onTap: () {
                          setState(() {
                            selected_dinein_option = index;
                            dine_in_id = _getalldineoptions[selected_dinein_option].id.toString();
                            dine_in_name = _getalldineoptions[selected_dinein_option].optionName.toString();
                            dine_in_behaviour = _getalldineoptions[selected_dinein_option].behavior.toString();

                            print("SELECTEDDINEIN" + "=====" + dine_in_name + "======" + dine_in_id + "====" + dine_in_behaviour);
                          });
                        },
                        child: Card(
                          elevation: 2,
                          color: selected_dinein_option == index ? Colors.lightBlueAccent : Colors.white,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: login_passcode_bg2,
                              ),
                              borderRadius: BorderRadius.all(Radius.circular(5)),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  toggleSwitches[index],
                                  // _getalldineoptions[index].optionName,
                                  style: TextStyle(color: add_food_item_bg, fontSize: 14),
                                )
                              ],
                            ),
                          ),
                        )),
                  );
                }));
  }

  Widget _horizontalDietTypeListView() {
    return SizedBox(
      height: 50,
      child: Expanded(
        child: Container(
            padding: EdgeInsets.fromLTRB(1, 0, 0, 0),
            margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
            child: ListView.builder(
                physics: ClampingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                primary: false,
                itemCount: getDietTypeList.length,
                itemBuilder: (context, index) {
                  return Container(
                    margin: EdgeInsets.all(0),
                    // width: MediaQuery.of(context).size.width * 0.3,
                    child: _toggleButton(index),
                  );
                })),
      ),
    );
  }

  Widget _switchButton(int index) {
    return Padding(
      padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
      child: ToggleSwitch(
        fontSize: 10.0,
        cornerRadius: 5,
        inactiveFgColor: Colors.white,
        initialLabelIndex: 0,
        totalSwitches: _getalldineoptions.length,
        activeBgColor: [receipt_header],
        labels: toggleSwitches,
        onToggle: (index) {
          print('switched to:' + _getalldineoptions[index].optionName);
        },
      ),
    );
  }

  Widget _toggleButton(int index) {
    return Container(
        margin: EdgeInsets.all(3),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              getDietTypeList[index].name,
              style: TextStyle(color: Colors.white, fontSize: 16.0),
            ),
            Padding(
                padding: EdgeInsets.fromLTRB(0, 5, 5, 5),
                child: Switch(
                  inactiveThumbColor: receipt_header,
                  inactiveTrackColor: Colors.white,
                  activeColor: Colors.lightGreenAccent,
                  activeTrackColor: Colors.white,
                  value: isSwitchesList[index],
                  onChanged: (value) {
                    print("TOGGGLEBUTTON" + value.toString());
                    setState(() {
                      isSwitchesList[index] = value;

                      var id = getDietTypeList[index].id;
                      print("DIETTYPELISTID" + id + "=========" + getDietTypelistId.length.toString());

                      if (value) {
                        getDietTypelistId.add(id);
                      } else {
                        getDietTypelistId.remove(id);
                      }

                      print("AFTERREMOVING" + getDietTypelistId.length.toString());

                      GetPopularDishesApi().getPopularDishes(widget.restaurant_id, getDietTypelistId).then((result_showpopulardishes) {
                        setState(() {
                          _get_all_menuitems.clear();

                          getPopularDishes_menu = result_showpopulardishes.popularDishes;
                          // _get_restaurantdetail_menu = result_showpopulardishes.menus;
                          print("POPULAR DISH LENGTH=====" + getPopularDishes_menu.length.toString());

                          restaurant_detail_restaurantname = result_showpopulardishes.restaurantDetails.restaurantName.toString();
                          restaurant_detail_restaurantdescription = result_showpopulardishes.restaurantDetails.restaurantDescription.toString();

                          GetAllMenu().getmenu(widget.restaurant_id, getDietTypelistId).then((result_allmenus) {
                            setState(() {
                              //isLoading = true;
                              _get_all_menus = result_allmenus;
                              if (_get_all_menus.length > 0) {
                                menu_id = _get_all_menus[0].id;
                                print(_get_all_menus.length);
                                menuGroupLoading = true;
                                GetMenuGroups().getmenu(widget.restaurant_id, menu_id, getDietTypelistId).then((result_allmenus) {
                                  setState(() {
                                    //isLoading = true;
                                    _get_menu_groups = result_allmenus;
                                    print(_get_menu_groups.length);
                                    _loading = false;
                                    menuGroupLoading = false;
                                    GetMenuItems()
                                        .getmenu(menu_id, _get_menu_groups[index].id, widget.restaurant_id, getDietTypelistId)
                                        .then((result_allmenuitems) {
                                      setState(() {
                                        //isLoading = true;
                                        _get_all_menuitems = result_allmenuitems;
                                        print("ONEXPAND" + _get_all_menuitems.length.toString());
                                      });
                                    });
                                  });
                                });
                              }
                            });
                          });
                        });
                      });
                    });
                  },
                )),
          ],
        ));
  }

  Widget _verticalListView() {
    return menuGroupLoading
        ? Center(
            child: SpinKitCircle(color: Colors.lightBlueAccent),
          )
        : _get_menu_groups.isNotEmpty
            ? ListView.builder(
                key: Key('builder ${selected.toString()}'),
                shrinkWrap: true,
                primary: false,
                scrollDirection: Axis.vertical,
                itemCount: _get_menu_groups.length,
                itemBuilder: (context, index) {
                  return Container(
                    margin: EdgeInsets.fromLTRB(10, 0, 10, 5),
                    width: MediaQuery.of(context).size.width * 0.3,
                    /*decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.transparent),
                    borderRadius: BorderRadius.all(
                      Radius.circular(5.0),
                    )),*/
                    child: Card(
                      elevation: 0.5,
                      child: Theme(
                          data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
                          child: ExpansionTile(
                            key: Key(index.toString()),
                            //attention
                            initiallyExpanded: index == selected,
                            onExpansionChanged: ((newState) {
                              if (newState)
                                setState(() {
                                  //Duration(seconds: 20000);
                                  menuItemsLoading = true;
                                  selected = index;
                                  if (_get_menu_groups[index].id != null) {
                                    GetMenuItems()
                                        .getmenu(menu_id, _get_menu_groups[index].id, widget.restaurant_id, getDietTypelistId)
                                        .then((result_allmenuitems) {
                                      setState(() {
                                        _get_all_menuitems = result_allmenuitems;
                                        menuItemsLoading = false;
                                        print("ONEXPAND" + _get_all_menuitems.toString());
                                      });
                                    });
                                  }
                                });
                              else
                                setState(() {
                                  selected = -1;
                                });
                            }),
                            title: Text(_get_menu_groups[index].name,style: TextStyle(
                                 fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w600)),
                            leading: Image.asset(
                              'images/appetizer.png',
                              height: 26,
                              width: 26,
                            ),
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                child: Divider(
                                  color: Colors.black12,
                                ),
                              ),
                              menuItemsLoading
                                  ? Center(
                                      child: SpinKitCircle(color: Colors.lightBlueAccent),
                                    )
                                  : _get_all_menuitems.isNotEmpty
                                      ? ListView.builder(
                                          shrinkWrap: true,
                                          primary: false,
                                          scrollDirection: Axis.vertical,
                                          itemCount: _get_all_menuitems.length,
                                          itemBuilder: (context, index) {
                                            return Container(
                                              margin: EdgeInsets.all(0),
                                              width: MediaQuery.of(context).size.width * 0.3,
                                              child: Container(
                                                // color: Colors.white,
                                                padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                                //padding: EdgeInsets.all(15),
                                                //height: 60,
                                                child: Column(
                                                  children: <Widget>[
                                                    Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: <Widget>[
                                                        Expanded(
                                                            flex: 3,
                                                            child: Row(
                                                              children: [
                                                                Expanded(
                                                                    flex: 2,
                                                                    child: Padding(
                                                                        padding: EdgeInsets.all(10),
                                                                        child: Text(
                                                                          _get_all_menuitems[index].name,
                                                                          style: TextStyle(
                                                                              color: add_food_item_bg,
                                                                              fontSize: 16,
                                                                              fontFamily: 'Poppins',
                                                                              fontWeight: FontWeight.w600),
                                                                          textAlign: TextAlign.start,
                                                                        ))),
                                                              ],
                                                            )),
                                                        Expanded(
                                                            flex: 3,
                                                            child: Row(
                                                              children: [
                                                                Expanded(
                                                                    flex: 1,
                                                                    child: Padding(
                                                                        padding: EdgeInsets.only(
                                                                          left: 0,
                                                                          right: 5,
                                                                        ),
                                                                        child: Text(
                                                                          String.fromCharCodes(Runes('\u0024')) + _get_all_menuitems[index].basePrice,
                                                                          style: TextStyle(
                                                                              color: login_passcode_text,
                                                                              fontSize: 14,
                                                                              fontFamily: 'Poppins',
                                                                              fontWeight: FontWeight.w500),
                                                                          textAlign: TextAlign.right,
                                                                        ))),
                                                                Expanded(
                                                                    flex: 1,
                                                                    child: Container(
                                                                        padding: EdgeInsets.only(
                                                                          left: 0,
                                                                          right: 10,
                                                                        ),
                                                                        height: 35,
                                                                        decoration: BoxDecoration(
                                                                          color: Colors.lightBlueAccent,
                                                                          borderRadius: BorderRadius.only(
                                                                              topLeft: Radius.circular(15),
                                                                              bottomLeft: Radius.circular(15),
                                                                              topRight: Radius.circular(15),
                                                                              bottomRight: Radius.circular(15)),
                                                                        ),
                                                                        // color: Colors.lightBlueAccent,
                                                                        child: TextButton(
                                                                            //minWidth: 30,
                                                                            child: Text("Add",
                                                                                style: TextStyle(
                                                                                    fontSize: 14,
                                                                                    fontFamily: 'Poppins',
                                                                                    fontWeight: FontWeight.w800,
                                                                                    color: Colors.white)),
                                                                            onPressed: () {
                                                                              print("ADDBUTTON" +
                                                                                  restaurant_rules_DeliveryStatus.toString() +
                                                                                  "-------" +
                                                                                  restaurant_rules_TakeOutStatus.toString());
                                                                              if (restaurant_rules_DeliveryStatus == true ||
                                                                                  restaurant_rules_TakeOutStatus == true) {
                                                                                GetItemModifiers()
                                                                                    .getmenu(
                                                                                        widget.restaurant_id, _get_all_menuitems[index].id, user_id)
                                                                                    .then((result_allmenuitemsmodifiers) {
                                                                                  setState(() {
                                                                                    //isLoading = true;
                                                                                    _get_all_menuitemsmodifiersgrouplist =
                                                                                        result_allmenuitemsmodifiers;
                                                                                    print("chai" +
                                                                                        _get_all_menuitemsmodifiersgrouplist.length.toString());
                                                                                    if (_get_all_menuitems[index].priceProvider == 3 &&
                                                                                        _get_all_menuitems[index].pricingStrategy == 1) {
                                                                                      gewinner(index);
                                                                                    } else if (_get_all_menuitems[index].priceProvider == 3 &&
                                                                                        _get_all_menuitems[index].pricingStrategy == 4) {
                                                                                      gewinner(index);
                                                                                    } else if (_get_all_menuitems[index].priceProvider == 3 &&
                                                                                        _get_all_menuitems[index].pricingStrategy == 5) {
                                                                                      print("MEDUVADA");
                                                                                      Toast.show("MEDUVADA", context,
                                                                                          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                                                                                      gewinner(index);
                                                                                    } else {
                                                                                      print("IDLY");
                                                                                      gewinner(index);
                                                                                    }
                                                                                  });
                                                                                });
                                                                              } else {
                                                                                Toast.show("Restaurant Not Accepting Orders", context,
                                                                                    duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                                                                              }
                                                                              /*if(restaurant_rules_TakeOutStatus){
                                                                        GetItemModifiers().getmenu(widget.restaurant_id, _get_all_menuitems[index].id,user_id).then((result_allmenuitemsmodifiers) {
                                                                          setState(() {
                                                                            //isLoading = true;
                                                                            _get_all_menuitemsmodifiersgrouplist = result_allmenuitemsmodifiers;
                                                                            print("chai" + _get_all_menuitemsmodifiersgrouplist.length.toString());

                                                                            if (_get_all_menuitems[index].priceProvider == 3 && _get_all_menuitems[index].pricingStrategy == 1) {
                                                                              gewinner(index);
                                                                            } else if (_get_all_menuitems[index].priceProvider == 3 && _get_all_menuitems[index].pricingStrategy == 4) {
                                                                              gewinner(index);
                                                                            } else if (_get_all_menuitems[index].priceProvider == 3 && _get_all_menuitems[index].pricingStrategy == 5) {
                                                                              print("MEDUVADA");
                                                                              Toast.show("MEDUVADA", context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);

                                                                              gewinner(index);
                                                                            } else {
                                                                              print("IDLY");

                                                                              gewinner(index);
                                                                            }
                                                                          });
                                                                        });
                                                                      }else{
                                                                        Toast.show("Restaurant Not Accepting Orders", context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                                                                      }*/
                                                                            })))
                                                              ],
                                                            )),
                                                      ],
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                                      child: Divider(
                                                        color: Colors.transparent,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            );
                                          })
                                      : Container(
                                          margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                          alignment: Alignment.center,
                                          padding: EdgeInsets.only(bottom: 10, top: 10),
                                          child: Text("No Items Available.",
                                              style: TextStyle(
                                                  color: login_passcode_text, fontSize: 14, fontFamily: 'Poppins', fontWeight: FontWeight.w600)),
                                        )
                            ],
                          )),
                    ),
                  );
                })
            : Container(
                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                alignment: Alignment.center,
                padding: EdgeInsets.only(bottom: 10, top: 10),
                child: Text("No Data Found",
                    style: TextStyle(color: login_passcode_text, fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w600)),
              );
  }

  _toastMessage() async {
    Toast.show("Restaurant Not Accepting Orders", context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
  }

  Future gewinner(int index) async {
    returnVal = await showDialog(
        context: context,
        builder: (context) {
          return _MyDialog(
              user_id: user_id,
              restaurant_id: widget.restaurant_id,
              taxIncludeOption: _get_all_menuitems[index].taxIncludeOption,
              cart_item_id: _get_all_menuitems[index].id.toString(),
              cart_menu_id: _get_all_menuitems[index].menuId.toString(),
              cart_menugroup_id: _get_all_menuitems[index].menuGroupId.toString(),
              cart_item_unit_price: _get_all_menuitems[index].basePrice.toString(),
              header_name: _get_all_menuitems[index].name.toString(),
              header_price: _get_all_menuitems[index].basePrice.toString(),
              //  priceProvider: _get_restaurantdetail_menu_items[index].priceProvider.toString(),
              priceStrategy: _get_all_menuitems[index].pricingStrategy.toString(),
              //sizeList: _get_restaurantdetail_menu_items[index].sizeList,
              //  timepriceList: _get_restaurantdetail_menu_items[index].timePriceList,
              modifierGroupList: _get_all_menuitemsmodifiersgrouplist,
              // applicableTaxrates: _get_restaurantdetail_menu_items[index].aplicableTaxRates,
              selectedCities: selectedCheckbox,
              onSelectedCitiesListChanged: (modifierGroupList) {
                selectedCheckbox = modifierGroupList;
                print(selectedCheckbox);
              });
        });
    print("RETURNVALUEINSIDE" + returnVal);

    if (returnVal == "Success") {
      /*Future.delayed(Duration(seconds: 2), () async {
        setState(() {
          CartsRepository().getcartslisting().then((cartList) {
            setState(() {
              __cart_items_list = cartList;
              cart_count = __cart_items_list.length;
              print(cart_count);
              final mapped = __cart_items_list.fold<Map<String, Map<String, dynamic>>>({}, (p, v) {
                final name = v.itemId;
                if (p.containsKey(name)) {
                  p[name]["NumberOfItems"] += int.parse(v.quantity);
                } else {
                  p[name] = {"NumberOfItems": int.parse(v.quantity)};
                }
                return p;
              });
              print(mapped.length);
              print(mapped);
              print(_get_all_menuitems.length);
              selected_item_quantity.clear();
              for (var i = 0; i < _get_all_menuitems.length; i++) {
                print(mapped.containsKey(_get_all_menuitems[i].id));
                if (mapped.containsKey(_get_all_menuitems[i].id) == true) {
                  print(mapped[_get_all_menuitems[i].id]['NumberOfItems']);
                  selected_item_quantity.add(mapped[_get_all_menuitems[i].id]['NumberOfItems'].toString());
                } else {
                  print("0");
                  selected_item_quantity.add("0");
                }
              } //_loading = false;
            });
          });
        });
      });*/
      getCartItems();
    }
  }

  void toggleSwitchveg(bool value) {
    if (isSwitchedveg == false) {
      setState(() {
        isSwitchedveg = true;
      });
      print('VEG Button is ON');
    } else {
      setState(() {
        isSwitchedveg = false;
      });
      print('Switch Button is OFF');
    }
  }

  void toggleSwitch(bool value) {
    if (isSwitched == false) {
      setState(() {
        isSwitched = true;
      });
      print('EGG Button is ON');
    } else {
      setState(() {
        isSwitched = false;
      });
      print('Switch Button is OFF');
    }
  }
}

class _MyDialog extends StatefulWidget {
  _MyDialog({
    this.user_id,
    this.restaurant_id,
    this.taxIncludeOption,
    this.cart_item_id,
    this.cart_menu_id,
    this.cart_menugroup_id,
    this.cart_item_unit_price,
    this.header_name,
    this.header_price,
    // this.priceProvider,
    this.priceStrategy,
    // this.sizeList,
    this.timepriceList,
    this.modifierGroupList,
    this.applicableTaxrates,
    this.selectedCities,
    this.onSelectedCitiesListChanged,
  });

  final String user_id;
  final String restaurant_id;
  final bool taxIncludeOption;
  final String cart_item_id;
  final String cart_menu_id;
  final String cart_menugroup_id;
  final String cart_item_unit_price;
  final String header_name;
  final String header_price;

  //final String priceProvider;
  final String priceStrategy;

  //final List<SizeList> sizeList;
  final List<TimePriceList> timepriceList;

  final List<ModifiersGroupsList> modifierGroupList;
  final List<AplicableTaxRates> applicableTaxrates;
  final List<String> selectedCities;

  final ValueChanged<List<String>> onSelectedCitiesListChanged;

  @override
  _MyDialogState createState() => _MyDialogState();
}

class _MyDialogState extends State<_MyDialog> {
  var _isChecked = false;
  var main_item_name = "";
  var main_item_price = "";
  final _formKey = GlobalKey<FormState>();
  final _formKey2 = GlobalKey<FormState>();
  List<List<bool>> _group_checkslist = [];
  bool _loading = true;

  List<String> cartdataadding = [];
  List<ModifierListApi> modifier_arraylist = [];
  List<TaxTable> tax_table_arraylist = [];
  List<SendTaxRatesModel> taxes_arraylist = [];
  List<SpecialRequestListApi> special_request_arraylist = [];

  TextEditingController cart_count_text_controller;
  TextEditingController custom_item_price_controller = TextEditingController();
  TextEditingController text_description_controller = TextEditingController();
  TextEditingController text_specialitem_controller = TextEditingController();
  TextEditingController text_discount_amount_controller = TextEditingController();
  TextEditingController text_specialitemprice_controller = TextEditingController();
  var counter_value = 1;
  var open_discount_type = "";
  double open_discount_maxamount = 0.0;
  var open_discount_id = "";
  var discount_type = "";
  var discount_type_item = "";
  var discount_id = "";
  var discount_name = "";
  double discount_maxamount = 0.0;
  var open_discount_name = "";
  double total_discountprice = 0.0;
  double total_discountitemprice = 0.0;
  double total_discountprice_double = 0.0;
  var body = "";
  int selected_pos = 0;

  List<Discounts> _get_All_Discounts = [];
  var discount_id_api = "";
  var discount_type_api = "";

  var taxType = "";
  var taxTypeId = 0;
  var tax = 0.0;
  String roundingOptions = "";
  int roundingOptionId = 0;
  var day = "";
  var time = "";
  var timefrom = "";
  var timeto = "";
  var time_base_price = "";
  double modifier_price = 0.0;
  List<String> _timedays = [];

  Widget _incrementButton() {
    return Container(
      margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
      child: Material(
        color: login_passcode_text,
        // button color
        child: InkWell(
          splashColor: login_passcode_bg2,
          // inkwell color
          child: SizedBox(
              width: 35,
              height: 35,
              child: Icon(
                Icons.add,
                color: Colors.white,
                size: 17,
              )),
          onTap: () {
            setState(() {
              counter_value = counter_value + 1;
            });
          },
        ),
      ),
    );
  }

  Widget _decrementButton() {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
      child: Material(
        color: login_passcode_bg1,
        // button color
        child: InkWell(
          splashColor: login_passcode_bg2,
          // inkwell color
          child: SizedBox(
              width: 35,
              height: 35,
              child: Icon(
                Icons.remove,
                color: Colors.white,
                size: 17,
              )),
          onTap: () {
            setState(() {
              if (counter_value > 1) {
                counter_value = counter_value - 1;
              }
            });
          },
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    //   print("PRICESTRATEGY " + widget.priceProvider + "-----" + widget.priceStrategy);
    print(widget.modifierGroupList.length);
    //  print(widget.sizeList.length);
    // print("TIMEPRICELIST" + widget.timepriceList.length.toString());

    day = DateFormat('EEEE').format(DateTime.now());
    var now = DateTime.now();
    time = DateFormat('HH:mm').format(now);
    print(day + "=====" + time.toString());

    main_item_name = widget.header_name;
    main_item_price = widget.cart_item_unit_price;

    for (int g = 0; g < widget.modifierGroupList.length; g++) {
      List<bool> _items_checked = [];
      for (int h = 0; h < widget.modifierGroupList[g].modifierList.length; h++) {
        //print(g.toString()+"--"+h.toString()+"--"+widget.modifierGroupList[g].modifierList.length.toString());
        _items_checked.add(false);
      }
      _group_checkslist.add(_items_checked);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.white,
      insetPadding: EdgeInsets.all(20),
      child: Column(
        children: [
          Expanded(
              child: SingleChildScrollView(
                  child: Container(
                      //height: MediaQuery.of(context).size.height,
                      child: Expanded(
                          child: Column(
            children: <Widget>[
              Container(
                  width: double.infinity,
                  child: Column(mainAxisSize: MainAxisSize.max, children: [
                    Container(
                        height: 56,
                        color: Colors.lightBlueAccent,
                        width: double.infinity,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Expanded(
                                flex: 5,
                                child: Padding(
                                    padding: EdgeInsets.only(left: 10, bottom: 0),
                                    child: Text(
                                      widget.header_name,
                                      style: TextStyle(color: Colors.white, fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w600),
                                      textAlign: TextAlign.start,
                                    ))),
                            widget.header_price != 'null'
                                ? Expanded(
                                    flex: 3,
                                    child: Padding(
                                        padding: EdgeInsets.only(right: 5, bottom: 0),
                                        child: Text(
                                          String.fromCharCodes(Runes('\u0024')) + main_item_price,
                                          style: TextStyle(color: Colors.white, fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w700),
                                          textAlign: TextAlign.start,
                                        )))
                                : SizedBox(),
                            Expanded(
                                flex: 1,
                                child: InkWell(
                                    child: Padding(
                                        padding: EdgeInsets.only(right: 0, bottom: 0),
                                        child: Image.asset(
                                          "images/cancel.png",
                                          height: 25,
                                          width: 25,
                                          color: Colors.white,
                                        )),
                                    onTap: () {
                                      Navigator.of(context).pop();
                                    })),
                          ],
                        )),
                  ])),

              widget.priceStrategy == '5'
                  ? Container(
                      height: 30,
                      child: Row(
                        children: [
                          TextFormField(
                            validator: (val) {
                              if (val.isEmpty) return 'Enter Username';
                              return null;
                            },
                            controller: custom_item_price_controller,
                            obscureText: false,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white,
                                hintText: "Enter Price",
                                contentPadding: EdgeInsets.only(
                                  bottom: 30 / 2,
                                  left: 50 / 2, // HERE THE IMPORTANT PART
                                  // HERE THE IMPORTANT PART
                                ),
                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(0.0))),
                          ),
                          Container(
                              margin: EdgeInsets.fromLTRB(25, 30, 25, 0),
                              // height: 50,
                              width: double.infinity,
                              // alignment: Alignment.center,
                              decoration: BoxDecoration(color: login_passcode_bg1, borderRadius: BorderRadius.circular(0)),
                              child: TextButton(
                                  // minWidth: double.infinity,
                                  // height: double.infinity,
                                  child: Text("SUBMIT",
                                      style: TextStyle(fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w800, color: Colors.white)),
                                  onPressed: () {}))
                        ],
                      ),
                    )
                  : SizedBox(),
              //SIZELISTDATA
              /*widget.sizeList.length > 0
                  ? Container(
                      margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                      //padding: EdgeInsets.all(5),
                      height: 70,
                      color: dashboard_bg,
                      child: ListView.separated(
                          separatorBuilder: (context, i) => Divider(
                                color: Colors.grey,
                                height: 0.1,
                              ),
                          scrollDirection: Axis.horizontal,
                          itemCount: widget.sizeList.length,
                          itemBuilder: (context, i) {
                            return InkWell(
                              onTap: () {
                                setState(() {
                                  selected_pos = i;
                                  var sizelistname = widget.sizeList[i].sizeName.toString();
                                  var sizelistprice = widget.sizeList[i].price.toString();
                                  main_item_name = widget.header_name + " " + sizelistname;
                                  main_item_price = sizelistprice;
                                  print("SIZELISTDATA" + main_item_name + "-----" + main_item_price);
                                });
                              },
                              child: Card(
                                elevation: 2,
                                color: selected_pos == i ? Colors.lightBlueAccent : Colors.white,
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.horizontal(left: Radius.circular(3.00), right: Radius.zero),
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Padding(
                                          padding: EdgeInsets.only(left: 10, top: 5, right: 10, bottom: 0),
                                          child: Text(
                                            widget.sizeList[i].sizeName,
                                            style: TextStyle(fontSize: 15, fontFamily: 'Poppins', fontWeight: FontWeight.w500, color: login_passcode_text),
                                          )),
                                      SizedBox(height: 3),
                                      Padding(
                                          padding: EdgeInsets.only(left: 10, top: 0, right: 10, bottom: 5),
                                          child: Text(
                                            String.fromCharCodes(Runes('\u0024')) + widget.sizeList[i].price.toString(),
                                            style: TextStyle(fontSize: 14, fontFamily: 'Poppins', fontWeight: FontWeight.w500, color: login_passcode_text),
                                          ))
                                    ],
                                  ),
                                ),
                              ),
                            );
                          }))
                  : SizedBox(),*/
              //MODIFIERLISTDATA
              widget.modifierGroupList.length > 0
                  ? Container(
                      margin: EdgeInsets.only(left: 10, top: 10, right: 10),
                      //padding: EdgeInsets.all(5),
                      //height: MediaQuery.of(context).size.height * 0.2,
                      color: dashboard_bg,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: widget.modifierGroupList.length,
                            itemBuilder: (BuildContext context1, index) {
                              return Container(
                                margin: EdgeInsets.fromLTRB(10, 0, 0, 15),
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Padding(
                                            padding: EdgeInsets.only(left: 5, bottom: 0, top: 10),
                                            child: Text(
                                              widget.modifierGroupList[index].modifierGroupName,
                                              style: TextStyle(
                                                  color: login_passcode_text, fontSize: 15, fontFamily: 'Poppins', fontWeight: FontWeight.w700),
                                              textAlign: TextAlign.start,
                                            )),
                                        widget.modifierGroupList[index].minSelections != null
                                            ? Padding(
                                                padding: EdgeInsets.only(right: 0, bottom: 0, top: 10),
                                                child: Text(
                                                  "  (Please choose up to " + widget.modifierGroupList[index].minSelections.toString() + ")",
                                                  style: TextStyle(
                                                      color: Colors.black, fontSize: 10, fontFamily: 'Poppins', fontWeight: FontWeight.w400),
                                                  textAlign: TextAlign.start,
                                                ),
                                              )
                                            : SizedBox()
                                      ],
                                    ),
                                    ListView.builder(
                                        physics: NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        itemCount: widget.modifierGroupList[index].modifierList.length,
                                        itemBuilder: (BuildContext context1, j) {
                                          print("MAXSELECTION=====" + widget.modifierGroupList[index].maxSelections.toString());
                                          int cheked_count = _group_checkslist[index].where((item) => item == true).length;
                                          print("value" + cheked_count.toString());
                                          return Container(
                                              child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Row(children: <Widget>[
                                                Theme(
                                                    data: ThemeData(unselectedWidgetColor: Colors.grey),
                                                    child: Checkbox(
                                                        value: _group_checkslist[index][j],
                                                        onChanged: (value) {
                                                          setState(() {
                                                            if (cheked_count != widget.modifierGroupList[index].maxSelections) {
                                                              _group_checkslist[index][j] = value;
                                                            } else {
                                                              _group_checkslist[index][j] = false;
                                                              Toast.show(
                                                                  "You Can't choose more than " +
                                                                      widget.modifierGroupList[index].maxSelections.toString(),
                                                                  context,
                                                                  duration: Toast.LENGTH_SHORT,
                                                                  gravity: Toast.BOTTOM);
                                                            }
                                                            print("checkbox lecngth" + _group_checkslist[index].length.toString());
                                                          });
                                                        },
                                                        activeColor: Colors.lightBlueAccent)),
                                                Text(
                                                  widget.modifierGroupList[index].modifierList[j].modifierName.toString(),
                                                  style: TextStyle(
                                                      color: Colors.black, fontSize: 14, fontFamily: 'Poppins', fontWeight: FontWeight.w400),
                                                ),
                                              ]),
                                              Padding(
                                                padding: EdgeInsets.only(right: 15),
                                                child: Text(
                                                  String.fromCharCodes(Runes('\u0024')) +
                                                      widget.modifierGroupList[index].modifierList[j].price.toString(),
                                                  style: TextStyle(
                                                      color: Colors.black, fontSize: 14, fontFamily: 'Poppins', fontWeight: FontWeight.w400),
                                                ),
                                              ),
                                            ],
                                          ));
                                        }),
                                  ],
                                ),
                              );
                            },
                          ),
                        ],
                      ))
                  : SizedBox(),

              Container(
                margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                color: modifiers_bg,
                child: Expanded(
                  child: Column(
                    children: [
                      Container(
                          child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(left: 0, top: 10, right: 0),
                            child: Text(
                              "Special Request",
                              style: TextStyle(color: login_passcode_text, fontSize: 15, fontFamily: 'Poppins', fontWeight: FontWeight.w700),
                              textAlign: TextAlign.left,
                            ),
                          ),
                          special_request_arraylist.length > 0
                              ? ListView.builder(
                                  shrinkWrap: true,
                                  physics: ClampingScrollPhysics(),
                                  scrollDirection: Axis.vertical,
                                  itemCount: special_request_arraylist.length,
                                  itemBuilder: (context, i) {
                                    return Container(
                                      margin: EdgeInsets.all(2),
                                      width: MediaQuery.of(context).size.width * 0.3,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Expanded(
                                              flex: 5,
                                              child: Padding(
                                                  padding: EdgeInsets.only(left: 15, right: 0),
                                                  child: Text(
                                                    special_request_arraylist[i].name.toString(),
                                                    style: TextStyle(
                                                        color: coupontext, fontSize: 14, fontFamily: 'Poppins', fontWeight: FontWeight.w400),
                                                    textAlign: TextAlign.start,
                                                  ))),
                                          Expanded(
                                              flex: 1,
                                              child: Padding(
                                                padding: EdgeInsets.only(left: 0, right: 0),
                                                child: Text(
                                                  String.fromCharCodes(Runes('\u0024')) +
                                                      special_request_arraylist[i].requestPrice.toStringAsFixed(2),
                                                  style:
                                                      TextStyle(color: coupontext, fontSize: 14, fontFamily: 'Poppins', fontWeight: FontWeight.w400),
                                                  textAlign: TextAlign.start,
                                                ),
                                              )),
                                          InkWell(
                                              child: Expanded(
                                                  flex: 1,
                                                  child: Padding(
                                                      padding: EdgeInsets.only(left: 0, right: 5),
                                                      child: Image.asset(
                                                        'images/cancel.png',
                                                        height: 20,
                                                        width: 20,
                                                      ))),
                                              onTap: () {
                                                setState(() {
                                                  special_request_arraylist.removeAt(i);
                                                });
                                              })
                                        ],
                                      ),
                                    );
                                  })
                              : SizedBox(),
                          Container(
                              margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                              child: Form(
                                  key: _formKey,
                                  child: Expanded(
                                      child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Container(
                                        width: 100,
                                        child: TextFormField(
                                            validator: (val) {
                                              if (val.isEmpty) return 'Enter Itemname';
                                              return null;
                                            },
                                            controller: text_specialitem_controller,
                                            obscureText: false,
                                            keyboardType: TextInputType.text,
                                            decoration: InputDecoration(
                                                filled: true,
                                                fillColor: Colors.white,
                                                hintText: "Item",
                                                contentPadding: EdgeInsets.only(
                                                  bottom: 20 / 2,
                                                  left: 20 / 2, // HERE THE IMPORTANT PART
                                                  // HERE THE IMPORTANT PART
                                                ),
                                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)))),
                                      ),
                                      Container(
                                        width: 100,
                                        child: TextFormField(
                                            validator: (val) {
                                              if (val.isEmpty) return 'Enter Price';
                                              return null;
                                            },
                                            controller: text_specialitemprice_controller,
                                            obscureText: false,
                                            keyboardType: TextInputType.number,
                                            decoration: InputDecoration(
                                                filled: true,
                                                fillColor: Colors.white,
                                                hintText: "Price",
                                                contentPadding: EdgeInsets.only(
                                                  bottom: 30 / 2,
                                                  left: 50 / 2, // HERE THE IMPORTANT PART
                                                  // HERE THE IMPORTANT PART
                                                ),
                                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)))),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          left: 0,
                                          right: 15,
                                        ),
                                        child: Container(
                                          decoration: BoxDecoration(
                                            color: Colors.lightBlueAccent,
                                            borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(15),
                                                bottomLeft: Radius.circular(15),
                                                topRight: Radius.circular(15),
                                                bottomRight: Radius.circular(15)),
                                          ),
                                          child: TextButton(
                                              // minWidth: 30,
                                              child: Text("Add",
                                                  style: TextStyle(
                                                      fontSize: 14, fontFamily: 'Poppins', fontWeight: FontWeight.w800, color: Colors.white)),
                                              onPressed: () {
                                                if (_formKey.currentState.validate()) {
                                                  _formKey.currentState.save();
                                                  print(text_specialitem_controller.text.toString() +
                                                      "-----" +
                                                      text_specialitemprice_controller.text.toString());
                                                  setState(() {
                                                    special_request_arraylist.add(SpecialRequestListApi(
                                                        text_specialitem_controller.text, int.parse(text_specialitemprice_controller.text)));

                                                    text_specialitem_controller.text = "";
                                                    text_specialitemprice_controller.text = "";
                                                  });

                                                  print("SPECIALREQUESTLENGTH" + special_request_arraylist.length.toString());
                                                }
                                              }),
                                        ),
                                      )
                                    ],
                                  )))),
                        ],
                      )),
                      _get_All_Discounts.length > 0
                          ? Container(
                              child: Column(
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(left: 0, top: 10, right: 0),
                                  child: Text(
                                    "Discount Item",
                                    style: TextStyle(color: login_passcode_text, fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w700),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                                discount_type_api != "open"
                                    ? Container(
                                        margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              flex: 1,
                                              child: SizedBox(
                                                height: 120,
                                                child:
                                                    /*RadioListBuilder(
                                    num: 5,
                                  )*/
                                                    Container(
                                                        margin: EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
                                                        child: Expanded(
                                                          flex: 1,
                                                          child: ListView.builder(
                                                            itemCount: _get_All_Discounts.length,
                                                            itemBuilder: (context, index) {
                                                              return SizedBox(
                                                                  height: 40,
                                                                  child: RadioListTile(
                                                                    value: _get_All_Discounts[index],
                                                                    groupValue: _get_All_Discounts[index].orderValue,
                                                                    onChanged: (value) => setState(() {
                                                                      _get_All_Discounts[index].orderValue = value;
                                                                    }),

                                                                    /* onChanged: (ind){ setState(() => _get_All_Discounts[index].orderValue = ind);
                                            },*/
                                                                    title: Text(String.fromCharCodes(Runes('\u0024')) +
                                                                        _get_All_Discounts[index].maxDiscountAmount.toString()),
                                                                  ));
                                                            },
                                                          ),
                                                        )),
                                              ),
                                            ),
                                          ],
                                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                                        ),
                                      )
                                    : SizedBox(),
                                Container(
                                  margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: SizedBox(
                                          height: 80,
                                          child: Container(
                                              margin: EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
                                              child: Expanded(
                                                flex: 1,
                                                child: ListView.builder(
                                                  itemCount: _get_All_Discounts.length,
                                                  itemBuilder: (context, index) {
                                                    return SizedBox(
                                                        height: 40,
                                                        child: RadioListTile(
                                                          value: 1,
                                                          groupValue: _get_All_Discounts[index].orderValue,
                                                          onChanged: (value) => setState(() {
                                                            _get_All_Discounts[index].orderValue = value;

                                                            open_discount_type = _get_All_Discounts[index].discountValueType.toString();
                                                            open_discount_id = _get_All_Discounts[index].id.toString();
                                                            open_discount_name = _get_All_Discounts[index].name.toString();
                                                            open_discount_maxamount =
                                                                double.parse(_get_All_Discounts[index].maxDiscountAmount.toString());

                                                            print("DISCOUNTCHECKED" +
                                                                value.toString() +
                                                                "--------" +
                                                                open_discount_type +
                                                                "-----" +
                                                                open_discount_id +
                                                                "-----" +
                                                                open_discount_name);
                                                          }),
                                                          /* onChanged: (ind){ setState(() => _get_All_Discounts[index].orderValue = ind);
                                            },*/
                                                          title: Text(_get_All_Discounts[index].type.toString()),
                                                        ));
                                                  },
                                                ),
                                              )),
                                        ),
                                      ),
                                      Container(
                                          margin: EdgeInsets.fromLTRB(0, 0, 50, 10),
                                          child: Form(
                                              key: _formKey2,
                                              child: Expanded(
                                                  child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                children: [
                                                  Container(
                                                    width: 100,
                                                    child: TextFormField(
                                                        /* validator: (val) {
                                                      if (val.isEmpty)
                                                        return 'Enter Amount';
                                                      return null;
                                                    },*/
                                                        controller: text_discount_amount_controller,
                                                        obscureText: false,
                                                        keyboardType: TextInputType.number,
                                                        decoration: InputDecoration(
                                                            filled: true,
                                                            fillColor: Colors.white,
                                                            hintText: "Discount",
                                                            contentPadding: EdgeInsets.only(
                                                              bottom: 20 / 2,
                                                              left: 20 / 2, // HERE THE IMPORTANT PART
                                                              // HERE THE IMPORTANT PART
                                                            ),
                                                            border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)))),
                                                  ),
                                                ],
                                              )))),
                                    ],
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  ),
                                ),
                              ],
                            ))
                          : SizedBox(),
                    ],
                  ),
                ),
              ),
            ],
          ))))),
          Align(
              alignment: Alignment.bottomCenter,
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10, top: 10, right: 10),
                    child: TextField(
                      controller: text_description_controller,
                      textAlign: TextAlign.left,
                      style: TextStyle(color: login_passcode_text, fontSize: 14, fontFamily: 'Poppins', fontWeight: FontWeight.w400),
                      maxLines: 2,
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.all(20.0),
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
                        hintText: "Dressing on the side? No pickles? Let us know here.",
                      ),
                    ),
                  ),
                  Container(
                    // margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                    alignment: Alignment.topLeft,
                    child: Padding(
                      padding: EdgeInsets.only(left: 15, top: 10, right: 10),
                      child: Text(
                        "Quantity",
                        style: TextStyle(color: login_passcode_text, fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w700),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Card(
                          color: Colors.white,
                          elevation: 5,
                          child: Row(
                            children: [
                              _decrementButton(),
                              Container(
                                  color: Colors.white,
                                  width: 40,
                                  child: Padding(
                                    padding: EdgeInsets.all(6),
                                    child: Text(
                                      '${counter_value}',
                                      style: TextStyle(color: login_passcode_text, fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w600),
                                      textAlign: TextAlign.center,
                                    )
                                    /*Expanded(child: TextField(controller: cart_count_text_controller,decoration: InputDecoration(hintText: '${numbers[0]}'),style:TextStyle(
                                        color: login_passcode_text,
                                        fontSize: 16,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.w600) ,),)*/
                                    ,
                                  )),
                              _incrementButton(),
                            ],
                          ),
                        ),
                        Container(
                          alignment: Alignment.centerRight,
                          child: Padding(
                              padding: EdgeInsets.only(
                                left: 5,
                                right: 5,
                              ),
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.lightBlueAccent,
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(15),
                                      bottomLeft: Radius.circular(15),
                                      topRight: Radius.circular(15),
                                      bottomRight: Radius.circular(15)),
                                ),
                                padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                child: TextButton(
                                    // minWidth: 100,
                                    child: Text("Add to cart",
                                        style: TextStyle(fontSize: 16, fontFamily: 'Poppins', fontWeight: FontWeight.w600, color: Colors.white)),
                                    onPressed: () {
                                      if (counter_value >= 1) {
                                        print(_group_checkslist);
                                        for (int i = 0; i < _group_checkslist.length; i++) {
                                          //print("GROUPCHECKS" +_group_checkslist[i].toString());
                                          for (int j = 0; j < _group_checkslist[i].length; j++) {
                                            if (_group_checkslist[i][j] == true) {
                                              if (counter_value > 1) {
                                                modifier_price =
                                                    (double.parse(widget.modifierGroupList[i].modifierList[j].price) * counter_value).toDouble();

                                                print("MODIFIERMULTIPLICATION" + modifier_price.toString());
                                              } else {
                                                modifier_price = double.parse(widget.modifierGroupList[i].modifierList[j].price);
                                              }

                                              modifier_arraylist.add(ModifierListApi(
                                                  widget.modifierGroupList[i].modifierList[j].modifierId,
                                                  widget.modifierGroupList[i].modifierList[j].modifierName,
                                                  double.parse(widget.modifierGroupList[i].modifierList[j].price),
                                                  widget.modifierGroupList[i].modifierGroupId,
                                                  modifier_price,
                                                  counter_value));
                                            }
                                          }
                                        }

                                        var total_price = double.parse(main_item_price) * counter_value;

                                        print("TOTALPRICE-----" + total_price.toString() + "-------" + modifier_arraylist.length.toString());
                                        print("MAINITEMNAME-----" + main_item_name + "-------" + main_item_price);

                                        if (open_discount_type == "percentage") {
                                          discount_type_item = "item";
                                          discount_maxamount = open_discount_maxamount;

                                          total_discountprice =
                                              double.parse(main_item_price) * (double.parse(text_discount_amount_controller.text) / 100);

                                          //total_discountitemprice = double.parse(main_item_price) - total_discountprice;
                                          discount_type = open_discount_type;
                                          discount_name = open_discount_name;
                                          discount_id = open_discount_id;
                                          print("MAXDISCOUNTBEFORE" + total_discountprice.toString() + "-----------" + discount_maxamount.toString());
                                          if (total_discountprice == discount_maxamount) {
                                            total_discountprice = double.parse(discount_maxamount.toString());
                                            print("MAXDISCOUNTINSIDEIF" + total_discountprice.toString());
                                          } else {
                                            total_discountprice =
                                                double.parse(main_item_price) * (double.parse(text_discount_amount_controller.text) / 100);
                                            print("MAXDISCOUNTINSIDEELSE" + total_discountprice.toString());
                                          }
                                          // total_discountprice_double = double.parse(total_discountprice.toString());
                                          // main_item_price = total_discountitemprice.toString();
                                        } else {
                                          discount_type_item = "fixed";
                                        }
                                        discount_type_item = "fixed";

                                        //TAX CALCULATION
                                        print("TAXINCLUDEOPTION" + widget.taxIncludeOption.toString());
                                        if (widget.taxIncludeOption == true) {
                                          if (widget.applicableTaxrates.length > 0) {
                                            print("TAXRATELENGTH" + widget.applicableTaxrates.length.toString());
                                            for (int g = 0; g < widget.applicableTaxrates.length; g++) {
                                              taxType = widget.applicableTaxrates[g].taxType.toString();
                                              var taxrate = widget.applicableTaxrates[g].taxRate.toString();
                                              if (taxType == "0") {
                                                taxType = "disable";
                                                taxTypeId = 0;
                                                tax = 0.00;
                                                taxType = taxTypeId.toString();
                                              } else if (taxType == "1") {
                                                print("TAXTYPE" + taxType);
                                                var taxRate = total_price * (widget.applicableTaxrates[g].taxRate.toDouble() / 100);

                                                print("ROUNDING" + widget.applicableTaxrates[g].roundingOptions.toString());
                                                if (widget.applicableTaxrates[g].roundingOptions == 1) {
                                                  print("NUMBERROUNDUP" + taxRate.toString());
                                                  print("ROUNDINGhalfEVEN" + halfEven(taxRate, 2).toString());
                                                  taxRate = halfEven(taxRate, 2);
                                                } else if (widget.applicableTaxrates[g].roundingOptions == 2) {
                                                  print("ROUNDINGhalfup" + halfUp(taxRate, 2).toString());
                                                  taxRate = halfUp(taxRate, 2);
                                                } else if (widget.applicableTaxrates[g].roundingOptions == 3) {
                                                  print("ROUNDINGdown" + alwaysDown(taxRate, 2).toString());
                                                  taxRate = alwaysDown(taxRate, 2);
                                                } else if (widget.applicableTaxrates[g].roundingOptions == 4) {
                                                  print("ROUNDINGALWAYSUP" + getNumber(alwaysUp(taxRate), precision: 2).toString());
                                                  taxRate = getNumber(alwaysUp(taxRate), precision: 2);
                                                }

                                                taxType = "percent";
                                                taxTypeId = 1;
                                                taxType = taxTypeId.toString();
                                                tax = taxRate;
                                                print("TAXTYPE" +
                                                    taxType +
                                                    "=========" +
                                                    tax.toStringAsFixed(2) +
                                                    "========" +
                                                    widget.applicableTaxrates[g].taxid);
                                              } else if (taxType == "2") {
                                                taxType = "fixed";
                                                taxTypeId = 2;
                                                tax = total_price * widget.applicableTaxrates[g].taxRate.toDouble();
                                                taxType = taxTypeId.toString();
                                              } else if (taxType == "3") {
                                                taxType = "taxTable";
                                                taxTypeId = 3;
                                                if (widget.applicableTaxrates[g].taxTable.length > 0) {
                                                  for (int h = 0; h < widget.applicableTaxrates[h].taxTable.length; h++) {
                                                    if (total_price >= double.parse(widget.applicableTaxrates[g].taxTable[h].from) &&
                                                        total_price <= double.parse(widget.applicableTaxrates[g].taxTable[h].to)) {
                                                      tax += double.parse(widget.applicableTaxrates[g].taxTable[h].taxApplied);
                                                    }
                                                  }
                                                } else {
                                                  tax = 0.00;
                                                }
                                                taxType = taxTypeId.toString();
                                              }

                                              var sendTaxRates = SendTaxRatesModel(
                                                  widget.applicableTaxrates[g].enableTakeOutRate,
                                                  widget.applicableTaxrates[g].importId,
                                                  widget.applicableTaxrates[g].orderValue,
                                                  widget.applicableTaxrates[g].roundingOptions,
                                                  widget.applicableTaxrates[g].status,
                                                  widget.applicableTaxrates[g].taxName,
                                                  widget.applicableTaxrates[g].taxRate,
                                                  int.parse(taxType),
                                                  /* taxTypeId,*/
                                                  tax_table_arraylist,
                                                  widget.applicableTaxrates[g].taxid,
                                                  widget.applicableTaxrates[g].uniqueNumber,
                                                  tax);

                                              taxes_arraylist.add(sendTaxRates);
                                            }
                                          } else {
                                            taxes_arraylist = [];
                                          }
                                        } else {
                                          taxes_arraylist = [];
                                        }

                                        print("TAXESARRAYLIST" + taxes_arraylist.length.toString());

                                        getCartItemModelapi.add(MenuCartItemapi(
                                            widget.cart_item_id,
                                            widget.cart_menu_id,
                                            widget.cart_menugroup_id,
                                            main_item_name,
                                            main_item_price,
                                            counter_value.toString(),
                                            total_price.toString(),
                                            text_description_controller.text.toString(),
                                            modifier_arraylist,
                                            special_request_arraylist,
                                            "normal",
                                            discount_id,
                                            discount_name,
                                            discount_type_item,
                                            discount_type,
                                            0,
                                            total_discountprice,
                                            "",
                                            "",
                                            false,
                                            "",
                                            "",
                                            false,
                                            false,
                                            false,
                                            taxes_arraylist));
                                        //addcart_session.add(getCartItemModel);
                                        print("getCartItemModelapi" + getCartItemModelapi.length.toString());

                                        _savecartList(getCartItemModelapi);

                                        Navigator.of(context).pop("Success");

                                        Toast.show("Added to Cart", context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                                      } else {
                                        Toast.show("Minimum Cart Value is 1", context, duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
                                      }
                                    }),
                              )),
                        )
                      ],
                    ),
                  )
                ],
              ))
        ],
      ),
    );
  }

  bool isCurrentDateInRange(DateTime startDate, DateTime endDate) {
    final currentDate = DateTime.now();
    print(currentDate);
    DateTime tempDatefrom = DateFormat("HH:mm").parse(time);

    //  DateTime  tempDatefrom = DateFormat("HH:mm").parse(DateTime.now().timeZoneName);
    return tempDatefrom.isAfter(startDate) && tempDatefrom.isBefore(endDate);
  }

/*GetAllDiscountsApi(String item_id) {
    GetAllDiscountsItemRepository().getalldiscountitems(item_id, widget.user_id, widget.restaurant_id).then((result_allmenus) {
      setState(() {
        //isLoading = true;
        _get_All_Discounts = result_allmenus;
        discount_id_api = _get_All_Discounts[0].id;
        discount_type_api = _get_All_Discounts[0].type;
        print("GETALLDISCOUNTS" + discount_type + "----" + discount_id.toString());
      });
    });
  }*/
}

_savecartList(cartlist) async {
  final prefs = await SharedPreferences.getInstance();
  final key1 = 'save_to_cartlist';
  final cartlist_value = jsonEncode(cartlist);
  prefs.setString(key1, cartlist_value);
  print('savedCART $cartlist_value');
}
